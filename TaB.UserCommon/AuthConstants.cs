﻿namespace TaB.UserCommon
{
    public static class GrantTypes
    {
        public const string Password = "password";
        public const string Identity = "identity";
        public const string RefreshToken = "refresh_token";
    }
}
