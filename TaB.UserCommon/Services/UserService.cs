﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using TaB.Application;
using TaB.Application.Services;
using TaB.Domain.Consts;
using TaB.Domain.Entities;
using TaB.SharedKernel.Exceptions;
using TaB.UserCommon.Filters;
using TaB.UserCommon.Identity;
using TaB.UserCommon.Models;
using TaB.UserCommon.Queries;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using TaB.SharedKernel;
using Microsoft.Extensions.Options;

namespace TaB.UserCommon.Services
{
    public class UserService : BaseService<UserLogin, UserInfo, UserFilter, UserFilterQuery, UserService>, IUserService
    {
        private StorageOptions _storageOptions;
        public UserService(IUnitOfWorkFactory unitOfWorkFactory, ILogger<UserService> logger, IMapper mapper, IOptions<StorageOptions> storageOptions) : base(unitOfWorkFactory, logger, mapper) 
        {
            _storageOptions = storageOptions.Value;
        }

        public UserInfo Add(PutUserInfo data)
        {
            var userLogin = Mapper.Map<PutUserInfo, UserLogin>(data);
            userLogin.CreateTime = DateTime.UtcNow;
            var hasher = new CustomPasswordHasher();
            userLogin.PasswordHash = hasher.HashPassword(null, data.Password);
            using var uow = GetUnitOfWork();
            userLogin.HaveSecretPhrase = (data.SecretPhrase == _storageOptions.SecretPhrase);

            userLogin.UserRoleId = uow.Repository.GetBy<UserRole>(x => x.Code == data.Role).Id;
            if (userLogin.UserRoleId == 0)
            {
                throw new ExceptionBase(HttpStatusCode.BadRequest, $"Role '{data.Role}' not found!");
            }

            uow.Repository.Add(userLogin);
            uow.Commit();
            return GetBy(x => x.UserId == data.UserId);
        }

        public UserInfo Update(PostUserInfo data)
        {
            using var uow = GetUnitOfWork();
            var realUser = uow.Repository.GetBy<UserLogin>(x => x.UserId == data.UserId);

            if (realUser == null)
            {
                throw new ExceptionBase(HttpStatusCode.NotFound, "User not found!");
            }

            var role = uow.Repository.GetBy<UserRole>(x => x.Code == data.Role);

            if (role == null)
            {
                throw new ExceptionBase(HttpStatusCode.BadRequest, $"Role '{data.Role}' not found!");
            }

            realUser.UserRoleId = role.Id;
            realUser.Email = data.Email;
            realUser.IsDisabled = data.IsDisabled;
            realUser.FirstName = data.GivenName;
            realUser.LastName = data.Surname;

            uow.Repository.Update(realUser);
            uow.Commit();
            return Mapper.Map<UserInfo>(realUser);
        }

        public void ResetPassword(ResetPasswordModel model)
        {
            using var uow = GetUnitOfWork();
            var realUser = uow.Repository.GetBy<UserLogin>(x => x.UserId == model.UserId);

            if (realUser == null)
            {
                throw new ExceptionBase(HttpStatusCode.NotFound, "User not found");
            }

            var hasher = new CustomPasswordHasher();
            realUser.PasswordHash = hasher.HashPassword(null, model.Password);
            uow.Repository.Update(realUser);
            uow.Commit();
        }

        public void ChangePassword(ChangePasswordModel model)
        {
            using var uow = GetUnitOfWork();

            var realUser = uow.Repository.GetBy<UserLogin>(x => x.UserId == model.UserId);
            var hasher = new CustomPasswordHasher();

            if (realUser == null)
            {
                throw new ExceptionBase(HttpStatusCode.NotFound, "User name or password wrong.");
            }
            if (hasher.VerifyHashedPassword(null, realUser.PasswordHash, model.OldPassword) ==
                PasswordVerificationResult.Failed)
            {
                throw new ExceptionBase(HttpStatusCode.BadRequest, "Old password is wrong.");
            }

            realUser.PasswordHash = hasher.HashPassword(null, model.NewPassword);
            uow.Repository.Update(realUser);
            uow.Commit();
        }

        public List<RoleInfo> GetRoles()
        {
            using var uow = GetUnitOfWork();
            return uow.Repository.GetAll<UserRole>().Where(x => x.Code != SystemConst.SystemRole).ProjectTo<RoleInfo>(Mapper.ConfigurationProvider).ToList();
        }
    }
}