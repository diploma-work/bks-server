﻿using TaB.Application.Services;
using TaB.Domain.Entities;
using TaB.UserCommon.Filters;
using TaB.UserCommon.Models;

namespace TaB.UserCommon.Services
{
    public interface IUserService : IBaseService<UserLogin, UserInfo, UserFilter>
    {
        UserInfo Add(PutUserInfo data);
        UserInfo Update(PostUserInfo data);
        List<RoleInfo> GetRoles();
        void ResetPassword(ResetPasswordModel model);
        void ChangePassword(ChangePasswordModel model);
    }
}