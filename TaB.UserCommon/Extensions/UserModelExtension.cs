﻿using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Globalization;
using System.Security.Claims;
using System.Security.Principal;
using TaB.Application.Extensions;
using TaB.Domain.Entities;
using TaB.UserCommon.Models;

namespace TaB.UserCommon.Extensions
{
    public static class UserModelExtension
    {
        //private static readonly ILookupService _lookupService = ServiceLocator.Resolve<ILookupService>();
        public static bool Contains(this UserInfo user, string[] strList)
        {
            if (strList.Length < 1)
                return true;

            var id = user.UserId ?? string.Empty;
            var email = user.Email ?? string.Empty;
            var role = user.Role ?? string.Empty;
            //var location = user.LocationId == null ? string.Empty : _lookupService.GetById(user.LocationId.Value).Value ?? string.Empty;
            var createTime = user.CreateTime?.ToString(CultureInfo.CurrentCulture);
            var lastLoginTime = user.LastLoginTime;
            var disabled = user.IsDisabled.ToString();

            return strList.All(x => id.ContainsIgnoreCase(x) ||
                                    email.ContainsIgnoreCase(x) ||
                                    role.ContainsIgnoreCase(x) ||
                                    //location.ContainsIgnoreCase(x) ||
                                    createTime.ContainsIgnoreCase(x) ||
                                    disabled.ContainsIgnoreCase(x) ||
                                    (lastLoginTime?.ToString(CultureInfo.CurrentCulture).ContainsIgnoreCase(x) ?? false));
        }

        public static IPrincipal GetPrincipal(this UserLogin user)
        {
            var RoleClaimType = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
            var UserIdClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
            var UserNameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
            var SecurityStampClaimType = "AspNet.Identity.SecurityStamp";
            ClaimsIdentity id = new ClaimsIdentity("Importer", UserNameClaimType, RoleClaimType);
            id.AddClaim(new Claim(UserIdClaimType, user.Id.ToString(), "http://www.w3.org/2001/XMLSchema#string"));
            id.AddClaim(new Claim(UserNameClaimType, user.UserId, "http://www.w3.org/2001/XMLSchema#string"));
            id.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "ASP.NET Identity", "http://www.w3.org/2001/XMLSchema#string"));
            id.AddClaim(new Claim(RoleClaimType, user.UserRole.Code, "http://www.w3.org/2001/XMLSchema#string"));
            
            return new GenericPrincipal(id, new[] { user.UserRole.Code });
        }

        public static bool Compare(this UserLogin @this, UserPrincipal adUser, UserRole currentRole)
        {
            return string.Equals(@this.Email, adUser.EmailAddress, StringComparison.InvariantCultureIgnoreCase)
                   && string.Equals(@this.FirstName, adUser.GivenName, StringComparison.InvariantCultureIgnoreCase)
                   && string.Equals(@this.LastName, adUser.Surname, StringComparison.InvariantCultureIgnoreCase)
                   && @this.UserRoleId == currentRole.Id
                ;
        }

        public static UserLogin Update(this UserLogin @this, UserPrincipal adUser, UserRole currentRole)
        {
            @this.IsDisabled = adUser.IsAccountLockedOut();
            @this.Email = adUser.EmailAddress;
            @this.FirstName = adUser.GivenName;
            @this.LastName = adUser.Surname;
            @this.UserRoleId = currentRole.Id;
            return @this;
        }

        private static string GetProperty(this DirectoryEntry directoryEntry, string propertyName, int index = 0)
        {
            if (directoryEntry.Properties.Contains(propertyName) && index > -1 && index < directoryEntry.Properties[propertyName].Count)
            {
                return directoryEntry.Properties[propertyName][index].ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        private static string GetProperty(this Principal principal, string property)
        {
            var directoryEntry = principal.GetUnderlyingObject() as DirectoryEntry;
            return directoryEntry.GetProperty(property);
        }

        public static IPrincipal GetPrincipal(this UserInfo user)
        {
            var RoleClaimType = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
            var UserIdClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
            var UserNameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
            ClaimsIdentity id = new ClaimsIdentity("Importer", UserNameClaimType, RoleClaimType);
            id.AddClaim(new Claim(UserIdClaimType, user.Id.ToString(), "http://www.w3.org/2001/XMLSchema#string"));
            id.AddClaim(new Claim(UserNameClaimType, user.UserId, "http://www.w3.org/2001/XMLSchema#string"));
            id.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "ASP.NET Identity", "http://www.w3.org/2001/XMLSchema#string"));
            id.AddClaim(new Claim(RoleClaimType, user.Role, "http://www.w3.org/2001/XMLSchema#string"));

            return new GenericPrincipal(id, new[] { user.Role });
        }

    }
}