﻿using System.Security.Principal;

namespace TaB.UserCommon.Extensions
{
    public static class PrincipalExtensions
    {
        public static bool IsAuthenticated(this IPrincipal principal)
        {
            return principal?.Identity != null && principal.Identity.IsAuthenticated;
        }
    }
}
