﻿using AutoMapper;
using TaB.Domain.Entities;
using TaB.UserCommon.Filters;
using TaB.UserCommon.Models;
using TaB.UserCommon.Queries;

namespace TaB.UserCommon.Mappings
{
    public class MappingProfile : Profile
    {
        public override string ProfileName => "TaB.UM.Common.MappingProfile";

        public MappingProfile()
        {
            CreateMap<UserLogin, UserInfo>()
                .ForMember(dest => dest.Role, src => src.MapFrom(c => c.UserRole.Code))
                .ForMember(dest => dest.GivenName, src => src.MapFrom(c => c.FirstName))
                .ForMember(dest => dest.Surname, src => src.MapFrom(c => c.LastName));
           
            CreateMap<UserInfo, UserLogin>()
                .ForMember(dest => dest.FirstName, src => src.MapFrom(c => c.GivenName))
                .ForMember(dest => dest.LastName, src => src.MapFrom(c => c.Surname));

            CreateMap<PutUserInfo, UserLogin>()
                .ForMember(dest => dest.FirstName, src => src.MapFrom(c => c.GivenName))
                .ForMember(dest => dest.LastName, src => src.MapFrom(c => c.Surname));

            CreateMap<PostUserInfo, UserLogin>()
                .ForMember(dest => dest.FirstName, src => src.MapFrom(c => c.GivenName))
                .ForMember(dest => dest.LastName, src => src.MapFrom(c => c.Surname));

            CreateMap<UserRole, RoleInfo>();
            CreateMap<RoleInfo, UserRole>();

            CreateMap<UserInfo, PutUserInfo>().ReverseMap();
            CreateMap<UserInfo, PostUserInfo>().ReverseMap();
            CreateMap<PutUserInfo, PostUserInfo>().ReverseMap();

            CreateMap<UserFilter, UserFilterQuery>();
        }
    }

}