﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using TaB.Domain.Entities;
using TaB.UserCommon.Services;
using Microsoft.AspNetCore.Identity;

namespace TaB.UserCommon.Identity
{
    public class RoleStore : IRoleStore<UserRole>
    {
        private readonly IMapper _mapper;
        private readonly IUserService _umRepository;

        public RoleStore(IUserService umRepository, IMapper mapper)
        {
            _mapper = mapper;
            _umRepository = umRepository;
        }

        public async Task<IdentityResult> CreateAsync(UserRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            return IdentityResult.Success;
        }

        public async Task<IdentityResult> DeleteAsync(UserRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            return IdentityResult.Success;
        }

        public async Task<string> GetRoleIdAsync(UserRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<string> GetRoleNameAsync(UserRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task SetRoleNameAsync(UserRole role, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<string> GetNormalizedRoleNameAsync(UserRole role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task SetNormalizedRoleNameAsync(UserRole role, string normalizedName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<UserRole> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<UserRole> FindByIdAsync(int roleId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            return await Task.Run(() => _mapper.Map<UserRole>(_umRepository.GetRoles().FirstOrDefault(x => x.Id == roleId)), cancellationToken);
        }

        public async Task<UserRole> FindByNameAsync(string roleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            return await Task.Run(() => _mapper.Map<UserRole>(_umRepository.GetRoles().FirstOrDefault(x => x.Code.ToUpperInvariant() == roleName.ToUpperInvariant())), cancellationToken);
        }

        public async Task<IdentityResult> UpdateAsync(UserRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            return IdentityResult.Success;
        }

        public void Dispose()
        {
        }
    }
}