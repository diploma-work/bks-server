﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using TaB.Application;
using TaB.Domain.Entities;
using Microsoft.AspNetCore.Identity;

namespace TaB.UserCommon.Identity
{
    public class UserStore : 
        IUserStore<UserLogin>,
        IUserPasswordStore<UserLogin>,
        IUserLockoutStore<UserLogin>,
        IUserRoleStore<UserLogin>,
        IUserEmailStore<UserLogin>,
        IUnitOfWorkFactory
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;

        public UserStore(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
        }

        public IUnitOfWork GetUnitOfWork() => _unitOfWorkFactory.GetUnitOfWork();

        public async Task<IdentityResult> CreateAsync(UserLogin user, CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                using var uow = GetUnitOfWork();
                uow.Repository.Add(user);
                uow.Commit();
            }, cancellationToken);

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> UpdateAsync(UserLogin user, CancellationToken cancellationToken)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            await Task.Run(() =>
            {
                using var uow = GetUnitOfWork();
                uow.Repository.Update(user);
                uow.Commit();
            }, cancellationToken);

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> DeleteAsync(UserLogin user, CancellationToken cancellationToken)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            await Task.Run(() =>
            {
                using var uow = GetUnitOfWork();
                uow.Repository.Remove(user);
                uow.Commit();
            }, cancellationToken);
            return IdentityResult.Success;
        }

        public async Task<UserLogin> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (userId == null) throw new ArgumentNullException(nameof(userId));
            return await Task.Run(() =>
            {
                using var uow = GetUnitOfWork();
                return uow.Repository.GetBy<UserLogin>(x => x.UserId == userId,
                    new List<Expression<Func<UserLogin, object>>>() {{x => x.UserRole}});
            }, cancellationToken);
        }

        public async Task<UserLogin> FindByNameAsync(string userName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (userName == null) throw new ArgumentNullException(nameof(userName));
            return await Task.Run(() =>
            {
                using var uow = GetUnitOfWork();
                return uow.Repository.GetBy<UserLogin>(x => x.UserId == userName, new List<Expression<Func<UserLogin, object>>>() { { x => x.UserRole }});
            }, cancellationToken);
        }

        public async Task<string> GetPasswordHashAsync(UserLogin user, CancellationToken cancellationToken)
        {
            return await Task.FromResult(user.PasswordHash);
        }

        public async Task<bool> HasPasswordAsync(UserLogin user, CancellationToken cancellationToken)
        {
            return await Task.FromResult(user.PasswordHash != null);
        }

        public async Task SetPasswordHashAsync(UserLogin user, string passwordHash, CancellationToken cancellationToken)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));
            user.PasswordHash = passwordHash;
            await UpdateAsync(user, cancellationToken);
        }

        public Task<int> GetAccessFailedCountAsync(UserLogin user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.LastFailedLoginCount);
        }

        public Task<bool> GetLockoutEnabledAsync(UserLogin user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.IsDisabled);
        }

        public Task<DateTimeOffset?> GetLockoutEndDateAsync(UserLogin user, CancellationToken cancellationToken)
        {
            return Task.FromResult(new DateTimeOffset?(DateTimeOffset.MaxValue));
        }

        public async Task<int> IncrementAccessFailedCountAsync(UserLogin user, CancellationToken cancellationToken)
        {
            user.LastFailedLoginCount++;
            user.TotalFailedLoginCount++;
            await UpdateAsync(user, cancellationToken);
            return user.LastFailedLoginCount;
        }

        public Task ResetAccessFailedCountAsync(UserLogin user, CancellationToken cancellationToken)
        {
            user.LastFailedLoginCount = 0;
            return Task.FromResult(user.LastFailedLoginCount);
        }

        public Task SetLockoutEnabledAsync(UserLogin user, bool enabled, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(0);
        }

        public async Task SetLockoutEndDateAsync(UserLogin user, DateTimeOffset? lockoutEnd, CancellationToken cancellationToken)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.IsDisabled = true;
            await UpdateAsync(user, cancellationToken);
        }

        public void Dispose()
        {
        }

        public async Task AddToRoleAsync(UserLogin user, string roleName, CancellationToken cancellationToken)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));
            if (string.IsNullOrWhiteSpace(roleName)) throw new ArgumentException("roleName");

            var role = await Task.Run(() =>
            {
                using var uow = GetUnitOfWork();
                return uow.Repository.GetBy<UserRole>(x => x.Code == roleName);
            }, cancellationToken);

            if (role == null) throw new InvalidOperationException("Role not found");

            user.UserRole = role;
            user.UserRoleId = role.Id;

            await UpdateAsync(user, cancellationToken);
        }

        public async Task<IList<string>> GetRolesAsync(UserLogin user, CancellationToken cancellationToken)
        {
            var result = new List<string>();
            if (user.UserRole == null) return result;
            result.Add(user.UserRole.Code);
            return result;
        }

        public async Task<bool> IsInRoleAsync(UserLogin user, string roleName, CancellationToken cancellationToken)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));
            if (string.IsNullOrWhiteSpace(roleName)) throw new ArgumentException("roleName");

            var role = await Task.Run(() =>
            {
                using var uow = GetUnitOfWork();
                return uow.Repository.GetBy<UserRole>(x => x.Code == roleName);
            }, cancellationToken);

            return user.UserRole.Id == role?.Id; // .Any(userRole => userRole.RoleId == role.Id);
        }

        public async Task RemoveFromRoleAsync(UserLogin user, string roleName, CancellationToken cancellationToken)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));
            if (string.IsNullOrWhiteSpace(roleName)) throw new ArgumentException("roleName");
        }

        public Task SetEmailAsync(UserLogin user, string email, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<string> GetEmailAsync(UserLogin user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user == null) throw new ArgumentNullException(nameof(user));

            return await Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(UserLogin user, CancellationToken cancellationToken)
        {
            return Task.FromResult(true);
        }

        public async Task SetEmailConfirmedAsync(UserLogin user, bool confirmed, CancellationToken cancellationToken)
        {
            return;
        }

        public async Task<UserLogin> FindByEmailAsync(string email, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                using var uow = GetUnitOfWork();
                return uow.Repository.GetBy<UserLogin>(x => x.Email == email);
            }, cancellationToken);
        }

        public async Task<string> GetUserIdAsync(UserLogin user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user == null) throw new ArgumentNullException(nameof(user));

            return await Task.FromResult(user.UserId);
        }

        public async Task<string> GetUserNameAsync(UserLogin user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user == null) throw new ArgumentNullException(nameof(user));

            return await Task.FromResult(user.UserId);
        }

        public async Task SetUserNameAsync(UserLogin user, string userName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<string> GetNormalizedUserNameAsync(UserLogin user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user == null) throw new ArgumentNullException(nameof(user));
            return await Task.FromResult(user.UserId);
        }

        public async Task SetNormalizedUserNameAsync(UserLogin user, string normalizedName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user == null) throw new ArgumentNullException(nameof(user));

        }

        public async Task<IList<UserLogin>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<string> GetNormalizedEmailAsync(UserLogin user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user == null) throw new ArgumentNullException(nameof(user));
            return await Task.FromResult(user.Email);
        }

        public async Task SetNormalizedEmailAsync(UserLogin user, string normalizedEmail, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            if (user == null) throw new ArgumentNullException(nameof(user));
        }
    }
}