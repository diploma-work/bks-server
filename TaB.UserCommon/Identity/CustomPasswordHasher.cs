﻿using TaB.Domain.Entities;
using Microsoft.AspNetCore.Identity;

namespace TaB.UserCommon.Identity
{
    public class CustomPasswordHasher : PasswordHasher<UserLogin>
    {
        private const string Salt = "1BA9403E-6254-49CC-A9F6-71D873515813";

        public override string HashPassword(UserLogin user, string password)
        {
            return CryptographyExtention.CreateHash(password + Salt);
        }

        public override PasswordVerificationResult VerifyHashedPassword(UserLogin user, string hashedPassword, string providedPassword)
        {
            return CryptographyExtention.Verify(providedPassword + Salt, hashedPassword) ? PasswordVerificationResult.Success : PasswordVerificationResult.Failed;
        }
    }
}