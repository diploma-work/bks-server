namespace TaB.UserCommon
{
    public class AccountException : Exception
    {
        public AccountException(string message) : base(message)
        {
        }
    }
}