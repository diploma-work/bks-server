﻿using System.Linq.Expressions;
using TaB.Domain.Consts;
using TaB.Domain.Entities;
using TaB.SharedKernel.Extensions;
using TaB.SharedKernel.Query;
using TaB.UserCommon.Filters;

namespace TaB.UserCommon.Queries
{
    public class UserFilterQuery : SortedQuery<UserLogin>
    {
        public string FreeText { get; set; }
        public UserFilterInfo? Data { get; set; } 

        public override Expression<Func<UserLogin, bool>> GetExpression()
        {
            var filter = base.GetExpression();

            filter = filter.And(x => x.UserId != SystemConst.SystemUser /*&& x.UserRole.Code != "User"*/);

            if (!string.IsNullOrEmpty(FreeText))
                FreeText.Split().ForEach(term =>
                {
                    filter = filter.And(c => c.FirstName.Contains(term) || c.UserId.Contains(term) || c.LastName.Contains(term)) ;
                });

            if (Data == null) return filter;

            if (!Data.RolesIds.IsEmpty())
                filter = filter.And(x => Data.RolesIds!.Contains(x.UserRoleId));

            if (Data.DateFrom.HasValue)
                filter = filter.And(x => x.LastLoginTime >= Data.DateFrom);

            if (Data.DateTo.HasValue)
                filter = filter.And(x => x.LastLoginTime <= Data.DateTo);

            return filter;
        }

        public override Func<IQueryable<UserLogin>, IOrderedQueryable<UserLogin>> GetSortingExpression()
        {
            return SortColumn switch
            {
                "Id" => item => item.ApplyOrder(x => x.Id, IsAscending),
                "Role" => item => item.ApplyOrder(x => x.UserRole.Name, IsAscending),
                "GivenName" => item => item.ApplyOrder(x => x.FirstName, IsAscending),
                _ => item => item.ApplyOrder(SortColumn, IsAscending)
            };
        }

        public override List<Expression<Func<UserLogin, object>>> GetIncludes()
        {
            var @base =  base.GetIncludes();
            @base.Add(x => x.UserRole);
            return @base;
        }
    }
}