using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Ardalis.ApiEndpoints;
using AutoMapper;
using FluentValidation;
using TaB.Application;
using TaB.Domain.Entities;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace TaB.UserCommon.Endpoints.Token
{
    public class TokenRequest : IRequest<TokenResponse>
    {
        [JsonProperty("grant_type")]
        [FromForm(Name = "grant_type")]
        public string GrantType { get; set; }

        [JsonProperty("client_id")]
        [FromForm(Name = "client_id")]
        public Guid ClientId { get; set; }

        [JsonProperty("username")]
        [FromForm(Name = "username")]
        public string? Username { get; set; }

        [JsonProperty("password")]
        [FromForm(Name = "password")]
        public string? Password { get; set; }

        [JsonProperty("refresh_token")]
        [FromForm(Name = "refresh_token")]
        public string? RefreshToken { get; set; }
    }

    public class TokenResponse
    {
        [JsonProperty(".expires")]
        public DateTime Expires { get; set; }

        [JsonProperty(".issued")]
        public string Issued { get; set; }

        [JsonProperty("expires_in")]
        public int TokenLifetimeInSec { get; set; }

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }

    public class TokenRequestValidation : AbstractValidator<TokenRequest>
    {
        public TokenRequestValidation()
        {
            RuleFor(x => x.GrantType)
                .Must(x => x == GrantTypes.Password || x == GrantTypes.RefreshToken || x == GrantTypes.Identity)
                .WithMessage("Unsupported grant type");
        }
    }

    public class Token : BaseAsyncEndpoint<TokenRequest, TokenResponse>
    {
        private readonly IMediator _mediator;

        public Token(IMediator mediator)
        {
            _mediator = mediator;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost("token")]
        [Consumes("application/x-www-form-urlencoded")]
        public override async Task<ActionResult<TokenResponse>> HandleAsync([FromForm] TokenRequest request, CancellationToken cancellationToken = new())
        {
            return await _mediator.Send(request, cancellationToken);
        }
    }

    public class TokenRequestHandler : IRequestHandler<TokenRequest, TokenResponse>
    {
        private readonly ILogger<TokenRequestHandler> _logger;
        private readonly IMapper _mapper;
        private readonly UserManager<UserLogin> _userManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly SignInManager<UserLogin> _signInManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDateTime _dateTime;
        private readonly AuthOptions _authOptions;
        private readonly Dictionary<string, string> _roleMapping;

        public TokenRequestHandler(
            ILogger<TokenRequestHandler> logger,//+
            IMapper mapper,
            UserManager<UserLogin> userManager,
            RoleManager<UserRole> roleManager,
            SignInManager<UserLogin> signInManager,
            IHttpContextAccessor httpContextAccessor,
            IDateTime dateTime,
            IOptions<AuthOptions> authOptions,
            IOptions<Dictionary<string, string>> roleMappingOptions
        )
        {
            _logger = logger;
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _httpContextAccessor = httpContextAccessor;
            _dateTime = dateTime;
            _authOptions = authOptions.Value;
            _roleMapping = roleMappingOptions.Value;
        }

        public async Task<TokenResponse> Handle(TokenRequest request, CancellationToken cancellationToken)
        {
            return request.GrantType switch
            {
                GrantTypes.Password => await LoginByPassword(request),
                GrantTypes.Identity => await LoginByIdentity(request),
                GrantTypes.RefreshToken => await RefreshToken(request),
                _ => throw new BadRequestException("invalid_grant", "Unsupported grant type")
            };
        }

        private async Task<TokenResponse> LoginByPassword(TokenRequest request)
        {
            var user = await _userManager.FindByIdAsync(request.Username);
            if (user == null)
            {
                throw new BadRequestException("invalid_grant", "Wrong login or password");
            }

            //if (!_authOptions.AllowedRoles.Any(x => x.Equals(user.UserRole.Code, StringComparison.OrdinalIgnoreCase)))
            //{
            //    throw new ExceptionBase(System.Net.HttpStatusCode.Forbidden, "You don't have permission");
            //}

            // Check that the user can sign in and is not locked out.
            // If two-factor authentication is supported, it would also be appropriate to check that 2FA is enabled for the user
            if (!await _signInManager.CanSignInAsync(user) || (_userManager.SupportsUserLockout && await _userManager.IsLockedOutAsync(user)) || user.IsDisabled)
            {
                throw new BadRequestException("invalid_grant", "The user name is locked");
            }

            if (!await _userManager.CheckPasswordAsync(user, request.Password))
            {
                await _userManager.AccessFailedAsync(user);
                throw new BadRequestException("invalid_grant", "Wrong login or password");
            }

            // The user is now validated, so reset lockout counts, if necessary
            if (_userManager.SupportsUserLockout)
            {
                await _userManager.ResetAccessFailedCountAsync(user);
            }

            user.LastLoginTime = _dateTime.Now;
            user.TotalSuccessLoginCount += 1;
            await _userManager.UpdateAsync(user);

            // Create the principal
            var principal = await _signInManager.CreateUserPrincipalAsync(user);

            return GenerateToken(principal, user);
        }

        private async Task<TokenResponse> LoginByIdentity(TokenRequest request)
        {
            if (request.ClientId == Guid.Empty)
            {
                throw new BadRequestException("invalid_grant", "Invalid client_id");
            }

            _logger.LogInformation("[client_id = {0}]", request.ClientId);

            var user = await _userManager.FindByNameAsync(request.ClientId.ToString());
            var userRole = _mapper.Map<UserRole>(await _roleManager.FindByNameAsync("User"));

            if (user == null)
            {
                // client_id not found - create new record
                var createResult = await _userManager.CreateAsync(new UserLogin
                {
                    CreateTime = _dateTime.Now,
                    UserId = request.ClientId.ToString(),
                    LastLoginTime = _dateTime.Now,
                    PasswordHash = "$2a$12$en7",
                    UserRoleId = userRole.Id,
                    UserName = request.ClientId.ToString(),
                    TotalSuccessLoginCount = 1,
                    Email = string.Empty,
                    FirstName = string.Empty,
                    LastName = string.Empty
                });

                if (!createResult.Succeeded)
                {
                    var errors = string.Join(Environment.NewLine, createResult.Errors);
                    throw new BadRequestException("invalid_grant", errors);
                }

                user = await _userManager.FindByNameAsync(request.ClientId.ToString());
            }
            else
            {
                user.LastLoginTime = _dateTime.Now;
                user.TotalSuccessLoginCount += 1;
                var updateResult = await _userManager.UpdateAsync(user);
            }

            var principal = await _signInManager.CreateUserPrincipalAsync(user);

            return GenerateToken(principal, user);
        }

        private async Task<TokenResponse> RefreshToken(TokenRequest request)
        {
            if (string.IsNullOrEmpty(request.RefreshToken)) throw new BadRequestException("invalid_grant", "Bad refresh_token format");

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = true,
                ValidAudience = "Refresh",
                ValidateIssuer = true,
                ValidIssuer = _authOptions.Issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _authOptions.GetSymmetricSecurityKey(),
                ValidateLifetime = true
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                var principal = tokenHandler.ValidateToken(request.RefreshToken, tokenValidationParameters, out var securityToken);
                var jwtSecurityToken = securityToken as JwtSecurityToken;
                if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                    StringComparison.InvariantCultureIgnoreCase))
                    throw new SecurityTokenException("Invalid token");

                var user = await _userManager.FindByIdAsync(principal.Identity.Name);
                if (user == null || user.IsDisabled)
                {
                    throw new BadRequestException("invalid_grant", "Wrong login or the user name is locked");
                }

                return GenerateToken(principal, user);
            }
            catch (Exception ste) when (ste is SecurityTokenExpiredException)
            {
                throw new BadRequestException("invalid_grant", "The token has expired. Please login.");
            }

            catch (Exception ste)
            {
                throw new BadRequestException("invalid_grant", ste.Message);
            }
        }

        private TokenResponse GenerateToken(ClaimsPrincipal principal, UserLogin user)
        {
            var now = _dateTime.Now;
            var expires = now.Add(TimeSpan.FromMinutes(_authOptions.AccessTokenExpireTimeSpanInMinutes));

            var jwt = new JwtSecurityToken(
                _authOptions.Issuer,
                _authOptions.Audience,
                notBefore: now,
                claims: principal.Claims,
                expires: expires,
                signingCredentials: new SigningCredentials(_authOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));

            var jwtAccessToken = new JwtSecurityTokenHandler().WriteToken(jwt);

            var expiresRefresh = now.Add(TimeSpan.FromMinutes(_authOptions.RefreshTokenExpireTimeSpanInMinutes));
            var refreshJwt = new JwtSecurityToken(
                _authOptions.Issuer,
                "Refresh",
                notBefore: now,
                claims: principal.Claims,
                expires: expiresRefresh,
                signingCredentials: new SigningCredentials(_authOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));

            var jwtRefreshToken = new JwtSecurityTokenHandler().WriteToken(refreshJwt);

            return new TokenResponse
            {
                Expires = expires,
                Issued = _authOptions.Issuer,
                AccessToken = jwtAccessToken,
                Email = user.Email,
                RefreshToken = jwtRefreshToken,
                TokenLifetimeInSec = _authOptions.AccessTokenExpireTimeSpanInMinutes * 60 - 1,
                Role = user.UserRole.Code,
                TokenType = JwtBearerDefaults.AuthenticationScheme,
                UserName = user.UserId,
                FirstName =user.FirstName,
                LastName =user.LastName
            };
        }
    }
}
