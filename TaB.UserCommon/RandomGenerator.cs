﻿using System;
using System.Linq;

namespace TaB.UserCommon
{
    public class RandomGenerator
    {
        public enum CharsSet { 
            Numbers,
            AlphabetLowChars,
            AlphabetUpChars,
            Punctuation
        }

        public static string BuildRandomString(int length)
        {
            return BuildRandomString(length, new CharsSet[0]);
        }

        public static string BuildRandomString(int length, CharsSet[] allowedChars)
        {
            const string NUMBERS = "0123456789";
            const string ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string PUNCTUATION = "!@#$%^&*()_+";

            var chars = "";
            if (allowedChars.Length == 0 || allowedChars.Contains(CharsSet.Numbers))
            {
                chars += NUMBERS;
            }

            if (allowedChars.Length == 0 || allowedChars.Contains(CharsSet.AlphabetLowChars))
            {
                chars += ALPHABET.ToLower() + ALPHABET.ToLower() + ALPHABET.ToLower();
            }

            if (allowedChars.Length == 0 || allowedChars.Contains(CharsSet.AlphabetUpChars))
            {
                chars += ALPHABET.ToUpper() + ALPHABET.ToUpper() + ALPHABET.ToUpper();
            }

            if (allowedChars.Length == 0 || allowedChars.Contains(CharsSet.Punctuation))
            {
                chars += PUNCTUATION;
            }

            var stringChars = new char[length];
            var random = new Random();
            var chartsLength = stringChars.Length;

            for (int i = 0; i < chartsLength - 4; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            if (chartsLength >= 4)
                stringChars[chartsLength - 4] = ALPHABET.ToLower().ToCharArray()[random.Next(ALPHABET.Length)];
            if (chartsLength >= 3)
                stringChars[chartsLength - 3] = NUMBERS.ToCharArray()[random.Next(NUMBERS.Length)];
            if (chartsLength >= 2)
                stringChars[chartsLength - 2] = ALPHABET.ToUpper().ToCharArray()[random.Next(ALPHABET.Length)];
            if (chartsLength >= 1)
                stringChars[chartsLength - 1] = PUNCTUATION.ToCharArray()[random.Next(PUNCTUATION.Length)];

            return new String(stringChars);
        }
    }
}
