﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using FluentValidation;
using TaB.Application;
using TaB.Domain.Entities;
using TaB.Persistence;
using TaB.SharedKernel.Models;
using TaB.UserCommon.Endpoints.Token;
using TaB.UserCommon.Identity;
using TaB.UserCommon.Services;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace TaB.UserCommon
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddIdentity(this IServiceCollection services, IConfiguration configuration)
        {
            var authOptionsSection = configuration.GetSection("AuthOptions");

            services.AddOptions();
            services.Configure<AuthOptions>(authOptionsSection);
            services.Configure<Dictionary<string, string>>(configuration.GetSection("RoleMapping"));

            ////services.AddAutoMapper(Assembly.GetExecutingAssembly());
            //services.AddMediatR(Assembly.GetExecutingAssembly());

            //services.AddTransient<IRequestHandler<TokenRequest, TokenResponse>, TokenRequestHandler>();

            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());

            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPerformanceBehavior<,>));
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehavior<,>));

            services.AddScoped<IPasswordHasher<UserLogin>, CustomPasswordHasher>();

            services.AddIdentity<UserLogin, UserRole>(options =>
                {
                    // Basic built in validations
                    options.Password.RequireDigit = true;
                    options.Password.RequireLowercase = true;
                    options.Password.RequireNonAlphanumeric = true;
                    options.Password.RequireUppercase = true;
                    options.Password.RequiredLength = 12;

                    options.Lockout.MaxFailedAccessAttempts = 5;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>();


            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    var authOptions = authOptionsSection.Get<AuthOptions>();
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = authOptions.Issuer,

                        ValidateAudience = true,
                        ValidAudience = authOptions.Audience,

                        ValidateLifetime = true,

                        IssuerSigningKey = authOptions.GetSymmetricSecurityKey(),
                        ValidateIssuerSigningKey = true,

                        ClockSkew = TimeSpan.Zero
                    };
                    options.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = context =>
                        {
                            context.NoResult();
                            context.Response.StatusCode = (int) HttpStatusCode.Unauthorized;
                            context.Response.ContentType = "application/json";
                            context.Response
                                .WriteAsync(JsonConvert.SerializeObject(new Error("error", "Token expired. Please, authorize.")))
                                .Wait();
                            return Task.CompletedTask;
                        }
                    };
                });

            services.AddTransient<IUserStore<UserLogin>, UserStore>();
            services.AddTransient<IRoleStore<UserRole>, RoleStore>();
            services.AddTransient<IUserService, UserService>();

            return services;
        }

        public static void UseIdentity(this IApplicationBuilder app)
        {
            app.UseAuthentication();
            app.UseAuthorization();
        }
    }
}
