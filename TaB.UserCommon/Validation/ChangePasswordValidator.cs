﻿using FluentValidation;
using TaB.UserCommon.Models;

namespace TaB.UserCommon.Validation
{
    public class ChangePasswordValidator : AbstractValidator<ChangePasswordModel>
    {
        public ChangePasswordValidator()
        {
            RuleFor(item => item.UserId)
                .NotEmpty().NotNull();

            RuleFor(item => item.NewPassword)
                .Length(12, 128).Matches(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z])")
                .WithMessage("New password should have at least 1 letter, 1 uppercase letter, 1 number and 1 special symbol");

            RuleFor(item => item.OldPassword)
                .NotEmpty().NotNull();
        }
    }
}