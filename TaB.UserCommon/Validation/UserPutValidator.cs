﻿using FluentValidation;
using TaB.UserCommon.Services;

namespace TaB.UserCommon.Validation
{
    public class UserPutValidator : UserPostValidator
    {

        public UserPutValidator(IUserService userService) : base(userService)
        {
            RuleFor(item => item.Password).NotNull().WithMessage("Password is empty");

            RuleFor(item => item.UserId)
                .NotEmpty().Length(3, 60)
                .Must(LoginUnique)
                .WithMessage("Such user already exists. Please select another Login.");

            RuleFor(item => item.Email)
                .EmailAddress()
                .Must(EmailUnique)
                .WithMessage("Such user already exists. Please select another Email.");
        }


        private bool LoginUnique(string userId)
        {
            var result = UserService.GetBy(x => x.UserId == userId);
            return result == null;
        }

        private bool EmailUnique(string email)
        {
            var result = UserService.GetBy(x => x.Email == email);
            return result == null;
        }
    }
}