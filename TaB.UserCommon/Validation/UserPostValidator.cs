﻿using System.Linq;
using FluentValidation;
using FluentValidation.Validators;
using TaB.UserCommon.Models;
using TaB.UserCommon.Services;

namespace TaB.UserCommon.Validation
{
    public class UserPostValidator : AbstractValidator<PostUserInfo>
    {
        protected readonly IUserService UserService;

        public UserPostValidator(IUserService userService)
        {
            UserService = userService;

            RuleFor(item => item.GivenName).NotNull().WithMessage("Please, choose the value for given name!");
            RuleFor(item => item.Surname).NotNull().WithMessage("Please, choose the value for surname!");
            RuleFor(item => item.Role).NotNull().Must(RoleExists).WithMessage("Role is required");
            RuleFor(item => item.Email).EmailAddress(EmailValidationMode.AspNetCoreCompatible)
                .Must(EmailIsUnique).WithMessage("Email is exists");
        }

        private bool EmailIsUnique(PostUserInfo postUserInfo, string email)
        {
            return UserService.GetBy(x => x.Email == email && x.UserId != postUserInfo.UserId) == null;
        }

        private bool RoleExists(string role)
        {
            return UserService.GetRoles().FirstOrDefault(x => x.Code == role) != null;
        }
    }
}