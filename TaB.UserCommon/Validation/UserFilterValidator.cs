﻿using TaB.SharedKernel.Validation.Filters;
using TaB.UserCommon.Filters;
using TaB.UserCommon.Models;

namespace TaB.UserCommon.Validation
{
    public class UserFilterValidator : FilterValidatorBase<UserFilter, UserInfo>
    {
    }
}