﻿using System.ComponentModel.DataAnnotations;

namespace TaB.UserCommon.Models
{
    public class ResetPasswordModel
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [MinLength(12)]
        [MaxLength(128)]
        public string Password { get; set; }

        public override string ToString()
        {
            return $"UserId {UserId}, Password.Length {Password?.Length ?? 0}";
        }
    }
}
