﻿namespace TaB.UserCommon.Models
{
    public class RoleInfo
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}