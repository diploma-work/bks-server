﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TaB.UserCommon.Models
{
    /// <summary>
    /// Model for change password
    /// </summary>
    public class ChangePasswordModel
    {
        /// <summary>
        /// User login
        /// </summary>
        [JsonProperty("Login")]
        public string UserId { get; set; }

        /// <summary>
        /// Old password
        /// </summary>
        [JsonProperty("OldPwd")]
        [MaxLength(128)]
        public string OldPassword { get; set; }

        /// <summary>
        /// New password
        /// </summary>
        [JsonProperty("NewPwd")]
        [MinLength(12)]
        [MaxLength(128)]
        public string NewPassword { get; set; }

        public override string ToString()
        {
            return
                $"UserId {UserId}, OldPassword.Length {OldPassword?.Length ?? 0}, NewPassword.Length {NewPassword?.Length ?? 0}, match {OldPassword == NewPassword}";
        }

    }
}