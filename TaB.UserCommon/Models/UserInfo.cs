﻿using TaB.SharedKernel.Attributes;
using Newtonsoft.Json;
using TaB.SharedKernel.Interfaces;
using AutoMapper;

namespace TaB.UserCommon.Models
{
    public class PostUserInfo
    {
        [JsonIgnore]
        public int Id { get; set; }

        [Orderable]
        public string UserId { get; set; }

        public string Password { get; set; }

        [Orderable]
        public string Role { get; set; }

        [Orderable]
        public bool IsDisabled { get; set; }

        [Orderable]
        public string Email { get; set; }

        [Orderable]
        public string GivenName { get; set; }

        [Orderable]
        public string Surname { get; set; }
    }

    public class PutUserInfo
    {
        [JsonIgnore]
        public int Id { get; set; }

        [Orderable]
        public string UserId { get; set; }

        public string Password { get; set; }

        [Orderable]
        public string Role { get; set; }

        [Orderable]
        public bool IsDisabled { get; set; }

        [Orderable]
        public string Email { get; set; }

        [Orderable]
        public string GivenName { get; set; }

        [Orderable]
        public string Surname { get; set; }

        [Orderable]
        public string SecretPhrase { get; set; }
    }

    public class UserInfo : PostUserInfo
    {
        [Orderable]
        public DateTime? CreateTime { get; set; }

        [Orderable]
        public DateTime? LastLoginTime { get; set; }

        [Orderable]
        public bool HaveSecretPhrase { get; set; }
    }

    
}