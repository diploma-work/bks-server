﻿using TaB.SharedKernel.Filters;

namespace TaB.UserCommon.Filters
{
    public class UserFilter : SortedPagedFilter
    {
        public UserFilterInfo? Data { get; set; }

        public UserFilter()
        {
            SortColumn = "UserId";
        }
    }

    public class UserFilterInfo
    {
        public List<int>? RolesIds { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}