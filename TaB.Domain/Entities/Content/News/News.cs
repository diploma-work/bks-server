﻿using TaB.Domain.Enums;

namespace TaB.Domain.Entities.Content.News
{
    public class News : Content
    {
        public News()
        {
            ContentTypeId = (int)ContentType.News;
        }

        public NewsMetadata Metadata { get; set; } = default!;
        public List<Like> Likes { get; set; } = default!;
        public List<Comment> Comments { get; set; } = default!;
    }
}
