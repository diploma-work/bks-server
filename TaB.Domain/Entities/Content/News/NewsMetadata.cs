﻿namespace TaB.Domain.Entities.Content.News
{
    public class NewsMetadata : ContentMetadata
    {
        public News News { get; set; } = default!;
    }
}
