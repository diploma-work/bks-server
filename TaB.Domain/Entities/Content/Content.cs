﻿using TaB.Domain.Enums;

namespace TaB.Domain.Entities.Content
{
    public abstract class Content
    {
        public int Id { get; set; }
        public int ContentTypeId { get; set; }
        public StatusEnum Status { get; set; } = default!;

        //public ContentMetadata BaseMetadata { get; set; } = default!;
        public ICollection<File> Files { get; set; } = default!;
        public ICollection<FilesInContent> ContentFiles { get; set; } = default!;
    }
}
