﻿namespace TaB.Domain.Entities.Content
{
    public abstract class ContentMetadata
    {
        public int ContentId { get; set; }
        public string Title { get; set; } = default!;
        public string ContentString { get; set; } = default!;
        public virtual Content Content { get; set; } = default!;
    }
}
