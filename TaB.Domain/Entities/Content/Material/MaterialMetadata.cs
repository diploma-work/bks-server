﻿namespace TaB.Domain.Entities.Content.Material
{
    public class MaterialMetadata : ContentMetadata
    {
        public Material Material { get; set; } = default!;
    }
}
