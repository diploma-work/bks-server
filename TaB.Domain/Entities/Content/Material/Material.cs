﻿using TaB.Domain.Enums;

namespace TaB.Domain.Entities.Content.Material
{
    public class Material : Content
    {
        public Material()
        {
            ContentTypeId = (int)ContentType.Material;
        }

        public MaterialMetadata Metadata { get; set; } = default!;
    }
}
