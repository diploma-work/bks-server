﻿namespace TaB.Domain.Entities
{
    public class MasterData
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
    }
}
