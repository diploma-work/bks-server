﻿using Microsoft.AspNetCore.Identity;
using TaB.Domain.Entities.Testing;

namespace TaB.Domain.Entities
{
    public class UserLogin : IdentityUser<int>
    {
        public override int Id { get; set; }
        public string UserId { get; set; } = default!;
        public override string PasswordHash { get; set; } = default!;
        public DateTime CreateTime { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public int LastFailedLoginCount { get; set; }
        public bool IsDisabled { get; set; }
        public int TotalSuccessLoginCount { get; set; }
        public int TotalFailedLoginCount { get; set; }
        public int UserRoleId { get; set; }
        public override string Email { get; set; } = string.Empty;
        public string FirstName { get; set; } = default!;
        public string LastName { get; set; } = default!;

        public bool HaveSecretPhrase { get; set; } = false;

        public virtual UserRole UserRole { get; set; } = default!;
        public ICollection<TestResult> TestResults { get; set; } = default!;
        public ICollection<Like> Likes { get; set; } = default!;
        public ICollection<Comment> Comments { get; set; } = default!;
    }
}
