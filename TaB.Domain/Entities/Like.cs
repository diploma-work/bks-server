﻿using TaB.Domain.Entities.Content.News;

namespace TaB.Domain.Entities
{
    public class Like
    {
        public int ContentId { get; set; }
        public int UserLoginId { get; set; }

        public UserLogin User { get; set; } = default!;
        public News News { get; set; } = default!;
    }
}
