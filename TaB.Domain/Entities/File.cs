﻿using TaB.Domain.Enums;

namespace TaB.Domain.Entities
{
    public class File
    {
        public int Id { get; set; }
        public FileType FileType { get; set; }
        public long Size { get; set; }
        public string? Name { get; set; }
        public string Link { get; set; } = default!;

        public ICollection<Content.Content> ContentList { get; set; } = default!;
        public ICollection<FilesInContent> ContentFiles { get; set; } = default!;
    }
}
