﻿using TaB.Domain.Entities.Content.News;

namespace TaB.Domain.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public int ContentId { get; set; }
        public int UserLoginId { get; set; }
        public int? ParentCommentId { get; set; }
        public string Text { get; set; } = default!;

        public Comment? ParrentComment { get; set; }
        public List<Comment>? ChildrenComments { get; set; }
        public News News { get; set; } = default!;
        public UserLogin User { get; set; } = default!;

    }
}
