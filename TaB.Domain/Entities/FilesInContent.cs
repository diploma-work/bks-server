﻿namespace TaB.Domain.Entities
{
    public class FilesInContent
    {
        public int FilesId { get; set; }
        public int ContentId { get; set; }
        public Content.Content Content { get; set; } = default!;
        public File File { get; set; } = default!;
    }
}
