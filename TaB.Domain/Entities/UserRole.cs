﻿using Microsoft.AspNetCore.Identity;

namespace TaB.Domain.Entities
{
    public class UserRole : IdentityRole<int>
    {
        public override int Id { get; set; } // Id (Primary key)
        public override string Name { get; set; } = default!; // Name (length: 96)
        public string Code { get; set; } = default!; // Code (length: 32)
        public string Comment { get; set; } = default!; // Comment (length: 1024)

        public virtual ICollection<UserLogin> UserLogin { get; set; }

        public UserRole()
        {
            UserLogin = new List<UserLogin>();
        }
    }
}
