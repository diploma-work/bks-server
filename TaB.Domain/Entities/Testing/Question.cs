﻿namespace TaB.Domain.Entities.Testing
{
    public class Question
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public string Text { get; set; } = default!;

        public Test Test { get; set; } = default!;
        public ICollection<Answer> Answers { get; set; } = default!;
    }
}
