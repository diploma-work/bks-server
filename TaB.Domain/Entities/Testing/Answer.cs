﻿namespace TaB.Domain.Entities.Testing
{
    public class Answer
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Text { get; set; } = default!;
        public bool IsCorrect { get; set; }

        public Question Question { get; set; } = default!;
    }
}
