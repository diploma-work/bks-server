﻿namespace TaB.Domain.Entities.Testing
{
    public class Test
    {
        public int Id { get; set; }
        public string Topic { get; set; } = default!;
        public int QuestionsCount { get; set; }

        public ICollection<Question> Questions { get; set; } = default!;
        public ICollection<TestResult> TestResults { get; set; } = default!;
    }
}
