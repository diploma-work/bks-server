﻿namespace TaB.Domain.Entities.Testing
{
    public class TestResult
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public int UserLoginId { get; set; }
        public int CorrectQuestionsCount { get; set; }
        public string AnswersText { get; set; } = default!;

        public Test Test { get; set; } = default!;
        public UserLogin User { get; set; } = default!;
    }
}
