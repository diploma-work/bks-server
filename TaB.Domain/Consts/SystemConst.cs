﻿namespace TaB.Domain.Consts
{
    public static class SystemConst
    {
        public const string SystemUser = "System";
        public const string SystemRole = "System";
    }
}