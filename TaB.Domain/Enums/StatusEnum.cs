﻿namespace TaB.Domain.Enums
{
    public enum StatusEnum : int
    {
        Active = 1,
        Archived = 2
    }
}
