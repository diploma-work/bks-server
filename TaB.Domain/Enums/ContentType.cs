﻿namespace TaB.Domain.Enums
{
    public enum ContentType : int
    {
        News = 1,
        Material = 2
    }
}
