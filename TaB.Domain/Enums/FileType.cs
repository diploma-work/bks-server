﻿namespace TaB.Domain.Enums
{
    public enum FileType : int
    {
        Unknown = 0,
        Pdf = 1,
        Image = 2
    }
}
