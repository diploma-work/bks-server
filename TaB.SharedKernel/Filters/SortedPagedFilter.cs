﻿using BU.SharedKernel;

namespace TaB.SharedKernel.Filters
{
    public abstract class SortedPagedFilter : IPagedFilter, ISortedFilter
    {
        protected SortedPagedFilter()
        {
            PageNumber = Constants.DefaultPageNumber;
            PageSize = Constants.DefaultPageSize;
            SortColumn = Constants.DefaultSortColumn;
            IsAscending = Constants.DefaultIsAscending;
        }

        public string? FreeText { get; set; } = default!;

        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public string SortColumn { get; set; }

        public bool IsAscending { get; set; }
    }
}
