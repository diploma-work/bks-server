﻿namespace TaB.SharedKernel
{
    public class StorageOptions
    {
        public string DocumentsFolder { get; set; } = default!;
        public string UploadFolder { get; set; } = default!;
        public string TempFolder { get; set; } = default!;
        public List<string> AllowedFileExtensions { get; set; } = default!;
        public string SecretPhrase { get; set; } = default!;
    }
}
