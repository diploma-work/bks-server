﻿namespace TaB.SharedKernel.Helpers
{
    public class EqualityComparer<T> : IEqualityComparer<T>
    {
        private readonly Func<T, int> _hashCode;
        private readonly Func<T, T, bool> _comparer;

        private EqualityComparer(Func<T, T, bool> comparer, Func<T, int> hashCode)
        {
            _hashCode = hashCode;
            _comparer = comparer;
        }

        public bool Equals(T? x, T? y) => x != null && y != null && _comparer(x, y);

        public int GetHashCode(T obj) => _hashCode(obj);

        public static EqualityComparer<T> Create(Func<T, T, bool> comparer, Func<T, int> hashCode) => new(comparer, hashCode);
    }
}