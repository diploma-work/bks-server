﻿using FluentValidation;
using TaB.SharedKernel.Attributes;
using TaB.SharedKernel.Filters;

namespace TaB.SharedKernel.Validation.Filters
{
    public class FilterValidatorBase<T, T1> : AbstractValidator<T> where T : SortedPagedFilter
    {
        private static readonly List<string> Columns = (from p in typeof(T1).GetProperties()
            let attr = (OrderableAttribute[])p.GetCustomAttributes(typeof(OrderableAttribute), true)
            let name = attr.Length == 1 ? (!string.IsNullOrWhiteSpace(attr.First().Name) ? attr.First().Name : p.Name) : null
            where !string.IsNullOrWhiteSpace(name)
            select name//.ToLowerInvariant()
            ).ToList();

        protected FilterValidatorBase()
        {
            RuleFor(item => item.SortColumn)
                .Must(CheckSortedColumn)
                .WithMessage($"SortedColumn invalid value (Accept only: {string.Join(", ", Columns)})");
        }

        private bool CheckSortedColumn(string sortColumn)
        {
            return Columns.FirstOrDefault(x => x.Equals(sortColumn, StringComparison.OrdinalIgnoreCase)) != null;
        }
    }
}