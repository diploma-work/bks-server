﻿namespace TaB.SharedKernel.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class OrderableAttribute : Attribute
    {
        public string Name { get; set; } = default!;
        //public Type? KeySelector { get; set; }
    }
}
