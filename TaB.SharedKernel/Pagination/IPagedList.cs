﻿namespace TaB.SharedKernel.Pagination
{
    public interface IPagedList<T>
    {
        PagingInfo PagingInfo { get; }

        List<T> Result { get; }
    }
}
