﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using TaB.SharedKernel.Filters;
using TaB.SharedKernel.Pagination;
using TaB.SharedKernel.Query;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace TaB.SharedKernel.Extensions
{
    public static class QueryableExtensions
    {
        public static IPagedList<T> ApplyPaging<T>(this IQueryable<T> items, int pageNumber, int pageSize)
        {
            return new PagedList<T>(items, pageNumber, pageSize);
        }

        public static IPagedList<T> ApplyPaging<T, TPagedFilter>(this IQueryable<T> items, TPagedFilter filter)
            where TPagedFilter : IPagedFilter
        {
            return new PagedList<T>(items, filter.PageNumber, filter.PageSize);
        }

        public static IPagedList<TDestination> ProjectTo<TSource, TDestination>(this IPagedList<TSource> source, IMapper mapper)
        {
            return new PagedList<TDestination>(mapper.Map<IEnumerable<TSource>, IEnumerable<TDestination>>(source.Result), source.PagingInfo);
        }

        public static TDestination? ProjectTo<TSource, TDestination>(this TSource? source, IMapper mapper)
        {
            return source == null ? default : mapper.Map<TSource, TDestination>(source);
        }

        public static TDestination ProjectTo<TDestination>(this object source, IMapper mapper)
        {
            return mapper.Map<TDestination>(source);
        }

        public static TDestination ProjectTo<TDestination>(this object source, IRuntimeMapper mapper)
        {
            return mapper.Map<TDestination>(source);
        }

        public static async Task<TDestination> ProjectToFirstOrDefaultAsync<TDestination>(this IQueryable source, IMapper mapper, object parameters)
        {
            var firstOrDefaultAsync = await source.ProjectTo<TDestination>(mapper.ConfigurationProvider, parameters).FirstOrDefaultAsync();
            return mapper.Map<TDestination>(firstOrDefaultAsync);
        }

        public static async Task<List<TDestination>> ProjectToListAsync<TDestination>(this IQueryable source, IMapper mapper)
        {
            var listAsync = await source.ProjectTo<TDestination>(mapper.ConfigurationProvider).ToListAsync();
            return mapper.Map<List<TDestination>>(listAsync);
        }

        public static IOrderedQueryable<T> ApplyQuery<T>(this IQueryable<T> items, ISortedQuery<T> query) where T : class
        {
            var result = items.Where(query.GetExpression());
            result = query.GetIncludes().Aggregate(result, (current, include) => current.Include(include));
            return query.GetSortingExpression()(result);
        }

        public static IQueryable<T> ApplyQuery<T>(this IQueryable<T> items, IQuery<T> query) where T : class
        {
            var result = items.Where(query.GetExpression());
            result = query.GetIncludes().Aggregate(result, (current, include) => current.Include(include));
            return result;
        }

        public static TK? MaxOrDefault<T, TK>(this IQueryable<T> enumeration, Func<T, TK> selector)
        {
            return enumeration.Any() ? enumeration.Max(selector) : default;
        }

        public static IOrderedQueryable<T> ApplyOrder<T, TType>(this IQueryable<T> @this, Expression<Func<T, TType>> keySelector, bool isAscending)
        {
            return isAscending ? @this.OrderBy(keySelector) : @this.OrderByDescending(keySelector);
        }

        public static IOrderedQueryable<T> ApplyThen<T, TType>(this IOrderedQueryable<T> @this, Expression<Func<T, TType>> keySelector, bool isAscending)
        {
            return isAscending ? @this.ThenBy(keySelector) : @this.ThenByDescending(keySelector);
        }

        private static readonly MethodInfo orderBy = typeof(Queryable).GetMethods(BindingFlags.Static | BindingFlags.Public).First(x => x.Name == "OrderBy" && x.GetParameters().Length == 2);
        private static readonly MethodInfo orderByDescending = typeof(Queryable).GetMethods(BindingFlags.Static | BindingFlags.Public).First(x => x.Name == "OrderByDescending" && x.GetParameters().Length == 2);

        public static IOrderedQueryable<T> ApplyOrder<T>(this IQueryable<T> @this, string sortColumn, bool isAscending)
        {
            var pi = typeof(T).GetProperties().FirstOrDefault(x => x.Name.Equals(sortColumn, StringComparison.InvariantCultureIgnoreCase));
            if (pi == null) return @this.ApplyOrder("Id", false);
            var selectorParam = Expression.Parameter(typeof(T), "keySelector");
            return (IOrderedQueryable<T>)@this.Provider.CreateQuery<T>(Expression.Call((!isAscending ? orderByDescending : orderBy).MakeGenericMethod(typeof(T), pi.PropertyType), @this.Expression, Expression.Lambda(typeof(Func<,>).MakeGenericType(typeof(T), pi.PropertyType), Expression.Property(selectorParam, pi), selectorParam)));
        }
    }
}
