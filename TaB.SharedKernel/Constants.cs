﻿namespace BU.SharedKernel
{
    public static class Constants
    {
        public const int DefaultPageNumber = 0;
        public const int DefaultPageSize = 10;
        public const string DefaultSortColumn = "Id";
        public const bool DefaultIsAscending = false;
    }
}
