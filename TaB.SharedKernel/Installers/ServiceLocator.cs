﻿using DryIoc;
using DryIoc.Microsoft.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;

namespace TaB.SharedKernel.Installers
{
    public static class ServiceLocator
    {
        private static IContainer _container = default!;

        public static IContainer Configure(IContainer container)
        {
            // Create container instance
            _container = container;

            return _container;
        }

        public static T Resolve<T>()
        {
            return _container.GetServiceProvider().GetService<T>()!;
        }

        public static T Resolve<T>(string name)
        {
            throw new NotImplementedException();
        }
    }
}
