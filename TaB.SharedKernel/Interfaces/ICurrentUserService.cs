﻿using System.Security.Principal;

namespace TaB.SharedKernel.Interfaces
{
    public interface ICurrentUserService
    {
        IPrincipal User { get; }
        string UserId { get; }
        int Id { get; }
        string RoleCode { get; }
    }
}
