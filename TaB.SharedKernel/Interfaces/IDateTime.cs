﻿namespace TaB.SharedKernel.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
