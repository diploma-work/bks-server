﻿using System;
using System.Net;

namespace TaB.SharedKernel.Exceptions
{
    public class ExceptionBase : Exception
    {
        public ExceptionBase(HttpStatusCode statusCode, string message)
            : base(message)
        {
            StatusCode = statusCode;
        }

        public ExceptionBase(HttpStatusCode statusCode, string message, Exception innerException) : base(message, innerException)
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; }
    }
}