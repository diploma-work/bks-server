﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using TaB.Domain.Entities;
using TaB.Domain.Enums;
using TaB.SharedKernel.Interfaces;

namespace TaB.SharedKernel.Models
{
    public class FilesInfo : IMapFrom<Domain.Entities.File>
    {
        public int Id { get; set; }
        public FileType Type { get; set; }
        public string? Name { get; set; } = default!;
        public string Link { get; set; } = default!;

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Domain.Entities.File, FilesInfo>().ReverseMap();

            profile.CreateMap<FilesInfo, FilesInContent>()
                .EqualityComparison((src, dest) => src.Id == dest.FilesId)
                .ForMember(dest => dest.FilesId, opt => opt.MapFrom(src => src.Id));
        }
    }
}
