using TaB.SharedKernel.Extensions;

namespace TaB.SharedKernel.Query
{
    public abstract class SortedQuery<TSource> : Query<TSource>, ISortedQuery<TSource> 
    {
        public string SortColumn { get; set; }
        public bool IsAscending { get; set; }

        protected SortedQuery()
        {
            SortColumn = "Id";
            IsAscending = false;
        }

        public virtual Func<IQueryable<TSource>, IOrderedQueryable<TSource>> GetSortingExpression() => 
            SortColumn switch
            {
                _ => item => item.ApplyOrder(SortColumn, IsAscending)
            };
    }
}