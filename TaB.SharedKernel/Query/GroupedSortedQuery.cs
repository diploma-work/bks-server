using System.Linq.Expressions;

namespace TaB.SharedKernel.Query
{
    public abstract class GroupedSortedQuery<TSource, TKey> : SortedQuery<TSource>, IGroupedSortedQuery<TSource, TKey>
    {
        public abstract (Expression<Func<TSource, TKey>> keySelector, Expression<Func<TKey, IEnumerable<TSource>, TSource>> resultSelector) GetGroupBy();
        public abstract IQueryable<TSource> EmulateGroupBy(IQueryable<TSource> all);
    }
}