using System.Linq.Expressions;

namespace TaB.SharedKernel.Query
{
    public interface IGroupedQuery<TSource, TKey, TResult> : IQuery<TSource>
    {
        (Expression<Func<TSource, TKey>> keySelector, Expression<Func<TKey, IEnumerable<TSource>, TResult>> resultSelector) GetGroupBy();

        /// <summary>
        /// This is a temporary method (Instead of the method above), as a solution to the problem of the EF Core 3.x,
        /// which consists in the fact that at the moment there is no way to make a request
        /// with a grouping and selection of N elements from each group.
        /// https://docs.microsoft.com/en-us/ef/core/what-is-new/ef-core-3.x/breaking-changes#linq-queries-are-no-longer-evaluated-on-the-client
        /// https://github.com/dotnet/efcore/issues/12088
        /// https://github.com/dotnet/efcore/issues/13805
        /// </summary>
        IQueryable<TResult> EmulateGroupBy(IQueryable<TSource> all);
    }
}