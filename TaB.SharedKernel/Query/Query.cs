using System.Linq.Expressions;

namespace TaB.SharedKernel.Query
{
    public abstract class Query<TSource> : IQuery<TSource> 
    {
        public virtual Expression<Func<TSource, bool>> GetExpression()
        {
            Expression<Func<TSource, bool>> filter = uniqueEntity => true;
            return filter;
        }

        public virtual List<Expression<Func<TSource, object>>> GetIncludes()
        {
            return new List<Expression<Func<TSource, object>>>();
        }
    }
}