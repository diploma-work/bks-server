namespace TaB.SharedKernel.Query
{
    public interface IGroupedSortedQuery<TSource, TKey> : ISortedQuery<TSource>, IGroupedQuery<TSource, TKey, TSource>
    {}
}