﻿using System.Linq.Expressions;

namespace TaB.SharedKernel.Query
{
    public interface IQuery<TSource>
    {
        Expression<Func<TSource, bool>> GetExpression();
        List<Expression<Func<TSource, object>>> GetIncludes();
    }
}
