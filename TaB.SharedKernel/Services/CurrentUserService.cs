﻿using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Security.Principal;

namespace BU.SharedKernel.Services
{
    public class CurrentUserService
    {
        private int _id;
        private string _roleCode = string.Empty;

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            UserId = httpContextAccessor.HttpContext?.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            User = httpContextAccessor.HttpContext?.User!;
        }

        public IPrincipal User { get; }
        public string? UserId { get; }

        public int Id
        {
            get
            {
                if (string.IsNullOrEmpty(UserId)) return 0;
                if (_id != 0) return _id;

                FillValues();
                return _id;
            }
        }

        public string RoleCode
        {
            get
            {
                if (string.IsNullOrEmpty(UserId)) return _roleCode;
                if (!string.IsNullOrWhiteSpace(_roleCode)) return _roleCode;

                FillValues();
                return _roleCode;
            }
        }

        private void FillValues()
        {
            //var umr = ServiceLocator.Resolve<IUserService>();
            //var user = umr.GetBy(x => x.UserId == UserId);
            //if (user == null) return;
            //_id = user.Id;
            //_languageId = user.LanguageId ?? 0;
            //_roleCode = user.Role;
        }
    }
}
