﻿using TaB.SharedKernel.Abstractions;

namespace TaB.SharedKernel.Factories
{
    public abstract class AbstractFactory<TKeyType, TRequest, TAction>
       where TRequest : IAction
       where TAction : IAction
       where TKeyType : notnull
    {
        private readonly SortedList<TKeyType, Func<TRequest, TAction>> _map;
        private delegate TRequest CreateFunctor(TRequest obj);

        protected Func<TRequest, TKeyType>? _getKeyFunc = null;

        protected AbstractFactory()
        {
            _map = new SortedList<TKeyType, Func<TRequest, TAction>>();
        }

        public TAction Create(TRequest obj)
        {
            if (_getKeyFunc == null) throw new ArgumentNullException(nameof(_getKeyFunc));
            var createFunctor = _map[_getKeyFunc(obj)];
            return createFunctor(obj);
        }

        protected void Register(TKeyType key, Func<TRequest, TAction> func)
        {
            _map.Add(key, func);
        }
    }
}
