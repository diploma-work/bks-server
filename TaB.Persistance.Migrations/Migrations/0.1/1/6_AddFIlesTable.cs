﻿using FluentMigrator;
using System.Data;
using TaB.Persistence.Migrations.Settings;

namespace TaB.Persistance.Migrations
{
    [Tags("core")]
    [CustomMigration(0, 1, 0, 0, 6, "core", "Add table with files")]
    public class AddFIlesTable : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("Files")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("FileType").AsInt32() // Pdf, Image, Unknown
                .WithColumn("Size").AsInt64()
                .WithColumn("Name").AsString(256).Nullable()
                .WithColumn("Link").AsString(4096);

            Create.Table("FilesInContent")
            .WithColumn("FilesId").AsInt32()
                .ForeignKey("FK_FilesInContent_Files", "Files", "Id")
                .OnDelete(Rule.Cascade)
            .WithColumn("ContentId").AsInt32()
                .ForeignKey("FK_FilesInContent_Content", "Content", "Id");

            Create.PrimaryKey("PK_FilesInContent").OnTable("FilesInContent")
                .Columns("FilesId", "ContentId");

            Create.ForeignKey("FK_FilesInContent_Content1")
                .FromTable("FilesInContent").ForeignColumns("ContentId")
                .ToTable("Content").PrimaryColumns("Id");
        }
    }
}
