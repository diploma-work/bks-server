﻿using FluentMigrator;
using System.Data;
using TaB.Persistence.Migrations.Settings;

namespace TaB.Persistance.Migrations
{
    [Tags("core")]
    [CustomMigration(0, 1, 0, 0, 11, "core", "Add likes and comments tables")]
    public class AddLikesTableAndCommentsTable : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("Likes")
                .WithColumn("ContentId").AsInt32()
                    .ForeignKey("FK_Likes_Content", "Content", "Id")
                    .OnDelete(Rule.Cascade)
                .WithColumn("UserLoginId").AsInt32()
                    .ForeignKey("FK_Likes_UserLogin", "UserLogin", "Id");

            Create.PrimaryKey("PK_Likes").OnTable("Likes")
               .Columns("ContentId", "UserLoginId");

            Create.Table("Comments")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("ParentCommentId").AsInt32().Nullable()
                .WithColumn("UserLoginId").AsInt32()
                    .ForeignKey("FK_Comments_UserLogin", "UserLogin", "Id")
                .WithColumn("Text").AsString(1024);
        }
    }
}
