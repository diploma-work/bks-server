﻿using FluentMigrator;
using TaB.Persistence.Migrations.Settings;

namespace TaB.Persistance.Migrations
{
    [Tags("core")]
    [CustomMigration(0, 1, 0, 0, 10, "core", "Add test and Question tables")]
    public class AddTestTableAndQuestionTable : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("Tests")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Topic").AsString(256).NotNullable()
                .WithColumn("QuestionsCount").AsInt32();

            Create.Table("Questions")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("TestId").AsInt32()
                    .ForeignKey("FK_Questions_Tests", "Tests","Id")
                .WithColumn("Text").AsString(256).NotNullable();

            Create.Table("Answers")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("QuestionId").AsInt32()
                    .ForeignKey("FK_Answers_Questions", "Questions", "Id")
                .WithColumn("Text").AsString(256).NotNullable()
                .WithColumn("IsCorrect").AsBoolean().WithDefaultValue(false);

            Create.Table("TestsResults")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("TestId").AsInt32()
                    .ForeignKey("FK_TestsResults_Tests", "Tests", "Id")
                .WithColumn("UserLoginId").AsInt32()
                    .ForeignKey("FK_TestsResults_UserLogin", "UserLogin", "Id")
                .WithColumn("CorrectQuestionsCount").AsInt32().NotNullable()
                .WithColumn("AnswersText").AsString(4096).NotNullable();
        }
    }
}
