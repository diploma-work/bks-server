﻿using TaB.Persistence.Migrations.Settings;
using FluentMigrator;
using FluentMigrator.SqlServer;

namespace TaB.Persistance.Migrations
{
    [Tags("core")]
    [CustomMigration(0, 1, 0, 0, 4, "core", "Add content tables")]
    public class AddContentTables : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("ContentType")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Code").AsString(128).Unique("idx_ContentType_Code");

            Insert.IntoTable("ContentType")
                .WithIdentityInsert()
                .Row(new { Id = 1, Code = "News" })
                .Row(new { Id = 2, Code = "Material" });

            Create.Table("Content")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("ContentTypeId").AsInt32().NotNullable()
                    .ForeignKey("FK_Content_ContentType", "ContentType", "Id")
                .WithColumn("Status").AsByte().WithDefaultValue(1).NotNullable();

            Create.Table("ContentMetadata")
                .WithColumn("ContentId").AsInt32()
                    .ForeignKey("FK_ContentMetadata", "Content", "Id")
                .WithColumn("ContentTypeId").AsInt32().NotNullable()
                    .ForeignKey("FK_ContentMetadata_ContentType", "ContentType", "Id")
                .WithColumn("Title").AsString(128)
                .WithColumn("ContentString").AsString();
                Create.PrimaryKey("PK_ContentMetadata").OnTable("ContentMetadata").Columns("ContentId").Clustered();
        }
    }
}
