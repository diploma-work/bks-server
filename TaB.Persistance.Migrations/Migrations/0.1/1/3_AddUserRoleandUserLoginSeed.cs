﻿using TaB.Persistence.Migrations.Settings;
using FluentMigrator;
using FluentMigrator.SqlServer;

namespace TaB.Persistence.Migrations
{
    [Tags("core")]
    [CustomMigration(0, 1, 0, 0, 3, "core", "Seed roles and users")]
    public class SeedUsersAndRolesData : Migration
    {
        public override void Up()
        {
            Insert.IntoTable("UserRole")
                .WithIdentityInsert()
                .Row(new { Id = 1, Code = "System", Name = "System", Comment = "System role for system user. Must be hidden from UI" })
                .Row(new { Id = 2, Code = "Admin", Name = "Administrator", Comment = "Administrator role" })
                .Row(new { Id = 3, Code = "ContentManager", Name = "Content Manager", Comment = "Content Manager role" })
                .Row(new { Id = 4, Code = "Coach", Name = "Coach", Comment = "Coach role" })
                .Row(new { Id = 5, Code = "User", Name = "User", Comment = "User role" });


            Insert.IntoTable("UserLogin")
                .WithIdentityInsert()
                .Row(new { Id = 1, UserId = "System", PasswordHash = "000", UserRoleId = 1, CreateTime = DateTime.Now, Email = "" })
                .Row(new { Id = 2, UserId = "Taras", PasswordHash = "$2a$12$Zb5QQQzmi2KLqJAeF1z8AusljY4cEay5iAd14GmMarL9AbodiIsbW", UserRoleId = 2, CreateTime = DateTime.Now, Email = "kirsanovtaras8@gmail.com" })
                .Row(new { Id = 3, UserId = "Anton", PasswordHash = "$2a$12$CNafmUgzb4BYL/nUxItEyOomMJhkfQ2tQARek6QSqSkUDKeKTnXmG", UserRoleId = 2, CreateTime = DateTime.Now, Email = "" })
               ;
        }
        public override void Down()
        {
            Delete.FromTable("UserLogin").AllRows();
            Delete.FromTable("UserRole").AllRows();
        }
    }
}
