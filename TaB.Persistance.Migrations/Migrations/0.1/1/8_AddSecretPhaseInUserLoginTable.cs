﻿using FluentMigrator;
using TaB.Persistence.Migrations.Settings;

namespace TaB.Persistance.Migrations
{
    [Tags("core")]
    [CustomMigration(0, 1, 0, 0, 8, "core", "Add Secret Phase in UserLogin table")]
    public class AddSecretPhaseInUserLoginTable : AutoReversingMigration
    {
        public override void Up()
        {
            Alter.Table("UserLogin")
                .AddColumn("HaveSecretPhrase").AsBoolean().WithDefaultValue(false);
        }
    }
}
