﻿using FluentMigrator;
using FluentMigrator.SqlServer;
using TaB.Persistence.Migrations.Settings;

namespace TaB.Persistance.Migrations
{
    [Tags("core")]
    [CustomMigration(0, 1, 0, 0, 7, "core", "Add masterdata table and seed")]
    public class AddMasterdataTableAndSeed : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("MasterData")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Code").AsString(128)
                .WithColumn("Value").AsString(128);

            Insert.IntoTable("MasterData")
                .WithIdentityInsert()
                .Row(new { Id = 1, Code = "Header.Title", Value = "Платформа онлайн обучения и тестирования" })
                .Row(new { Id = 2, Code = "Header.Subtitle", Value = "БЕЛКООПСОЮЗ" })
                .Row(new { Id = 3, Code = "Navigation.FirstCard", Value = "Обучающие материалы и маткриалы для подготовки к тестированию" })
                .Row(new { Id = 4, Code = "Navigation.SecondCard", Value = "Пройти онлайн тестирование по соответствующим темам" })
                .Row(new { Id = 5, Code = "Footer.Text", Value = "Платформа онлайн обучения и тестирования" });
        }
    }
}
