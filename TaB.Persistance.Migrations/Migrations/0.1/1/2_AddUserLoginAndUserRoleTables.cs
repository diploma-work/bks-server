﻿using TaB.Persistence.Migrations.Settings;
using FluentMigrator;

namespace TaB.Persistence.Migrations
{
    [Tags("core")]
    [CustomMigration(0, 1, 0, 0, 2, "core", "Create user tables")]
    public class AddUserLoginAndUserRolesTables : AutoReversingMigration
    {
        public override void Up()
        {
            //---------------------------Table UserRole--------------------------

            Create.Table("UserRole")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Code").AsString(128).Unique("idx_UserRole_Code")
                .WithColumn("Name").AsString(256)
                .WithColumn("Comment").AsString(256).Nullable();

            //---------------------------Table UserLogin--------------------------

            Create.Table("UserLogin")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("UserId").AsString(96).Unique("idx_UserLogin_UserId")
                .WithColumn("PasswordHash").AsString(96)
                .WithColumn("CreateTime").AsDateTime()
                .WithColumn("LastLoginTime").AsDateTime().Nullable()
                .WithColumn("LastFailedLoginCount").AsInt32().WithDefaultValue(0)
                .WithColumn("IsDisabled").AsBoolean().WithDefaultValue(false)
                .WithColumn("TotalSuccessLoginCount").AsInt32().WithDefaultValue(0)
                .WithColumn("TotalFailedLoginCount").AsInt32().WithDefaultValue(0)
                .WithColumn("UserRoleId").AsInt32()
                    .ForeignKey("FK_UserLogin_UserRole", "UserRole", "Id")
                .WithColumn("Email").AsString(256)
                .WithColumn("FirstName").AsString(256).NotNullable().WithDefaultValue("")
                .WithColumn("LastName").AsString(256).NotNullable().WithDefaultValue("");
        }
    }
}