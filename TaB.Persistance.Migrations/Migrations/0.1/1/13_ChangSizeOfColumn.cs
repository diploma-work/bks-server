﻿using FluentMigrator;
using System.Data;
using TaB.Persistence.Migrations.Settings;

namespace TaB.Persistance.Migrations
{
    [Tags("core")]
    [CustomMigration(0, 1, 0, 0, 13, "core", "change size of the column text in content")]
    public class ChangSizeOfColumn : AutoReversingMigration
    {
        public override void Up()
        {
            Alter.Table("ContentMetadata")
                .AlterColumn("ContentString").AsString(int.MaxValue);
        }
    }
}
