﻿using FluentMigrator;
using TaB.Persistence.Migrations.Settings;

namespace TaB.Persistance.Migrations
{
    [Tags("core")]
    [CustomMigration(0, 1, 0, 0, 5, "core", "Change Status Column To Int")]
    public class ChangeStatusColumnToInt : Migration
    {
        public override void Up()
        {
            Delete.Column("Status").FromTable("Content");
            Alter.Table("Content")
                .AddColumn("Status").AsInt32().WithDefaultValue(1).NotNullable();
        }

        public override void Down()
        {
            Delete.Column("Status").FromTable("Content");
            Alter.Table("Content")
                .AddColumn("Status").AsByte().WithDefaultValue(1).NotNullable();
        }
    }
}
