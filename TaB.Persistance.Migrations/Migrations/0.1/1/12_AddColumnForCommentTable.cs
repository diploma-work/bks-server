﻿using FluentMigrator;
using System.Data;
using TaB.Persistence.Migrations.Settings;

namespace TaB.Persistance.Migrations
{
    [Tags("core")]
    [CustomMigration(0, 1, 0, 0, 12, "core", "Add column for comments tables")]
    public class AddColumnForCommentTable : AutoReversingMigration
    {
        public override void Up()
        {
            Alter.Table("Comments")
                .AddColumn("ContentId").AsInt32()
                    .ForeignKey("FK_Comments_Content", "Content", "Id");
        }
    }
}
