﻿using FluentMigrator;
using TaB.Persistence.Migrations.Settings;

namespace TaB.Persistance.Migrations
{
    [Tags("core")]
    [CustomMigration(0, 1, 0, 0, 9, "core", "Correct Users Code")]
    public class CorrectUsersCode : Migration
    {

        public override void Up()
        {
            Update.Table("UserRole")
                .Set(new { Code = "Client", Name = "Client", Comment = "Client role" })
                .Where(new { Id = 4 });
        }

        public override void Down()
        {
            Update.Table("UserRole")
                 .Set(new { Code = "Coach", Name = "Coach", Comment = "Coach role" })
                 .Where(new { Id = 4 });
        }
    }
}
