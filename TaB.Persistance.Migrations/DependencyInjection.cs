﻿using FluentMigrator.Runner;
using FluentMigrator.Runner.Initialization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace TaB.Persistence.Migrations
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddMigrations(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddSqlServer()
                    .WithGlobalConnectionString(configuration.GetConnectionString("DefaultConnection"))
                    .ScanIn(typeof(AddUserLoginAndUserRolesTables).Assembly).For.Migrations().For.EmbeddedResources())
                .AddLogging(lb => lb.AddFluentMigratorConsole()/*AddNLog()*/)
                .Configure<RunnerOptions>(opt =>
                {
                    opt.Tags = new[] { "core" };
                });

            //RepoDb.SqlServerBootstrap.Initialize();

            //FluentMapper.Entity<Region>().Table("MasterData");
            //FluentMapper.Entity<World>().Table("MasterData");

            return services;
        }
    }
}
