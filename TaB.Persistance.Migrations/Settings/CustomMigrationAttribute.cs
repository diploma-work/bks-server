﻿using FluentMigrator;

namespace TaB.Persistence.Migrations.Settings
{
    public class CustomMigrationAttribute : MigrationAttribute
    {
        public CustomMigrationAttribute(int majorVersion, int minorVersion, int buildVersion,
                                        int hotFixVersion, int migrationNumber, string codeProject, string description)
                : base(CalculateVersion(majorVersion, minorVersion, buildVersion, hotFixVersion,
                                      migrationNumber, codeProject, description),
                      CreateDescription(codeProject, description))
        {
            //_codeProject = codeProject.Trim().ToLower();
        }

        private static string? _codeProject;

        private static long CalculateVersion(int major, int minor, int build, int hotFix,
                                           int migrationNumber, string codeProject, string description)
        {
            int projectNumber;
            _codeProject = codeProject.Trim().ToLower();

            switch (_codeProject)
            {
                case "core":
                    projectNumber = (int)CodeProject.core;
                    break;
                case "version":
                    projectNumber = (int)CodeProject.version;
                    break;
                case "workflow":
                    projectNumber = (int)CodeProject.workflow;
                    break;
                case "analytics":
                    projectNumber = (int)CodeProject.analytics;
                    break;
                default:
                    projectNumber = 0;
                    break;
            }

            return major * 1000000000000000L +
                   minor * 1000000000000L +
                   build * 1000000000L +
                   hotFix * 1000000L +
                   migrationNumber * 1000L +
                   projectNumber;
        }

        public static string CreateDescription(string codeProject, string description)
        {
            var s = String.Concat(codeProject, ": ", description);
            return s;
        }
    }

    public enum CodeProject
    {
        core = 1,
        version = 2,
        workflow = 3,
        analytics = 4
    }
}
