﻿using FluentValidation.Results;
using TaB.SharedKernel.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;

namespace TaB.Application.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public abstract class BaseApiController : ControllerBase
    {
        protected ILogger<BaseApiController> Logger { get; }

        protected BaseApiController(ILogger<BaseApiController> logger)
        {
            Logger = logger;
        }

        protected ActionResult BadRequest(string message, string description)
        {
            Ok();
            return BadRequest(new Error(message, description));
        }

        protected ActionResult NotFound(string message, string description)
        {
            return NotFound(new Error(message, description));
        }

        protected ActionResult StatusCode500AndLogError(Exception exception)
        {
            Logger.LogError(exception, "Unhandled error: {0}", exception.Message);
            var description = exception.Message;
#if DEBUG
            description = exception.ToString();
#endif
            return StatusCode(StatusCodes.Status500InternalServerError, new Error(exception.Message, description));
        }

        protected string GetFirstModelError(ModelStateDictionary modelState)
        {
            var errorDescr = string.Empty;

            var modelError = modelState.Values.Any() ? modelState.FirstOrDefault(x => x.Value.Errors.Count > 0).Value.Errors[0] : null;
            if (modelError == null) return errorDescr;
            if (!string.IsNullOrEmpty(modelError.ErrorMessage))
            {
                errorDescr = modelError.ErrorMessage;
            }
            else if (modelError.Exception != null)
            {
                errorDescr = modelError.Exception.Message;
            }

            return errorDescr;
        }

        protected string GetFirstModelError(ValidationResult modelState)
        {
            var errorDescr = string.Empty;

            var modelError = modelState.Errors.FirstOrDefault();
            if (modelError == null) return errorDescr;
            if (!string.IsNullOrEmpty(modelError.ErrorMessage))
            {
                errorDescr = modelError.ErrorMessage;
            }

            return errorDescr;
        }
    }
}
