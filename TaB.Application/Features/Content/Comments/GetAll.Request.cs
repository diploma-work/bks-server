﻿using MediatR;
using TaB.Application.Features.Content.Comments.Models;
using TaB.SharedKernel.Pagination;

namespace TaB.Application.Features.Content.Comments
{
    public class GetAllRequest : GetAllFilter, IRequest<IPagedList<CommentInfo>> { }
}
