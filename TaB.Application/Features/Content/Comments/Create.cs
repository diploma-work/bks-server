﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.Comments.Models;
using TaB.Domain.Entities;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Content.Comments
{
    public class CreateHandler : IRequestHandler<CreateRequest, CommentInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMediator _mediator;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;

        public CreateHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMediator mediator,
            ICurrentUserService currentUserService,
            IMapper mapper
            )
        {
            _dbContextFactory = dbContextFactory;
            _mediator = mediator;
            _currentUserService = currentUserService;
            _mapper = mapper;
        }

        public async Task<CommentInfo> Handle(CreateRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();

            var content = await db.Set<Domain.Entities.Content.News.News>()
              .FirstOrDefaultAsync(x => x.Id == request.ContentId);

            if (content == null) throw new NotFoundException($"Not found News/{request.ContentId}");

            if(request.ParentCommentId != null)
            {
                var parentComment = await db.Set<Domain.Entities.Comment>()
                   .FirstOrDefaultAsync(x => x.Id == request.ParentCommentId);

                if (parentComment == null) throw new NotFoundException($"Not found ParentComment/{request.ParentCommentId}");

                if (await GetCommentDepth((int)request.ParentCommentId) >= 3) throw new BadRequestException($"Depth more than 3/{request.ParentCommentId}");
            }

            await db.BeginTransactionAsync();

            var entity = _mapper.Map<CommentInfo>(request);
            entity.UserLoginId = _currentUserService.Id;

            var comment = db.Set<Domain.Entities.Comment>()
                            .AddOrUpdate(_mapper.Map<Comment>(entity), x=>x.Id);

            await db.CommitTransactionAsync();

            return await _mediator.Send(_mapper.Map<GetRequest>(comment));
        }

        private async Task<int> GetCommentDepth(int parentCommentId)
        {
            int depth = 0;

            while(true)
            {
                depth++;

                var comment = await _mediator.Send(new GetRequest() { Id = parentCommentId});

                if (comment.ParentCommentId == null)
                    break;

                parentCommentId = (int)comment.ParentCommentId;
            }

            return depth;
        }
    }
}
