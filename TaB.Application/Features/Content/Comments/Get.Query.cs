﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TaB.SharedKernel.Interfaces;
using TaB.SharedKernel.Query;

namespace TaB.Application.Features.Content.Comments
{
    public class GetQuery : Query<Domain.Entities.Comment>, IMapFrom<GetRequest>
    {
        public int Id { get; set; }

        public override Expression<Func<Domain.Entities.Comment, bool>> GetExpression()
        {
            Expression<Func<Domain.Entities.Comment, bool>> filter = entity => entity.Id == Id;
            return filter;
        }

        public override List<Expression<Func<Domain.Entities.Comment, object>>> GetIncludes()
        {
            var @base = base.GetIncludes();
            @base.Add(x => x.User);
            @base.Add(x => x.News);
            return @base;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateRequest, GetQuery>();
            profile.CreateMap<DeleteRequest, GetQuery>();
            profile.CreateMap<CreateRequest, GetQuery>();
            profile.CreateMap<GetRequest, GetQuery>();
        }
    }
}
