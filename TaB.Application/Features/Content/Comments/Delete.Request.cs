﻿using MediatR;

namespace TaB.Application.Features.Content.Comments
{
    public class DeleteRequest : IRequest<Unit>
    {
        public int Id { get; set; }
    }
}
