﻿using MediatR;
using TaB.Application.Features.Content.Comments.Models;

namespace TaB.Application.Features.Content.Comments
{
    public class CreateRequest : IRequest<CommentInfo>
    {
        public int Id { get; set; }
        public int ContentId { get; set; }
        public int? ParentCommentId { get; set; }
        public string Text { get; set; } = default!;
    }
}
