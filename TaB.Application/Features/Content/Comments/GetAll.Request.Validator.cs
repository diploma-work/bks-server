﻿using TaB.Application.Features.Content.Comments.Models;
using TaB.SharedKernel.Validation.Filters;

namespace TaB.Application.Features.Content.Comments
{
    public class GetAllRequestValidator : FilterValidatorBase<GetAllRequest, CommentInfo> { }
}
