﻿using System.Linq.Expressions;
using TaB.SharedKernel.Extensions;
using TaB.SharedKernel.Interfaces;
using TaB.SharedKernel.Query;

namespace TaB.Application.Features.Content.Comments
{
    public class GetAllQuery : SortedQuery<Domain.Entities.Comment>, IMapFrom<GetAllFilter>
    {
        public string? FreeText { get; set; }
        public CommentFilterInfo? Data { get; set; }

        public override Expression<Func<Domain.Entities.Comment, bool>> GetExpression()
        {
            var filter = base.GetExpression();

            if (!string.IsNullOrEmpty(FreeText))
                FreeText.Split().ForEach(term =>
                {
                    filter = filter.And(c => c.Text.Contains(term));
                });

            if (Data == null) return filter;

            filter = filter.And(x => Data.ParentCommentId == x.ParentCommentId);

            if (Data.ContentId != null)
                filter = filter.And(x => Data.ContentId == x.ContentId);

            return filter;
        }

        public override Func<IQueryable<Domain.Entities.Comment>, IOrderedQueryable<Domain.Entities.Comment>> GetSortingExpression()
        {
            return SortColumn switch
            {
                _ => item => item.ApplyOrder(SortColumn, IsAscending)
            };
        }
    }
}
