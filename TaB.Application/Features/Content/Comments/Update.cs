﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.Comments.Models;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Extensions;

namespace TaB.Application.Features.Content.Comments
{
    public class UpdateHandler : IRequestHandler<UpdateRequest, CommentInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public UpdateHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper,
            IMediator mediator
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<CommentInfo> Handle(UpdateRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            await db.BeginTransactionAsync();

            var entity = await db.Set<Domain.Entities.Comment>()
                .ApplyQuery(_mapper.Map<GetQuery>(request))
                .FirstOrDefaultAsync(cancellationToken);

            if (entity == null) throw new NotFoundException($"Not found Comment/{request.Id}");

            entity.Text = request.Text;

            db.Set<Domain.Entities.Comment>()
                .AddOrUpdate(entity, x => x.Id);

            await db.CommitTransactionAsync();

            return await _mediator.Send(_mapper.Map<GetRequest>(request), cancellationToken);
        }

    }
}
