﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.Comments.Models;
using TaB.Persistence;
using TaB.SharedKernel.Extensions;
using TaB.SharedKernel.Pagination;

namespace TaB.Application.Features.Content.Comments
{
    public class GetAllHandler : IRequestHandler<GetAllRequest, IPagedList<CommentInfo>>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;

        public GetAllHandler(IDbContextFactory<ApplicationDbContext> dbContextFactory, IMapper mapper)
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
        }

        public async Task<IPagedList<CommentInfo>> Handle(GetAllRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            return db.Set<Domain.Entities.Comment>()
                .ApplyQuery(_mapper.Map<GetAllQuery>(request))
                .ProjectTo<CommentInfo>(_mapper.ConfigurationProvider)
                .ApplyPaging(request);
        }
    }
}
