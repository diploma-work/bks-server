﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;

namespace TaB.Application.Features.Content.Comments
{
    public class DeleteHandler : IRequestHandler<DeleteRequest, Unit>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContext;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public DeleteHandler(
            IDbContextFactory<ApplicationDbContext> dbContext,
            IMapper mapper,
            IMediator mediator
        )
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(DeleteRequest request, CancellationToken cancellationToken)
        {
            using var db = _dbContext.CreateDbContext();
           

            var entity = await db.Set<Domain.Entities.Comment>()
              .FirstOrDefaultAsync(x => x.Id == request.Id);

            if (entity == null) throw new NotFoundException($"Not found Comment/{request.Id}");

            //var childrenComments = db.Set<Domain.Entities.Comment>()
            //                            .Where(x => x.ParentCommentId == request.Id);

            //foreach (var childrenComment in childrenComments)
            //    await _mediator.Send(new DeleteRequest(){ Id = childrenComment.Id });

            await db.BeginTransactionAsync();

            db.Set<Domain.Entities.Comment>()
                .Remove(entity);

            await db.CommitTransactionAsync();

            return Unit.Value;
        }
    }
}
