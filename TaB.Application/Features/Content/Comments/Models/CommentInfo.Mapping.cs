﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using TaB.Domain.Entities;

namespace TaB.Application.Features.Content.Comments.Models
{
    public partial class CommentInfo
    {
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CommentInfo, Comment>();
            profile.CreateMap<CommentInfo, CreateRequest>().ReverseMap();
            profile.CreateMap<CommentInfo, UpdateRequest>().ReverseMap();
            profile.CreateMap<CommentInfo, GetRequest>().ReverseMap();
            profile.CreateMap<UpdateRequest, GetRequest>().ReverseMap();
            profile.CreateMap<Comment, GetRequest>().ReverseMap();
            profile.CreateMap<GetAllRequest, GetAllQuery>().ReverseMap();

            profile.CreateMap<Comment, CommentInfo>()
              .EqualityComparison((src, dest) => src.Id == dest.Id)
              .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User.UserId))
              .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
              .ForMember(dest => dest.SecondName, opt => opt.MapFrom(src => src.User.LastName));
        }
    }
}
