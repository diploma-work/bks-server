﻿using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Content.Comments.Models
{
    public partial class CommentInfo : IMapFrom<CommentInfo>
    {
        public int Id { get; set; }
        public int ContentId { get; set; }
        public int UserLoginId { get; set; }
        public string FirstName { get; set; } = default!;
        public string SecondName { get; set; } = default!;
        public string UserId { get; set; } = default!;
        public int? ParentCommentId { get; set; }
        public string Text { get; set; } = default!;
    }
}
