﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.Comments.Models;
using TaB.Domain.Entities;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Extensions;

namespace TaB.Application.Features.Content.Comments
{
    public class GetHandler : IRequestHandler<GetRequest, CommentInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;

        public GetHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
        }

        public async Task<CommentInfo> Handle(GetRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();

            return await db.Set<Domain.Entities.Comment>()
                .AsNoTracking()
                .ApplyQuery(_mapper.Map<GetQuery>(request))
                .ProjectTo<CommentInfo>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(cancellationToken)
                   ?? throw new NotFoundException($"Not found CommentInfo/{request.Id}");
        }
    }
}
