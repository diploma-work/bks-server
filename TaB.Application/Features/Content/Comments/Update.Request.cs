﻿using MediatR;
using TaB.Application.Features.Content.Comments.Models;

namespace TaB.Application.Features.Content.Comments
{
    public class UpdateRequest : IRequest<CommentInfo>
    {
        public int Id { get; set; }
        public string Text { get; set; } = default!;
    }
}
