﻿using TaB.SharedKernel.Filters;

namespace TaB.Application.Features.Content.Comments
{
    public class GetAllFilter : SortedPagedFilter
    {
        public CommentFilterInfo? Data { get; set; }

        public GetAllFilter()
        {
            Data = new CommentFilterInfo();
        }
    }

    public class CommentFilterInfo
    {
        public int? ParentCommentId { get; set; }
        public int? ContentId { get; set; }
    }
}
