﻿using MediatR;
using TaB.Application.Features.Content.Comments.Models;

namespace TaB.Application.Features.Content.Comments
{
    public class GetRequest : IRequest<CommentInfo>
    {
        public int Id { get; set; }
    }
}
