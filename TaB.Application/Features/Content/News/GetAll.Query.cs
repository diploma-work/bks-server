﻿using System;
using System.Linq;
using System.Linq.Expressions;
using TaB.Application.Features.Content.News.Models;
using TaB.SharedKernel.Extensions;
using TaB.SharedKernel.Interfaces;
using TaB.SharedKernel.Query;

namespace TaB.Application.Features.Content.News
{
    public class GetAllQuery : SortedQuery<Domain.Entities.Content.News.News>, IMapFrom<GetAllFilter>
    {
        public string? FreeText { get; set; }
        public NewsFilterInfo? Data { get; set; }

        public override Expression<Func<Domain.Entities.Content.News.News, bool>> GetExpression()
        {
            var filter = base.GetExpression();

            if (!string.IsNullOrEmpty(FreeText))
                FreeText.Split().ForEach(term =>
                {
                    filter = filter.And(c => c.Metadata.ContentString.Contains(term)
                                          || c.Metadata.Title.Contains(term));
                });

            if (Data == null) return filter;

            if (!Data.Statuses.IsEmpty())
                filter = filter.And(x => Data.Statuses!.Contains(x.Status));

            return filter;
        }

        public override Func<IQueryable<Domain.Entities.Content.News.News>, IOrderedQueryable<Domain.Entities.Content.News.News>> GetSortingExpression()
        {
            return SortColumn switch
            {
                _ => item => item.ApplyOrder(SortColumn, IsAscending)
            };
        }
    }
}
