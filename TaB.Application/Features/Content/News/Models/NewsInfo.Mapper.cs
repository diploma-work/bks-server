﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using AutoMapper.Internal;
using TaB.Application.Features.Content.Likes.Models;

namespace TaB.Application.Features.Content.News.Models
{
    public partial class NewsInfo
    {
        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateRequest, GetRequest>();
            profile.CreateMap<CreateRequest, GetRequest>();
            profile.CreateMap<NewsInfo, CreateRequest>().ReverseMap();
            profile.CreateMap<NewsInfo, UpdateRequest>();

            profile.CreateMap<NewsInfo, Domain.Entities.Content.News.News>()
              .EqualityComparison((src, dest) => src.Id == dest.Id)
              .ForMember(dest => dest.Files, opt => opt.Ignore())
              .ForMember(dest => dest.Likes, opt => opt.Ignore())
              .ForPath(dest => dest.Metadata.ContentString, opt => opt.MapFrom(src => src.ContentString))
              .ForPath(dest => dest.Metadata.Title, opt => opt.MapFrom(src => src.Title))
              .ForMember(dest => dest.ContentFiles, opt => opt.MapFrom(src => src.Files))
               .AfterMap((src, dest, _) =>
               {
                   dest.ContentFiles.ForAll(l =>
                   {
                       l.ContentId = src.Id;
                       l.Content = dest;
                   });
               });

            profile.CreateMap<Domain.Entities.Content.News.News, NewsInfo>()
                .EqualityComparison((src, dest) => src.Id == dest.Id)
                .ForMember(dest => dest.LikesInNews, opt => opt.Ignore())
                .ForMember(dest => dest.ContentString, opt => opt.MapFrom(src => src.Metadata.ContentString))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Metadata.Title));
                //.ForMember(dest => dest.LikesInNews, opt => opt.MapFrom<NewsInfoResolver>());

            profile.CreateMap<CreateRequest, Domain.Entities.Content.News.News>()
              .EqualityComparison((src, dest) => src.Id == dest.Id)
              .ForPath(dest => dest.Metadata.ContentString, opt => opt.MapFrom(src => src.ContentString))
              .ForPath(dest => dest.Metadata.Title, opt => opt.MapFrom(src => src.Title));

            profile.CreateMap<Domain.Entities.Content.News.News, CreateRequest>()
            .ForMember(dest => dest.ContentString, opt => opt.MapFrom(src => src.Metadata.ContentString))
            .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Metadata.Title));

            profile.CreateMap<UpdateRequest, Domain.Entities.Content.News.News>()
             .EqualityComparison((src, dest) => src.Id == dest.Id)
             .ForPath(dest => dest.Metadata.ContentString, opt => opt.MapFrom(src => src.ContentString))
             .ForPath(dest => dest.Metadata.Title, opt => opt.MapFrom(src => src.Title));

        }
    }
}
