﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.Likes.Models;
using TaB.Persistence;
using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Content.News.Models
{
    public class NewsInfoResolver : IValueResolver<Domain.Entities.Content.News.News, NewsInfo, LikesInNewsInfo?>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly ICurrentUserService _currentUserService;
        public NewsInfoResolver(IDbContextFactory<ApplicationDbContext> dbContextFactory,
            ICurrentUserService currentUserService)
        {
            _dbContextFactory = dbContextFactory;
            _currentUserService = currentUserService;
        }
        public LikesInNewsInfo? Resolve(Domain.Entities.Content.News.News source, NewsInfo destination, LikesInNewsInfo? destMember, ResolutionContext context)
        {
            using var db = _dbContextFactory.CreateDbContext();

            return new LikesInNewsInfo() 
            {
                ContentId = source.Id,
                Count = db.Set<Domain.Entities.Like>()
                             .Count(x => x.ContentId == source.Id),
                IsLikedByCurrentUser = db.Set<Domain.Entities.Like>()
                                            .FirstOrDefaultAsync(x =>
                                                x.UserLoginId == _currentUserService.Id &&
                                                x.ContentId == source.Id
                                            ) != null
            };
        }
    }
}
