﻿using TaB.Application.Features.Content.Likes.Models;
using TaB.SharedKernel.Attributes;
using TaB.SharedKernel.Interfaces;
using TaB.SharedKernel.Models;

namespace TaB.Application.Features.Content.News.Models
{
    public partial class NewsInfo : IMapFrom<NewsInfo>
    {
        [Orderable]
        public int Id { get; set; }

        [Orderable]
        public string Title { get; set; } = default!;

        [Orderable]
        public string ContentString { get; set; } = default!;

        public List<FilesInfo>? Files { get; set; }

        public LikesInNewsInfo? LikesInNews { get; set; } = default!;

    }
}
