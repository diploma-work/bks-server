﻿using TaB.Application.Features.Content.News.Models;
using TaB.SharedKernel.Pagination;
using MediatR;

namespace TaB.Application.Features.Content.News
{
    public class GetAllRequest : GetAllFilter, IRequest<IPagedList<NewsInfo>> { }
}
