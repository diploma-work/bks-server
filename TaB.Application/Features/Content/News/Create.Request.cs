﻿using MediatR;
using TaB.Application.Features.Content.News.Models;

namespace TaB.Application.Features.Content.News
{
    public class CreateRequest : IRequest<NewsInfo>
    {
        public int Id { get; set; }

        public string Title { get; set; } = default!;

        public string ContentString { get; set; } = default!;
    }
}
