﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.News.Models;
using TaB.Persistence;

namespace TaB.Application.Features.Content.News
{
    public class CreateHandler : IRequestHandler<CreateRequest, NewsInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public CreateHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper,
            IMediator mediator
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<NewsInfo> Handle(CreateRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            await db.BeginTransactionAsync();

            var entity = await db.Set<Domain.Entities.Content.News.News>()
                .AddAsync(_mapper.Map<Domain.Entities.Content.News.News>(request), cancellationToken);

            await db.CommitTransactionAsync();

            request.Id = entity.Entity.Id;

            return await _mediator.Send(_mapper.Map<GetRequest>(request), cancellationToken);
        }
    }
}
