﻿using MediatR;
using TaB.Application.Features.Content.News.Models;
using TaB.SharedKernel.Models;

namespace TaB.Application.Features.Content.News
{
    public class UpdateRequest : IRequest<NewsInfo> 
    {
        public int Id { get; set; }

        public string Title { get; set; } = default!;

        public string ContentString { get; set; } = default!;

        public List<FilesInfo>? Files { get; set; }
    }
}
