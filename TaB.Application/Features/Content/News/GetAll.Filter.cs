﻿using TaB.Domain.Enums;
using TaB.SharedKernel.Filters;

namespace TaB.Application.Features.Content.News
{
    public class GetAllFilter : SortedPagedFilter
    {
        public NewsFilterInfo? Data { get; set; }
    }

    public record NewsFilterInfo(
        List<StatusEnum>? Statuses);
}
