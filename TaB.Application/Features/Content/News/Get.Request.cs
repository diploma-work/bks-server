﻿using MediatR;
using TaB.Application.Features.Content.News.Models;

namespace TaB.Application.Features.Content.News
{
    public class GetRequest : IRequest<NewsInfo>
    {
        public int Id { get; set; }
    }
}
