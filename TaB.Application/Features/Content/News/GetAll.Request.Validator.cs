﻿using TaB.Application.Features.Content.News.Models;
using TaB.SharedKernel.Validation.Filters;

namespace TaB.Application.Features.Content.News
{
    public class GetAllRequestValidator : FilterValidatorBase<GetAllRequest, NewsInfo> { }
}
