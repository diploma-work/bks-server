﻿using AutoMapper;
using System.Linq.Expressions;
using TaB.SharedKernel.Interfaces;
using TaB.SharedKernel.Query;

namespace TaB.Application.Features.Content.News
{
    public class GetQuery : Query<Domain.Entities.Content.News.News>, IMapFrom<CreateRequest>
    {
        public int Id { get; set; }

        public override Expression<Func<Domain.Entities.Content.News.News, bool>> GetExpression()
        {
            Expression<Func<Domain.Entities.Content.News.News, bool>> filter = entity => entity.Id == Id;
            return filter;
        }

        public override List<Expression<Func<Domain.Entities.Content.News.News, object>>> GetIncludes()
        {
            var @base = base.GetIncludes();
            @base.Add(x => x.Metadata);
            @base.Add(x => x.Files);
            return @base;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateRequest, GetQuery>();
            profile.CreateMap<DeleteRequest, GetQuery>();
            profile.CreateMap<CreateRequest, GetQuery>();
            profile.CreateMap<GetRequest, GetQuery>();
        }
    }
}
