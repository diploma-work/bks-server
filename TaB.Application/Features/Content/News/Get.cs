﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.News.Models;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Extensions;

namespace TaB.Application.Features.Content.News
{
    public class GetHandler : IRequestHandler<GetRequest, NewsInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public GetHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper,
            IMediator mediator
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<NewsInfo> Handle(GetRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();

            var news = await db.Set<Domain.Entities.Content.News.News>()
                .AsNoTracking()
                .ApplyQuery(_mapper.Map<GetQuery>(request))
                .ProjectTo<NewsInfo>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(cancellationToken)
                   ?? throw new NotFoundException($"Not found NewsInfo/{request.Id}");

            news.LikesInNews = await _mediator.Send(new Features.Content.Likes.GetRequest() { ContentId = news.Id });

            return news;
        }
    }
}
