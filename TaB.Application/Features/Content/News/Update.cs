﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.News.Models;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Extensions;

namespace TaB.Application.Features.Content.News
{
    public class UpdateHandler : IRequestHandler<UpdateRequest, NewsInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public UpdateHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper,
            IMediator mediator
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<NewsInfo> Handle(UpdateRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            await db.BeginTransactionAsync();

            var entity = await db.Set<Domain.Entities.Content.News.News>()
                .ApplyQuery(_mapper.Map<GetQuery>(request))
                .FirstOrDefaultAsync(cancellationToken);

            if (entity == null) throw new NotFoundException($"Not found News/{request.Id}");

            entity = _mapper.Map(request, entity);
            db.Set<Domain.Entities.Content.News.News>()
                .Update(entity);

            await db.CommitTransactionAsync();

            return await _mediator.Send(_mapper.Map<GetRequest>(request), cancellationToken);
        }

    }
}
