﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.Likes.Models;
using TaB.Application.Features.Content.News.Models;
using TaB.Persistence;
using TaB.SharedKernel.Extensions;
using TaB.SharedKernel.Interfaces;
using TaB.SharedKernel.Pagination;

namespace TaB.Application.Features.Content.News
{
    public class GetAllHandler : IRequestHandler<GetAllRequest, IPagedList<NewsInfo>>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public GetAllHandler(IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper,
            IMediator mediator
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<IPagedList<NewsInfo>> Handle(GetAllRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            var news = db.Set<Domain.Entities.Content.News.News>()
                .ApplyQuery(_mapper.Map<GetAllQuery>(request))
                .ProjectTo<NewsInfo>(_mapper.ConfigurationProvider)
                .ApplyPaging(request);

            foreach (var item in news.Result)
            {
                item.LikesInNews = await _mediator.Send(new Features.Content.Likes.GetRequest() { ContentId = item.Id });
            }

            return news;
        }
    }
}
