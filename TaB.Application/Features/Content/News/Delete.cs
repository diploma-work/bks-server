﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Extensions;

namespace TaB.Application.Features.Content.News
{
    public class DeleteHandler : IRequestHandler<DeleteRequest, Unit>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContext;
        private readonly IMapper _mapper;

        public DeleteHandler(
            IDbContextFactory<ApplicationDbContext> dbContext,
            IMapper mapper
        )
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteRequest request, CancellationToken cancellationToken)
        {
            using var db = _dbContext.CreateDbContext();
            await db.BeginTransactionAsync();

            var entity = await db.Set<Domain.Entities.Content.News.News>()
              .ApplyQuery(_mapper.Map<GetQuery>(request))
              .FirstOrDefaultAsync(cancellationToken);

            if (entity == null) throw new NotFoundException($"Not found News/{request.Id}");

            db.Set<Domain.Entities.Content.News.News>()
                .Remove(entity);

            await db.CommitTransactionAsync();

            return Unit.Value;
        }
    }
}
