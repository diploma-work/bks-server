﻿using TaB.Application.Features.Content.Material.Models;
using TaB.SharedKernel.Validation.Filters;

namespace TaB.Application.Features.Content.Material
{
    public class GetAllRequestValidator : FilterValidatorBase<GetAllRequest, MaterialInfo> { }
}
