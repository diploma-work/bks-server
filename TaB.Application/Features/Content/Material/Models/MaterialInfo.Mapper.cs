﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using AutoMapper.Internal;

namespace TaB.Application.Features.Content.Material.Models
{
    public partial class MaterialInfo
    {
        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateRequest, GetRequest>();
            profile.CreateMap<CreateRequest, GetRequest>();
            profile.CreateMap<MaterialInfo, CreateRequest>().ReverseMap();
            profile.CreateMap<MaterialInfo, UpdateRequest>();

            profile.CreateMap<MaterialInfo, Domain.Entities.Content.Material.Material>()
              .EqualityComparison((src, dest) => src.Id == dest.Id)
              .ForMember(dest => dest.Files, opt => opt.Ignore())
              .ForPath(dest => dest.Metadata.ContentString, opt => opt.MapFrom(src => src.ContentString))
              .ForPath(dest => dest.Metadata.Title, opt => opt.MapFrom(src => src.Title))
              .ForMember(dest => dest.ContentFiles, opt => opt.MapFrom(src => src.Files))
               .AfterMap((src, dest, _) =>
               {
                   dest.ContentFiles.ForAll(l =>
                   {
                       l.ContentId = src.Id;
                       l.Content = dest;
                   });
               });

            profile.CreateMap<Domain.Entities.Content.Material.Material, MaterialInfo>()
                .EqualityComparison((src, dest) => src.Id == dest.Id)
                .ForMember(dest => dest.ContentString, opt => opt.MapFrom(src => src.Metadata.ContentString))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Metadata.Title));

            profile.CreateMap<CreateRequest, Domain.Entities.Content.Material.Material>()
              .EqualityComparison((src, dest) => src.Id == dest.Id)
              .ForPath(dest => dest.Metadata.ContentString, opt => opt.MapFrom(src => src.ContentString))
              .ForPath(dest => dest.Metadata.Title, opt => opt.MapFrom(src => src.Title));

            profile.CreateMap<Domain.Entities.Content.Material.Material, CreateRequest>()
            .ForMember(dest => dest.ContentString, opt => opt.MapFrom(src => src.Metadata.ContentString))
            .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Metadata.Title));

            profile.CreateMap<UpdateRequest, Domain.Entities.Content.Material.Material>()
             .EqualityComparison((src, dest) => src.Id == dest.Id)
             .ForPath(dest => dest.Metadata.ContentString, opt => opt.MapFrom(src => src.ContentString))
             .ForPath(dest => dest.Metadata.Title, opt => opt.MapFrom(src => src.Title));
        }
    }
}
