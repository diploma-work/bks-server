﻿using TaB.SharedKernel.Attributes;
using TaB.SharedKernel.Interfaces;
using TaB.SharedKernel.Models;

namespace TaB.Application.Features.Content.Material.Models
{
    public partial class MaterialInfo : IMapFrom<MaterialInfo>
    {
        [Orderable]
        public int Id { get; set; }

        [Orderable]
        public string Title { get; set; } = default!;

        [Orderable]
        public string ContentString { get; set; } = default!;

        public List<FilesInfo>? Files { get; set; }

    }
}
