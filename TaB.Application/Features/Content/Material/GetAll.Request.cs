﻿using MediatR;
using TaB.Application.Features.Content.Material.Models;
using TaB.SharedKernel.Pagination;

namespace TaB.Application.Features.Content.Material
{
    public class GetAllRequest : GetAllFilter, IRequest<IPagedList<MaterialInfo>> { }
}
