﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.Material.Models;
using TaB.Persistence;
using TaB.SharedKernel.Extensions;
using TaB.SharedKernel.Pagination;

namespace TaB.Application.Features.Content.Material
{
    public class GetAllHandler : IRequestHandler<GetAllRequest, IPagedList<MaterialInfo>>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;

        public GetAllHandler(IDbContextFactory<ApplicationDbContext> dbContextFactory, IMapper mapper)
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
        }

        public async Task<IPagedList<MaterialInfo>> Handle(GetAllRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            return db.Set<Domain.Entities.Content.Material.Material>()
                .ApplyQuery(_mapper.Map<GetAllQuery>(request))
                .ProjectTo<MaterialInfo>(_mapper.ConfigurationProvider)
                .ApplyPaging(request);
        }
    }
}
