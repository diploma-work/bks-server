﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.Material.Models;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Extensions;

namespace TaB.Application.Features.Content.Material
{
    public class UpdateHandler : IRequestHandler<UpdateRequest, MaterialInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public UpdateHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper,
            IMediator mediator
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<MaterialInfo> Handle(UpdateRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            await db.BeginTransactionAsync();

            var entity = await db.Set<Domain.Entities.Content.Material.Material>()
                .ApplyQuery(_mapper.Map<GetQuery>(request))
                .FirstOrDefaultAsync(cancellationToken);

            if (entity == null) throw new NotFoundException($"Not found News/{request.Id}");

            entity = _mapper.Map(request, entity);
            db.Set<Domain.Entities.Content.Material.Material>()
                .Update(entity);

            await db.CommitTransactionAsync();

            return await _mediator.Send(_mapper.Map<GetRequest>(request), cancellationToken);
        }

    }
}
