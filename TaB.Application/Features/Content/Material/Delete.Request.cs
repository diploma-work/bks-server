﻿using MediatR;

namespace TaB.Application.Features.Content.Material
{
    public class DeleteRequest : IRequest<Unit>
    {
        public int Id { get; set; }
    }
}
