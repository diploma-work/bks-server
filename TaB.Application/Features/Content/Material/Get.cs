﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.Material.Models;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Extensions;

namespace TaB.Application.Features.Content.Material
{
    public class GetHandler : IRequestHandler<GetRequest, MaterialInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;

        public GetHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
        }

        public async Task<MaterialInfo> Handle(GetRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();

            return await db.Set<Domain.Entities.Content.Material.Material>()
                .AsNoTracking()
                .ApplyQuery(_mapper.Map<GetQuery>(request))
                .ProjectTo<MaterialInfo>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(cancellationToken)
                   ?? throw new NotFoundException($"Not found NewsInfo/{request.Id}");
        }
    }
}
