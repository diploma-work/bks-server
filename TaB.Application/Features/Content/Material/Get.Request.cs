﻿using MediatR;
using TaB.Application.Features.Content.Material.Models;

namespace TaB.Application.Features.Content.Material
{
    public class GetRequest : IRequest<MaterialInfo>
    {
        public int Id { get; set; }
    }
}
