﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.Material.Models;
using TaB.Persistence;

namespace TaB.Application.Features.Content.Material
{
    public class CreateHandler : IRequestHandler<CreateRequest, MaterialInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public CreateHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper,
            IMediator mediator
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<MaterialInfo> Handle(CreateRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            await db.BeginTransactionAsync();

            var entity = await db.Set<Domain.Entities.Content.Material.Material>()
                .AddAsync(_mapper.Map<Domain.Entities.Content.Material.Material>(request), cancellationToken);

            await db.CommitTransactionAsync();

            request.Id = entity.Entity.Id;

            return await _mediator.Send(_mapper.Map<GetRequest>(request), cancellationToken);
        }
    }
}
