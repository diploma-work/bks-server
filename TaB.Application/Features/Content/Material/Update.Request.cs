﻿using MediatR;
using TaB.Application.Features.Content.Material.Models;

namespace TaB.Application.Features.Content.Material
{
    public class UpdateRequest : MaterialInfo, IRequest<MaterialInfo> { }
}
