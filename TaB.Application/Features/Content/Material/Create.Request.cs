﻿using MediatR;
using TaB.Application.Features.Content.Material.Models;

namespace TaB.Application.Features.Content.Material
{
    public class CreateRequest : IRequest<MaterialInfo>
    {
        public int Id { get; set; }
        public string Title { get; set; } = default!;
        public string ContentString { get; set; } = default!;
    }
}
