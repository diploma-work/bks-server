﻿using System.Linq.Expressions;
using TaB.SharedKernel.Extensions;
using TaB.SharedKernel.Interfaces;
using TaB.SharedKernel.Query;

namespace TaB.Application.Features.Content.Material
{
    public class GetAllQuery : SortedQuery<Domain.Entities.Content.Material.Material>, IMapFrom<GetAllFilter>
    {
        public string? FreeText { get; set; }
        public NewsFilterInfo? Data { get; set; }

        public override Expression<Func<Domain.Entities.Content.Material.Material, bool>> GetExpression()
        {
            var filter = base.GetExpression();

            if (!string.IsNullOrEmpty(FreeText))
                FreeText.Split().ForEach(term =>
                {
                    filter = filter.And(c => c.Metadata.Title.Contains(term) ||
                                             c.Metadata.ContentString.Contains(term));
                });

            if (Data == null) return filter;

            if (!Data.Statuses.IsEmpty())
                filter = filter.And(x => Data.Statuses!.Contains(x.Status));

            return filter;
        }

        public override Func<IQueryable<Domain.Entities.Content.Material.Material>, IOrderedQueryable<Domain.Entities.Content.Material.Material>> GetSortingExpression()
        {
            return SortColumn switch
            {
                _ => item => item.ApplyOrder(SortColumn, IsAscending)
            };
        }
    }
}
