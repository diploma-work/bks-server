﻿using MediatR;

namespace TaB.Application.Features.Content.Likes
{
    public class DeleteRequest : IRequest<Unit>
    {
        public int ContentId { get; set; }
    }
}
