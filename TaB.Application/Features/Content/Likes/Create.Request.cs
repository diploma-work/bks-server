﻿using MediatR;
using TaB.Application.Features.Content.Likes.Models;

namespace TaB.Application.Features.Content.Likes
{
    public class CreateRequest : IRequest<LikesInNewsInfo>
    {
        public int ContentId { get; set; }
    }
}
