﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.Likes.Models;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Content.Likes
{
    public class GetHandler : IRequestHandler<GetRequest, LikesInNewsInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly ICurrentUserService _currentUserService;

        public GetHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            ICurrentUserService currentUserService
            )
        {
            _dbContextFactory = dbContextFactory;
            _currentUserService = currentUserService;
        }

        public async Task<LikesInNewsInfo> Handle(GetRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();

            var content = await db.Set<Domain.Entities.Content.News.News>()
              .FirstOrDefaultAsync(x => x.Id == request.ContentId);

            if (content == null) throw new NotFoundException($"Not found News/{request.ContentId}");

            return new LikesInNewsInfo()
            {
                ContentId = request.ContentId,
                Count = await db.Set<Domain.Entities.Like>()
                             .CountAsync(x => x.ContentId == request.ContentId),
                IsLikedByCurrentUser = _currentUserService.UserId != null ? (await db.Set<Domain.Entities.Like>()
                                            .FirstOrDefaultAsync(x =>
                                                x.UserLoginId == _currentUserService.Id &&
                                                x.ContentId == request.ContentId
                                            ) != null) : false
            }; 
        }
    }
}
