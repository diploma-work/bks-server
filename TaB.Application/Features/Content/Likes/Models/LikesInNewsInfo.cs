﻿namespace TaB.Application.Features.Content.Likes.Models
{
    public class LikesInNewsInfo
    {
        public int ContentId { get; set; }
        public int Count { get; set; }
        public bool IsLikedByCurrentUser { get; set; }
    }
}
