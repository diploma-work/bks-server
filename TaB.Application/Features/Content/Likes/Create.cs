﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Content.Likes.Models;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Content.Likes
{
    public class CreateHandler : IRequestHandler<CreateRequest, LikesInNewsInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMediator _mediator;
        private readonly ICurrentUserService _currentUserService;

        public CreateHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMediator mediator,
            ICurrentUserService currentUserService
            )
        {
            _dbContextFactory = dbContextFactory;
            _mediator = mediator;
            _currentUserService = currentUserService;
        }

        public async Task<LikesInNewsInfo> Handle(CreateRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            await db.BeginTransactionAsync();

            var content = await db.Set<Domain.Entities.Content.News.News>()
              .FirstOrDefaultAsync(x => x.Id == request.ContentId);

            if (content == null) throw new NotFoundException($"Not found News/{request.ContentId}");

            var entity = await db.Set<Domain.Entities.Like>()
                .FirstOrDefaultAsync(x=>x.UserLoginId == _currentUserService.Id && x.ContentId == request.ContentId);

            if (entity == null)
            {
                await db.Set<Domain.Entities.Like>()
                    .AddAsync(new Domain.Entities.Like() { ContentId = request.ContentId, UserLoginId = _currentUserService.Id });
            }

            await db.CommitTransactionAsync();

            return await _mediator.Send(new GetRequest() { ContentId = request.ContentId});
        }
    }
}
