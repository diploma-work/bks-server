﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Content.Likes
{
    public class DeleteHandler : IRequestHandler<DeleteRequest, Unit>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContext;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;

        public DeleteHandler(
            IDbContextFactory<ApplicationDbContext> dbContext,
            IMapper mapper,
            ICurrentUserService currentUserService
        )
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _currentUserService = currentUserService;
        }

        public async Task<Unit> Handle(DeleteRequest request, CancellationToken cancellationToken)
        {
            using var db = _dbContext.CreateDbContext();
            await db.BeginTransactionAsync();

            var entity = await db.Set<Domain.Entities.Like>()
              .FirstOrDefaultAsync(x=>x.ContentId == request.ContentId && x.UserLoginId == _currentUserService.Id);

            if (entity == null) throw new NotFoundException($"Not found Like/{request.ContentId}, {_currentUserService.Id}");

            db.Set<Domain.Entities.Like>()
                .Remove(entity);

            await db.CommitTransactionAsync();

            return Unit.Value;
        }
    }
}
