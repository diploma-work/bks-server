﻿using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Testing.Models
{
    public partial class AnswerInfo : IMapFrom<AnswerInfo>
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Text { get; set; } = default!;
        public bool IsCorrect { get; set; }
    }
}
