﻿using AutoMapper;
using TaB.Domain.Entities.Testing;
using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Testing.Models
{
    public class TestShortInfo : IMapFrom<TestShortInfo>
    {
        public int Id { get; set; }
        public string Topic { get; set; } = default!;
        public int QuestionsCount { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Test, TestShortInfo>().ReverseMap();
        }
    }
}
