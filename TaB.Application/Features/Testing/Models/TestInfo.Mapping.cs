﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using TaB.Domain.Entities.Testing;

namespace TaB.Application.Features.Testing.Models
{
    public partial class TestInfo
    {
        public void Mapping(Profile profile)
        {
            profile.CreateMap<TestInfo, CreateRequest>().ReverseMap();
            profile.CreateMap<TestInfo, UpdateRequest>().ReverseMap();

            profile.CreateMap<TestInfo, Test>()
                .EqualityComparison((src, dest) => src.Id == dest.Id)
                .ForMember(dest => dest.Questions, opt => opt.MapFrom(src => src.Questions));

            profile.CreateMap<Test, TestInfo>()
               .EqualityComparison((src, dest) => src.Id == dest.Id)
               .ForMember(dest => dest.Questions, opt => opt.MapFrom(src => src.Questions));
        }
    }
}
