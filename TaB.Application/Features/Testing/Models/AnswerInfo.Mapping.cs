﻿using AutoMapper;
using TaB.Domain.Entities.Testing;

namespace TaB.Application.Features.Testing.Models
{
    public partial class AnswerInfo
    {
        public void Mapping(Profile profile)
        {
            profile.CreateMap<AnswerInfo, Answer>().ReverseMap();
        }
    }
}
