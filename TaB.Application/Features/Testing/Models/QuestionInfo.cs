﻿using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Testing.Models
{
    public partial class QuestionInfo : IMapFrom<QuestionInfo>
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public string Text { get; set; } = default!;

        public ICollection<AnswerInfo> Answers { get; set; } = default!;
    }
}
