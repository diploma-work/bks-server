﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using TaB.Domain.Entities.Testing;

namespace TaB.Application.Features.Testing.Models
{
    public partial class QuestionInfo
    {
        public void Mapping(Profile profile)
        {
            profile.CreateMap<QuestionInfo, Question>()
                .EqualityComparison((src, dest) => src.Id == dest.Id)
                .ForMember(dest => dest.Answers, opt => opt.MapFrom(src => src.Answers));

            profile.CreateMap<Question, QuestionInfo>()
               .EqualityComparison((src, dest) => src.Id == dest.Id)
               .ForMember(dest => dest.Answers, opt => opt.MapFrom(src => src.Answers));
        }
    }
}
