﻿using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Testing.Models
{
    public partial class TestInfo : IMapFrom<TestInfo>
    {
        public int Id { get; set; }
        public string Topic { get; set; } = default!;
        public int QuestionsCount { get; set; }

        public List<QuestionInfo> Questions { get; set; } = default!;
    }
}
