﻿using MediatR;
using TaB.Application.Features.Testing.Models;

namespace TaB.Application.Features.Testing
{
    public class CreateRequest : TestInfo, IRequest<TestInfo>
    {
    }
}
