﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Testing.Models;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Extensions;

namespace TaB.Application.Features.Testing
{
    public class UpdateHandler : IRequestHandler<UpdateRequest, TestInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public UpdateHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper,
            IMediator mediator
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<TestInfo> Handle(UpdateRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            await db.BeginTransactionAsync();

            var entity = await db.Tests
                .ApplyQuery(_mapper.Map<GetQuery>(request))
                .Include(x => x.Questions).ThenInclude(x => x.Answers)
                .FirstOrDefaultAsync(cancellationToken);

            if (entity == null) throw new NotFoundException($"Not found Test/{request.Id}");

            entity.QuestionsCount = entity.Questions?.Count ?? 0;

            entity = _mapper.Map(request, entity);
            db.Set<Domain.Entities.Testing.Test>()
                .AddOrUpdate(entity, x=>x.Id);

            await db.CommitTransactionAsync();

            return await _mediator.Send(_mapper.Map<GetRequest>(request), cancellationToken);
        }

    }
}
