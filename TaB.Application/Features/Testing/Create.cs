﻿using AutoMapper;
using AutoMapper.EntityFrameworkCore;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Testing.Models;
using TaB.Domain.Entities.Testing;
using TaB.Persistence;

namespace TaB.Application.Features.Testing
{
    public class CreateHandler : IRequestHandler<CreateRequest, TestInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public CreateHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper,
            IMediator mediator
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<TestInfo> Handle(CreateRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();

            await db.BeginTransactionAsync();
            var entity = _mapper.Map<TestInfo>(request);

            entity.QuestionsCount = entity.Questions?.Count ?? 0;

            db.Tests
               .AddOrUpdate(_mapper.Map<Test>(entity),x=>x.Id);

            await db.CommitTransactionAsync();

            return new TestInfo();
        }
    }
}
