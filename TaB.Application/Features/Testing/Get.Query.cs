﻿using AutoMapper;
using System.Linq.Expressions;
using TaB.SharedKernel.Interfaces;
using TaB.SharedKernel.Query;

namespace TaB.Application.Features.Testing
{
    public class GetQuery : Query<Domain.Entities.Testing.Test>, IMapFrom<CreateRequest>
    {
        public int Id { get; set; }

        public override Expression<Func<Domain.Entities.Testing.Test, bool>> GetExpression()
        {
            Expression<Func<Domain.Entities.Testing.Test, bool>> filter = entity => entity.Id == Id;
            return filter;
        }

        public override List<Expression<Func<Domain.Entities.Testing.Test, object>>> GetIncludes()
        {
            var @base = base.GetIncludes();
            @base.Add(x => x.Questions);
            
            return @base;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<UpdateRequest, GetQuery>().ReverseMap();
            profile.CreateMap<UpdateRequest, GetRequest>().ReverseMap();
            profile.CreateMap<CreateRequest, GetQuery>();
            profile.CreateMap<GetRequest, GetQuery>();
        }
    }
}
