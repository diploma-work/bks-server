﻿using AutoMapper;
using System.Linq.Expressions;
using TaB.Application.Features.Content.Material;
using TaB.SharedKernel.Extensions;
using TaB.SharedKernel.Interfaces;
using TaB.SharedKernel.Query;

namespace TaB.Application.Features.Testing
{
    public class GetAllQuery : SortedQuery<Domain.Entities.Testing.Test>, IMapFrom<GetAllFilter>
    {
        public string? FreeText { get; set; }

        public override Expression<Func<Domain.Entities.Testing.Test, bool>> GetExpression()
        {
            var filter = base.GetExpression();

            if (!string.IsNullOrEmpty(FreeText))
                FreeText.Split().ForEach(term =>
                {
                    filter = filter.And(c => c.Topic.Contains(term));
                });

            return filter;
        }

        public override Func<IQueryable<Domain.Entities.Testing.Test>, IOrderedQueryable<Domain.Entities.Testing.Test>> GetSortingExpression()
        {
            return SortColumn switch
            {
                _ => item => item.ApplyOrder(SortColumn, IsAscending)
            };
        }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<GetAllQuery, GetAllRequest>().ReverseMap();
        }

    }
}
