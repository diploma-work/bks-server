﻿using FluentValidation;

namespace TaB.Application.Features.Testing
{
    public class UpdateRequestValidator : AbstractValidator<UpdateRequest>
    {
        public UpdateRequestValidator()
        {
            RuleFor(p => p.Id).GreaterThan(0);
        }
    }
}
