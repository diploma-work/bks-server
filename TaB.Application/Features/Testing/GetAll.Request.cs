﻿using MediatR;
using TaB.Application.Features.Testing.Models;
using TaB.SharedKernel.Filters;
using TaB.SharedKernel.Pagination;

namespace TaB.Application.Features.Testing
{
    public class GetAllRequest : SortedPagedFilter, IRequest<IPagedList<TestShortInfo>> { }
}
