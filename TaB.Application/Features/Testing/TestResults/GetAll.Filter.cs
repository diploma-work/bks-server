﻿using TaB.SharedKernel.Filters;

namespace TaB.Application.Features.Testing.TestResults
{
    public class GetAllFilter : SortedPagedFilter
    {
        public TestResultsFilterInfo? Data { get; set; }

        public GetAllFilter()
        {
            Data = new TestResultsFilterInfo();
        }
    }

    public class TestResultsFilterInfo 
    {
        public List<int>? TestIds { get; set; }
        public List<int>? UserLoginIds { get; set; }
    }
}
