﻿using AutoMapper;
using System.Linq.Expressions;
using TaB.SharedKernel.Interfaces;
using TaB.SharedKernel.Query;

namespace TaB.Application.Features.Testing.TestResults
{
    public class GetQuery : Query<Domain.Entities.Testing.TestResult>, IMapFrom<CreateRequest>
    {
        public int Id { get; set; }

        public override Expression<Func<Domain.Entities.Testing.TestResult, bool>> GetExpression()
        {
            Expression<Func<Domain.Entities.Testing.TestResult, bool>> filter = entity => entity.Id == Id;
            return filter;
        }

        public override List<Expression<Func<Domain.Entities.Testing.TestResult, object>>> GetIncludes()
        {
            var @base = base.GetIncludes();

            return @base;
        }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<GetAllQuery, GetAllRequest>().ReverseMap();
            profile.CreateMap<CreateRequest, GetQuery>();
            profile.CreateMap<GetRequest, GetQuery>();
        }
    }
}
