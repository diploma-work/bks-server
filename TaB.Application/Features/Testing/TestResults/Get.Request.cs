﻿using MediatR;
using TaB.Application.Features.Testing.TestResults.Models;

namespace TaB.Application.Features.Testing.TestResults
{
    public class GetRequest : IRequest<TestResultsInfo>
    {
        public int Id { get; set; }
    }
}
