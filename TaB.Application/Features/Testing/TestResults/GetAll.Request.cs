﻿using MediatR;
using TaB.Application.Features.Testing.TestResults.Models;
using TaB.SharedKernel.Filters;
using TaB.SharedKernel.Pagination;

namespace TaB.Application.Features.Testing.TestResults
{
    public class GetAllRequest : GetAllFilter, IRequest<IPagedList<TestResultsShortInfo>> { }
}
