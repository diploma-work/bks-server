﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Testing.Models;
using TaB.Application.Features.Testing.TestResults.Models;
using TaB.Domain.Entities.Testing;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;

namespace TaB.Application.Features.Testing.TestResults
{
    public class CreateHandler : IRequestHandler<CreateRequest, TestResultsInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public CreateHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper,
            IMediator mediator
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<TestResultsInfo> Handle(CreateRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();

            var test =  await _mediator.Send(new Features.Testing.GetRequest() { Id = request.TestId});

            if (test == null) throw new NotFoundException($"Not found Test/{request.Id}");

            var testResult = ChectTestResult(test, _mapper.Map<TestResultsInfo>(request));

            await db.BeginTransactionAsync();

            var entity = db.Set<Domain.Entities.Testing.TestResult>()
                .FirstOrDefault(x => x.UserLoginId == request.UserLoginId && x.TestId == request.TestId);

            testResult.Id = entity?.Id ?? 0;

            var testResults = db.Set<Domain.Entities.Testing.TestResult>()
              .AddOrUpdate(testResult, x => x.Id);

            await db.CommitTransactionAsync();

            return await _mediator.Send(new GetRequest() { Id = testResults.Id});
        }

        private TestResult ChectTestResult(TestInfo test, TestResultsInfo testResultsInfo)
        {
            foreach(var questionResult in testResultsInfo.Questions)
            {
                bool isCorect = true;

                var question = test.Questions.First(x=>x.Id == questionResult.QuestionId);

                foreach (var answer in question.Answers.Where(x=>x.IsCorrect))
                {
                    if(questionResult.Answers.FirstOrDefault(x=>x.AnswerId == answer.Id) == null)
                    {
                        isCorect = false;
                        break;
                    }
                }

                questionResult.IsCorect = isCorect;
                if (isCorect)
                    testResultsInfo.CorrectQuestionsCount++;
            }

            return _mapper.Map<TestResult>(testResultsInfo);
        }
    }
}
