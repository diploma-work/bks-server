﻿using TaB.Application.Features.Testing.TestResults.Models;
using TaB.SharedKernel.Validation.Filters;

namespace TaB.Application.Features.Testing.TestResults
{
    public class GetAllRequestValidator : FilterValidatorBase<GetAllRequest, TestResultsShortInfo> { }
}
