﻿using System.Linq.Expressions;
using TaB.Application.Features.Testing.TestResults.Models;
using TaB.SharedKernel.Extensions;
using TaB.SharedKernel.Interfaces;
using TaB.SharedKernel.Query;

namespace TaB.Application.Features.Testing.TestResults
{
    public class GetAllQuery : SortedQuery<Domain.Entities.Testing.TestResult>, IMapFrom<GetAllFilter>
    {
        public string? FreeText { get; set; }
        public TestResultsFilterInfo? Data { get; set; }

        public override Expression<Func<Domain.Entities.Testing.TestResult, bool>> GetExpression()
        {
            var filter = base.GetExpression();

            if (!string.IsNullOrEmpty(FreeText))
                FreeText.Split().ForEach(term =>
                {
                    filter = filter.And(c => c.Test.Topic.Contains(term));
                });

            if (Data == null) return filter;

            if (!Data.TestIds.IsEmpty())
                filter = filter.And(x => Data.TestIds!.Contains(x.TestId));

            if (!Data.UserLoginIds.IsEmpty())
                filter = filter.And(x => Data.UserLoginIds!.Contains(x.UserLoginId));

            return filter;
        }

        public override Func<IQueryable<Domain.Entities.Testing.TestResult>, IOrderedQueryable<Domain.Entities.Testing.TestResult>> GetSortingExpression()
        {
            return SortColumn switch
            {
                _ => item => item.ApplyOrder(SortColumn, IsAscending)
            };
        }
    }
}
