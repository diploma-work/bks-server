﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Testing.TestResults.Models;
using TaB.Persistence;
using TaB.SharedKernel.Extensions;
using TaB.SharedKernel.Pagination;

namespace TaB.Application.Features.Testing.TestResults
{
    public class GetAllHandler : IRequestHandler<GetAllRequest, IPagedList<TestResultsShortInfo>>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;

        public GetAllHandler(IDbContextFactory<ApplicationDbContext> dbContextFactory, IMapper mapper)
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
        }

        public async Task<IPagedList<TestResultsShortInfo>> Handle(GetAllRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            return db.TestResults
                .ApplyQuery(_mapper.Map<GetAllQuery>(request))
                .ProjectTo<TestResultsShortInfo>(_mapper.ConfigurationProvider)
                .ApplyPaging(request);
        }
    }
}
