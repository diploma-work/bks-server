﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Testing.TestResults.Models;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Extensions;

namespace TaB.Application.Features.Testing.TestResults
{
    public class GetHandler : IRequestHandler<GetRequest, TestResultsInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;

        public GetHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
        }

        public async Task<TestResultsInfo> Handle(GetRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();

            var entity = await db.Set<Domain.Entities.Testing.TestResult>()
                .AsNoTracking()
                .Include(x=>x.User)
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (entity == null) throw new NotFoundException($"Not found TestResult/{request.Id}");

            return _mapper.Map<TestResultsInfo>(entity);
        }
    }
}
