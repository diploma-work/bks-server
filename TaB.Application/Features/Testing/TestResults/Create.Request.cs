﻿using MediatR;
using TaB.Application.Features.Testing.TestResults.Models;

namespace TaB.Application.Features.Testing.TestResults
{
    public class CreateRequest : TestResultsInfo, IRequest<TestResultsInfo> {}
}
