﻿using AutoMapper;
using TaB.Domain.Entities.Testing;
using TaB.Persistence;

namespace TaB.Application.Features.Testing.TestResults.Models
{
    public class AnswersResultResolver : IValueResolver<TestResultsInfo, TestResult, string>
    { 
        public string Resolve(TestResultsInfo source, TestResult destination, string destMember, ResolutionContext context)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(source.Questions);
        }
    }

    public class ReverseAnswerResolver : IValueResolver<TestResult, TestResultsInfo, List<QuestionResultInfo>>
    {
        public List<QuestionResultInfo> Resolve(TestResult source, TestResultsInfo destination, List<QuestionResultInfo> destMember, ResolutionContext context)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<List<QuestionResultInfo>>(source.AnswersText);
        }
    }
}
