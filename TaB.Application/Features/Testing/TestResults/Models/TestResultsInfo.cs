﻿using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Testing.TestResults.Models
{
    public partial class TestResultsInfo : IMapFrom<TestResultsInfo>
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public int UserLoginId { get; set; }
        public string? UserId { get; set; }
        public int? CorrectQuestionsCount { get; set; }

        public List<QuestionResultInfo> Questions { get; set; } = default!;
    }
}
