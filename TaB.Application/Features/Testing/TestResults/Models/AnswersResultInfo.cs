﻿namespace TaB.Application.Features.Testing.TestResults.Models
{
    public class AnswersResultInfo
    {
        public int AnswerId { get; set; }
        public string? Text { get; set; }
    }
}
