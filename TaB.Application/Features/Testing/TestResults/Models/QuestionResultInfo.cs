﻿namespace TaB.Application.Features.Testing.TestResults.Models
{
    public class QuestionResultInfo
    {
        public int QuestionId { get; set; }
        public string? Text { get; set; }
        public bool? IsCorect { get; set; }

        public List<AnswersResultInfo> Answers { get; set; } = default!;
    }
}
