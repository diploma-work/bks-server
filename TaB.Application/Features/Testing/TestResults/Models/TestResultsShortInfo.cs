﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using TaB.Domain.Entities.Testing;
using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Testing.TestResults.Models
{
    public class TestResultsShortInfo : IMapFrom<TestResultsShortInfo>
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public int UserLoginId { get; set; }
        public string UserId { get; set; } = default!;
        public int CorrectQuestionsCount { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<TestResultsShortInfo, TestResult>()
                .EqualityComparison((src, dest) => src.Id == dest.Id);

            profile.CreateMap<TestResult, TestResultsShortInfo>()
                .EqualityComparison((src, dest) => src.Id == dest.Id)
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User.UserId));

        }
    }
}
