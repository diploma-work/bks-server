﻿using AutoMapper;
using AutoMapper.EquivalencyExpression;
using TaB.Domain.Entities.Testing;

namespace TaB.Application.Features.Testing.TestResults.Models
{
    public partial class TestResultsInfo
    {
        public void Mapping(Profile profile)
        {
            profile.CreateMap<TestResultsInfo, TestResult>()
                .EqualityComparison((src, dest) => src.Id == dest.Id)
                .ForMember(dest => dest.AnswersText, opt => opt.MapFrom<AnswersResultResolver>());

            profile.CreateMap<TestResult, TestResultsInfo>()
               .EqualityComparison((src, dest) => src.Id == dest.Id)
               .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User.UserId))
               .ForMember(dest => dest.Questions, opt => opt.MapFrom<ReverseAnswerResolver>());

            profile.CreateMap<TestResultsInfo, CreateRequest>().ReverseMap();
        }
    }
}
