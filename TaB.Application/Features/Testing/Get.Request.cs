﻿using MediatR;
using TaB.Application.Features.Testing.Models;

namespace TaB.Application.Features.Testing
{
    public class GetRequest : IRequest<TestInfo>
    {
        public int Id { get; set; }
    }
}
