﻿using TaB.Application.Features.Testing.Models;
using TaB.SharedKernel.Validation.Filters;

namespace TaB.Application.Features.Testing
{
    public class GetAllRequestValidator : FilterValidatorBase<GetAllRequest, TestShortInfo> { }
}
