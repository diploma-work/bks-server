﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.Testing.Models;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Extensions;

namespace TaB.Application.Features.Testing
{
    public class GetHandler : IRequestHandler<GetRequest, TestInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;

        public GetHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
        }

        public async Task<TestInfo> Handle(GetRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();

            return await db.Set<Domain.Entities.Testing.Test>()
                .Include(x=>x.Questions).ThenInclude(x=>x.Answers)
                .AsNoTracking()
                //.ApplyQuery(_mapper.Map<GetQuery>(request))
                .ProjectTo<TestInfo>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(cancellationToken)
                   ?? throw new NotFoundException($"Not found TestInfo/{request.Id}");
        }
    }
}
