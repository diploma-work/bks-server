﻿using AutoMapper;
using TaB.Domain.Enums;
using TaB.Persistence;
using TaB.SharedKernel;
using TaB.SharedKernel.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace TaB.Application.Features.Files
{
    public class UploadHandler : BaseUpload, IRequestHandler<UploadRequest, FilesInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;

        public UploadHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory, 
            IMapper mapper,
            IOptions<StorageOptions> storageOptions
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
            _storageOptions = storageOptions.Value;
        }

        public async Task<FilesInfo> Handle(UploadRequest request, CancellationToken cancellationToken)
        {
            var localFile = GetLocalFileName(request.File.FileName);
            var uploadPath = _storageOptions.DocumentsFolder;
            uploadPath = Path.Combine(uploadPath, localFile);

            await using (var fileStream =
                new FileStream(uploadPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                await request.File.CopyToAsync(fileStream, cancellationToken);
                await fileStream.FlushAsync(cancellationToken);
            }

            var file = new Domain.Entities.File
            {
                Size = request.File.Length,
                FileType = request.FileType == FileType.Unknown ? GetFileType(request.File.FileName) : request.FileType,
                Name = request.File.FileName,
                Link = localFile
            };

            await using var db = _dbContextFactory.CreateDbContext();
            await db.BeginTransactionAsync();

            await db.Set<Domain.Entities.File>().AddAsync(file, cancellationToken);

            await db.CommitTransactionAsync();

            return _mapper.Map<FilesInfo>(file);
        }
    }
}