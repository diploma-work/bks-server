﻿using AutoMapper;
using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.Files.Models
{
    public class FileInfo : IMapFrom<FileInfo>
    {
        public void Mapping(Profile profile)
        {
            profile.CreateMap<UploadRequest, SharedKernel.Models.FilesInfo>().ReverseMap();
        }
    }
}
