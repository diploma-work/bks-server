﻿using TaB.Domain.Enums;
using MediatR;
using Microsoft.AspNetCore.Http;
using TaB.SharedKernel.Models;

namespace TaB.Application.Features.Files
{
    public class UploadRequest : IRequest<FilesInfo>
    {
        public IFormFile File { get; set; } = default!;
        public FileType FileType { get; set; } = FileType.Unknown;
    }
}
