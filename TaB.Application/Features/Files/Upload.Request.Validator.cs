﻿using FluentValidation;
using TaB.SharedKernel;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace TaB.Application.Features.Files
{
    public class UploadRequestValidator : AbstractValidator<UploadRequest>
    {
        private readonly StorageOptions _storageOptions;

        public UploadRequestValidator(IOptions<StorageOptions> storageOptions)
        {
            _storageOptions = storageOptions.Value;

            RuleFor(p => p.File).Must(Allowed).WithMessage("This file type cannot be uploaded."); ;
        }

        private bool Allowed(IFormFile file)
        {
            var extension = Path.GetExtension(file.FileName);
            return _storageOptions.AllowedFileExtensions.Contains(extension?.ToLower() ?? string.Empty);
        }
    }
}