﻿using TaB.Domain.Enums;
using TaB.SharedKernel;

namespace TaB.Application.Features.Files
{
    public class BaseUpload
    {
        protected StorageOptions _storageOptions = null!;

        protected string GetLocalFileName(string fileName)
        {
            var extension = !string.IsNullOrWhiteSpace(fileName) ? Path.GetExtension(fileName) : "";

            return Guid.NewGuid().ToString("N") + extension;
        }

        //protected string CreateHash(string localFileName)
        //{
        //    var hash = string.Empty;

        //    var path = Path.Combine(_storageOptions.DocumentsFolder, localFileName);
        //    if (!File.Exists(path)) return hash;

        //    using var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);

        //    HashAlgorithm murmur128 = MurmurHash.Create128(managed: false); // returns a 128-bit algorithm using "unsafe" code with default seed
        //    var hashBytes = murmur128.ComputeHash(fs);
        //    hash = hashBytes.ToHex();

        //    return hash;
        //}

        protected FileType GetFileType(string fileName)
        {
            var extensionToFileType = new Dictionary<string, FileType>
            {
                { ".pdf", FileType.Pdf },
                { ".jpg", FileType.Image },
                { ".jpeg", FileType.Image },
                { ".png", FileType.Image },
                { ".gif", FileType.Image },
                { ".bmp", FileType.Image },
                { "", FileType.Unknown }
            };
            var extension = !string.IsNullOrWhiteSpace(fileName) ? Path.GetExtension(fileName) : "";

            return extensionToFileType.ContainsKey(extension) ? extensionToFileType[extension] : FileType.Unknown;
        }
    }
}