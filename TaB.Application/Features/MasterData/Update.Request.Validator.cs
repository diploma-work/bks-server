﻿using FluentValidation;

namespace TaB.Application.Features.MasterData
{
    public class UpdateRequestValidator : AbstractValidator<UpdateRequest>
    {
        public UpdateRequestValidator()
        {
            RuleFor(p => p.Id).GreaterThan(0);

            RuleFor(p => p.Value)
                .NotEmpty()
                .NotNull();
        }
    }
}
