﻿using MediatR;
using TaB.Application.Features.MasterData.Models;

namespace TaB.Application.Features.MasterData
{
    public class UpdateRequest : IRequest<MasterDataInfo>
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
