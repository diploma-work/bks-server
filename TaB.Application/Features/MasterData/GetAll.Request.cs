﻿using MediatR;
using TaB.Application.Features.MasterData.Models;

namespace TaB.Application.Features.MasterData
{
    public class GetAllRequest : IRequest<List<MasterDataInfo>> { }
}
