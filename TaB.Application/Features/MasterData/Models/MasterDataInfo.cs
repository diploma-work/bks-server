﻿using AutoMapper;
using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Features.MasterData.Models
{
    public class MasterDataInfo : Domain.Entities.MasterData, IMapFrom<Domain.Entities.MasterData>
    {
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Domain.Entities.MasterData, MasterDataInfo>().ReverseMap();
        }
    }
}
