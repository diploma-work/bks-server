﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.MasterData.Models;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions;

namespace TaB.Application.Features.MasterData
{
    public class UpdateHandler : IRequestHandler<UpdateRequest, MasterDataInfo>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public UpdateHandler(
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            IMapper mapper,
            IMediator mediator
            )
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<MasterDataInfo> Handle(UpdateRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            await db.BeginTransactionAsync();

            var entity = await db.Set<Domain.Entities.MasterData>()
                .FirstOrDefaultAsync(x=>x.Id == request.Id);

            if (entity == null) throw new NotFoundException($"Not found MasterData/{request.Id}");

            entity.Value = request.Value;

            db.Set<Domain.Entities.MasterData>()
                .Update(entity);

            await db.CommitTransactionAsync();

            return _mapper.Map<MasterDataInfo>(entity);
        }

    }
}
