﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TaB.Application.Features.MasterData.Models;
using TaB.Persistence;

namespace TaB.Application.Features.MasterData
{
    public class GetAllHandler : IRequestHandler<GetAllRequest, List<MasterDataInfo>>
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly IMapper _mapper;

        public GetAllHandler(IDbContextFactory<ApplicationDbContext> dbContextFactory, IMapper mapper)
        {
            _dbContextFactory = dbContextFactory;
            _mapper = mapper;
        }

        public async Task<List<MasterDataInfo>> Handle(GetAllRequest request, CancellationToken cancellationToken)
        {
            await using var db = _dbContextFactory.CreateDbContext();
            return db.Set<Domain.Entities.MasterData>()
                .ProjectTo<MasterDataInfo>(_mapper.ConfigurationProvider)
                .ToList();
        }
    }
}
