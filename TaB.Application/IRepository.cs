﻿using System.Linq.Expressions;
using TaB.SharedKernel.Query;

namespace TaB.Application
{
    public interface IRepository : IDisposable
    {
        void Add<T>(T entity) where T : class;
        void AddOrUpdate<T>(T entity, Expression<Func<T, object>> checkCriteria) where T : class;
        void AddOrUpdate<T>(List<T> entities, Expression<Func<T, object>> checkCriteria) where T : class;
        void Update<T>(T entity) where T : class;
        void Remove<T>(T entity) where T : class;
        IQueryable<T> GetAll<T>() where T : class;
        IQueryable<T> GetAllBy<T>(Expression<Func<T, bool>> criteria, List<Expression<Func<T, object>>> includes = null) where T : class;
        IQueryable<T> ByQuery<T>(IQuery<T> query) where T : class;
        IOrderedQueryable<T> ByQuery<T, K>(IGroupedSortedQuery<T, K> query) where T : class;
        IOrderedQueryable<T> ByQuery<T>(ISortedQuery<T> query) where T : class;
        T GetBy<T>(Expression<Func<T, bool>> criteria, List<Expression<Func<T, object>>> includes = null) where T : class;
        string GetTableName<T>() where T : class;
    }
}
