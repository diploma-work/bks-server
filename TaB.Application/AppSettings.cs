﻿using TaB.SharedKernel.Installers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;

namespace TaB.Application
{
    public class AppSettings
    {
        public static string GetAbsoluteUri(string relativeUri)
        {
            string redirectUri = relativeUri;

            var accessor = ServiceLocator.Resolve<IHttpContextAccessor>();

            var uri = new Uri(relativeUri, UriKind.RelativeOrAbsolute);
            if (!uri.IsAbsoluteUri)
            {
                var baseUri = accessor.HttpContext.Request.GetEncodedUrl();

                if (!string.IsNullOrWhiteSpace(baseUri) && !baseUri.EndsWith("/"))
                {
                    baseUri += "/";
                }
                redirectUri = baseUri + redirectUri;
            }

            return redirectUri;
        }

        public static string GetApplicationPath()
        {
            var accessor = ServiceLocator.Resolve<IHttpContextAccessor>();

            var baseHref = accessor.HttpContext.Request.PathBase.Value;
            if (string.IsNullOrEmpty(baseHref))
            {
                return "/";
            }
            else if (baseHref.EndsWith("/"))
            {
                return baseHref;
            }
            else
            {
                return baseHref + "/";
            }
        }
    }
}
