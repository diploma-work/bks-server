﻿using Microsoft.Extensions.Logging;

namespace TaB.Application.Services
{
    public class BaseAbstractService<TService> where TService : IService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        protected ILogger<TService> Logger { get; }

        private BaseAbstractService(ILogger<TService> logger) => Logger = logger;

        protected BaseAbstractService(IUnitOfWorkFactory unitOfWorkFactory, ILogger<TService> logger) : this(logger) => _unitOfWorkFactory = unitOfWorkFactory ?? throw new ArgumentNullException(nameof(unitOfWorkFactory));

        protected IUnitOfWork GetUnitOfWork() => _unitOfWorkFactory.GetUnitOfWork();
    }
}