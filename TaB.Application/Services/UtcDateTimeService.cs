﻿using TaB.SharedKernel.Interfaces;

namespace TaB.Application.Services
{
    public class UtcDateTimeService : IDateTime
    {
        public DateTime Now => DateTime.UtcNow;
    }
}
