﻿using System.Linq.Expressions;
using TaB.SharedKernel.Filters;
using TaB.SharedKernel.Pagination;

namespace TaB.Application.Services
{
    public interface IService {}

    public interface IBaseService<TEntity, TType, in TTypeFilter> : IService
                                            where TEntity : class
                                            where TTypeFilter : SortedPagedFilter
    {
        List<TType> GetAll();
        List<TType> GetAll(Expression<Func<TEntity, bool>> criteria);
        IPagedList<TType> GetAll(TTypeFilter filter);
        TType? GetBy(Expression<Func<TEntity, bool>> criteria);
        TType Add(TType data);
        TType AddOrUpdate(TType data, Expression<Func<TEntity, object>> checkCriteria);
        TType Update(TType data);
        void Delete(Expression<Func<TEntity, bool>> criteria);
        void DeleteMany(Expression<Func<TEntity, bool>> criteria);
    }
}