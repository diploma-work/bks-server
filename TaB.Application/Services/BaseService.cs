﻿using System.Linq.Expressions;
using System.Net;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Extensions;
using TaB.SharedKernel.Filters;
using TaB.SharedKernel.Pagination;
using TaB.SharedKernel.Query;
using Microsoft.Extensions.Logging;

namespace TaB.Application.Services
{
    public class BaseService<TEntity, TType, TTypeFilter, TTypeQuery, TService> : BaseAbstractService<TService>, IBaseService<TEntity, TType, TTypeFilter> where TTypeFilter : SortedPagedFilter 
                                                                                                         where TEntity : class
                                                                                                         where TTypeQuery: ISortedQuery<TEntity>
                                                                                                         where TService : IService
    {
        protected IMapper Mapper { get; }

        protected BaseService(IUnitOfWorkFactory unitOfWorkFactory, ILogger<TService> logger, IMapper mapper) : base(unitOfWorkFactory, logger)
        {
            Mapper = mapper;
        }

        public List<TType> GetAll()
        {
            using var uow = GetUnitOfWork();
            return uow.Repository
                      .GetAll<TEntity>()
                      .ProjectTo<TType>(Mapper.ConfigurationProvider)
                      .ToList();
        }

        public List<TType> GetAll(Expression<Func<TEntity, bool>> criteria)
        {
            using var uow = GetUnitOfWork();
            return uow.Repository
                      .GetAll<TEntity>()
                      .Where(criteria)
                      .ProjectTo<TType>(Mapper.ConfigurationProvider)
                      .ToList();
        }

        public IPagedList<TType> GetAll(TTypeFilter filter)
        {
            var query = Mapper.Map<TTypeQuery>(filter);
            using var uow = GetUnitOfWork();
            return uow.Repository
                      .ByQuery(query)
                      .ProjectTo<TType>(Mapper.ConfigurationProvider)
                      .ApplyPaging(filter.PageNumber, filter.PageSize)
                ;//.ProjectTo<TEntity, TType>();
        }

        public TType? GetBy(Expression<Func<TEntity, bool>> criteria)
        {
            var query = Activator.CreateInstance<TTypeQuery>();
            using var uow = GetUnitOfWork();
            return uow.Repository
                      .GetBy<TEntity>(criteria, query.GetIncludes())
                      .ProjectTo<TEntity, TType>(Mapper);
            //?? throw new ExceptionBase(HttpStatusCode.NotFound, $"Not found {typeof(TEntity).Name}");
        }

        //public TType GetByCode(string code)
        //{
        //    using (var uow = GetUnitOfWork())
        //    {
        //        return uow.Repository
        //                   .GetAll<TEntity>()
        //                   .Where(x => x.Code == code)
        //                   .ProjectTo<TType>()
        //                   .FirstOrDefault() ?? throw new ExceptionBase(HttpStatusCode.NotFound, $"Not found {typeof(TEntity).Name} (code = {code})");
        //    }
        //}

        public TType Add(TType data)
        {
            using var uow = GetUnitOfWork();
            var entity = Mapper.Map<TEntity>(data);
            uow.Repository.Add(entity);
            uow.Commit();

            return Mapper.Map<TType>(entity);//GetById(contentType.Id);
        }

        public TType AddOrUpdate(TType data, Expression<Func<TEntity, object>> checkCriteria)
        {
            using var uow = GetUnitOfWork();
            var entity = Mapper.Map<TEntity>(data);
            uow.Repository.AddOrUpdate(entity, checkCriteria);
            uow.Commit();

            return Mapper.Map<TType>(entity);//GetById(contentType.Id);
        }
        
        public TType Update(TType data)
        {
            using var uow = GetUnitOfWork();
            var entity = Mapper.Map<TEntity>(data);
            uow.Repository.Update(entity);
            uow.Commit();

            return Mapper.Map<TType>(entity);//GetById(contentType.Id);
        }

        public void Delete(Expression<Func<TEntity, bool>> criteria)
        {
            using var uow = GetUnitOfWork();
            var entity = uow.Repository.GetBy<TEntity>(criteria) ?? throw new ExceptionBase(HttpStatusCode.NotFound, $"Not found {typeof(TEntity).Name} ({criteria.Body})");
            uow.Repository.Remove(entity);
            uow.Commit();
        }

        public void DeleteMany(Expression<Func<TEntity, bool>> criteria)
        {
            using var uow = GetUnitOfWork();
            var entities = uow.Repository.GetAll<TEntity>().Where(criteria);
            entities.ForEach(x => uow.Repository.Remove(x));
            uow.Commit();
        }

        protected void AddOrUpdate(List<TType> list, Expression<Func<TEntity, object>> checkCriteria)
        {
            using var uow = GetUnitOfWork();
            var entities = Mapper.Map<List<TEntity>>(list);
            uow.Repository.AddOrUpdate(entities, checkCriteria);
            uow.Commit();
        }
    }
}