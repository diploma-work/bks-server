﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace TaB.Application
{
    public class AuthOptions
    {
        public string AppPrefix { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string Key { get; set; }
        public int AccessTokenExpireTimeSpanInMinutes { get; set; }
        public int RefreshTokenExpireTimeSpanInMinutes { get; set; }
        public bool AllowInsecureTokenRequest { get; set; }
        public bool WindowsAuthEnabled { get; set; }
        public string WindowsAuthServerUrl { get; set; }
        public SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
        public string[] AllowedRoles { get; set; }
    }
}
