﻿using Microsoft.EntityFrameworkCore;

namespace TaB.Application
{
    public class UnitOfWork<TDbContext> : IUnitOfWork where TDbContext: DbContext
    {
        private bool _disposed;
        private readonly TDbContext _dbContext;
        private readonly Repository<TDbContext> _repository;
        private string _iiTable;

        public UnitOfWork(TDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _repository = new Repository<TDbContext>(dbContext);
        }

        public IRepository Repository => _repository;

        public void Commit()
        {
            if (string.IsNullOrEmpty(_iiTable)) _dbContext.SaveChanges();
            else
            {
                //using var tr = _dbContext.Database.BeginTransaction();
                //_dbContext.Database.ExecuteSqlRaw($"SET IDENTITY_INSERT {_iiTable} ON");
                _dbContext.SaveChanges();
                //_dbContext.Database.ExecuteSqlRaw($"SET IDENTITY_INSERT {_iiTable} OFF");
                //tr.Commit();
            }
        }

        public void SetIdentityInsert<T>() where T : class
        {
            _iiTable = Repository.GetTableName<T>();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _repository.Dispose();
            }

            _disposed = true;
        }
    }
}
