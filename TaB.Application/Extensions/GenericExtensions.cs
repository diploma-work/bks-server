﻿namespace TaB.Application.Extensions
{
    public static class GenericExtensions
    {
        public static T Clone<T>(this T original)
        {
            if (original == null) throw new ArgumentNullException(nameof(original));
            var newObject = (T)Activator.CreateInstance(original.GetType())!;

            foreach (var originalProp in original.GetType().GetProperties())
            {
                originalProp.SetValue(newObject, originalProp.GetValue(original));
            }

            return newObject;
        }
    }
}
