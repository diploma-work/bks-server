﻿namespace TaB.Application.Extensions
{
    public static class StringExtension
    {
        public static bool ContainsIgnoreCase(this string s, string subs)
        {
            return s.IndexOf(subs, StringComparison.InvariantCultureIgnoreCase) >= 0;
        }
    }
}