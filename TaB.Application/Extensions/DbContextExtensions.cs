﻿using TaB.Persistence;
using Microsoft.EntityFrameworkCore;

namespace TaB.Application.Extensions
{
    public static class DbContextExtension
    {
        public static void CollectionUpdater<T>(this ApplicationDbContext dbContext, ICollection<T>? api, ICollection<T>? db, Func<T, T, bool> comparerFunc, Func<T, int> hashCodeFunc, bool onlyUpdate = false)
        {
            api ??= new List<T>();
            db ??= new List<T>();
            dbContext.CollectionUpdater(api.AsEnumerable(), db.AsEnumerable(), comparerFunc, hashCodeFunc, onlyUpdate);
        }

        public static void CollectionUpdater<T>(this ApplicationDbContext dbContext, IEnumerable<T>? api, IEnumerable<T>? db, Func<T, T, bool> comparerFunc, Func<T, int> hashCodeFunc, bool onlyUpdate = false)
        {
            api ??= new List<T>();
            db ??= new List<T>();
            var comparer = SharedKernel.Helpers.EqualityComparer<T>.Create(comparerFunc, hashCodeFunc);
            var deleted = !onlyUpdate ? db.Except(api, comparer).ToList() : new List<T>();
            deleted.ForEach(c => dbContext.Entry(c).State = EntityState.Deleted);
            var added = !onlyUpdate ? api.Except(db, comparer).ToList() : new List<T>();
            added.ForEach(c => dbContext.Entry(c).State = EntityState.Added);
            var updated = db.Intersect(api, comparer).ToList();
            updated.ForEach(c => dbContext.Entry(c).CurrentValues.SetValues(api.FirstOrDefault(x => comparerFunc(x, c))));
        }

    }
}