﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace TaB.Application.Extensions
{
    /// <summary>
    /// IAF specific Decimal converter.
    /// Supports decimal strings in both US or in DE formats.
    /// See test cases for details of what is supported.
    /// </summary>
    public static class DecimalConverterExtention
    {
        private static readonly Regex _deCulturePattern = new Regex(@"^(:?-?[\d.]*,)*-?\d+$");
        private static readonly Regex _usCulturePattern = new Regex(@"^(:?-?[\d,]*\.)*-?\d+$");
        private static readonly CultureInfo _deCultureInfo = new CultureInfo("de-DE");
        private static readonly CultureInfo _usCultureInfo = new CultureInfo("en-US");

        /// <summary>
        /// Converts the string representation of a number to its System.Decimal equivalent.
        /// A return value indicates whether the conversion succeeded or failed.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool TryParse(string? s, out decimal result)
        {
            var styles = NumberStyles.Number;

            CultureInfo cultureInfo;
            if (string.IsNullOrEmpty(s))
            {
                result = 0;
                return false;
            }
            else if (_deCulturePattern.IsMatch(s))
            {
                cultureInfo = _deCultureInfo;
            }
            else if (_usCulturePattern.IsMatch(s))
            {
                cultureInfo = _usCultureInfo;
            }
            else
            {
                result = 0;
                return false;
            }

            return decimal.TryParse(s, styles, cultureInfo, out result);
        }

        /// <summary>
        /// Converts the string representation of a number to its System.Decimal equivalent.
        /// </summary>
        /// <param name="s">The string representation of the number to convert.</param>
        /// <returns>The equivalent to the number contained in s.</returns>
        /// <exception cref="System.ArgumentNullException">s is null</exception> 
        /// <exception cref="System.FormatException">s is not in the correct format.</exception>
        /// <exception cref="System.OverflowException">s represents a number less than System.Decimal.MinValue or greater than System.Decimal.MaxValue.</exception>
        public static decimal Parse(string s)
        {
            var styles = NumberStyles.Number;

            CultureInfo cultureInfo;
            if (_deCulturePattern.IsMatch(s))
            {
                cultureInfo = _deCultureInfo;
            }
            else if (_usCulturePattern.IsMatch(s))
            {
                cultureInfo = _usCultureInfo;
            }
            else
            {
                throw new ArgumentException($"[{s}] is not valid decimal");
            }

            return decimal.Parse(s, styles, cultureInfo);
        }
    }
}
