﻿using System.Linq.Expressions;
using TaB.SharedKernel.Query;
using Microsoft.EntityFrameworkCore;

namespace TaB.Application
{
    public class Repository<TDbContext> : IRepository where TDbContext : DbContext
    {
        private bool _disposed;
        private readonly TDbContext _dbContext;

        public Repository(TDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        ~Repository()
        {
            if(!_disposed)
                Dispose();
        }

        public string GetTableName<T>() where T : class
        {
            return _dbContext.Model.FindEntityType(typeof(T)).GetDefaultTableName();
        }

        public void Add<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Add(entity);
        }        
        
        public void AddOrUpdate<T>(T entity, Expression<Func<T, object>> checkCriteria) where T : class 
        {
            _dbContext.Set<T>().AddOrUpdate(ref entity, checkCriteria);
        }

        public void AddOrUpdate<T>(List<T> entities, Expression<Func<T, object>> checkCriteria) where T : class
        {
            _dbContext.Set<T>().AddOrUpdate(ref entities, checkCriteria);
        }

        public void Update<T>(T entity) where T : class
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Remove<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Remove(entity);
        }

        public IQueryable<T> GetAll<T>() where T : class
        {
            return _dbContext.Set<T>().AsNoTracking();
        }

        public IQueryable<T> ByQuery<T>(IQuery<T> query) where T : class
        {
            var result = GetAll<T>().Where(query.GetExpression());
            result = query.GetIncludes().Aggregate(result, (current, include) => current.Include(include));

            return result;
        }

        public IOrderedQueryable<T> ByQuery<T>(ISortedQuery<T> query) where T : class
        {
            var result = GetAll<T>().Where(query.GetExpression());
            result = query.GetIncludes().Aggregate(result, (current, include) => current.Include(include));
            return query.GetSortingExpression()(result);
        }

        public IOrderedQueryable<T> ByQuery<T, K>(IGroupedSortedQuery<T, K> query) where T : class
        {
            var result = GetAll<T>().Where(query.GetExpression());
            result = query.GetIncludes().Aggregate(result, (current, include) => current.Include(include));

            //var (keySelector, resultSelector) = query.GetGroupBy();
            //result = result.GroupBy(keySelector, resultSelector);

            result = query.EmulateGroupBy(result);

            return query.GetSortingExpression()(result);
        }

        public T GetBy<T>(Expression<Func<T, bool>> criteria, List<Expression<Func<T, object>>>? includes = null) where T : class
        {
            if (includes == null) return _dbContext.Set<T>().AsNoTracking().FirstOrDefault(criteria);

            var result = _dbContext.Set<T>().AsNoTracking().AsQueryable();
            result = includes.Aggregate(result, (current, include) => current.Include(include));
            return result.FirstOrDefault(criteria);
        }

        public IQueryable<T> GetAllBy<T>(Expression<Func<T, bool>> criteria, List<Expression<Func<T, object>>> includes = null) where T : class
        {
            if (includes == null) return GetAll<T>().Where(criteria);

            var result = GetAll<T>().Where(criteria);
            result = includes.Aggregate(result, (current, include) => current.Include(include));
            return result;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _dbContext.Dispose();

            _disposed = true;
        }
    }
}