namespace TaB.Application
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository Repository { get; }
        void Commit();
        void SetIdentityInsert<T>() where T : class;
    }

    public interface IUnitOfWorkFactory
    {
        IUnitOfWork GetUnitOfWork();
    }
}