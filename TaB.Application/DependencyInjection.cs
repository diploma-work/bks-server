﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TaB.SharedKernel.Interfaces;
using TaB.Application.Services;
using TaB.SharedKernel;

namespace TaB.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddCore(this IServiceCollection services, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            services.AddOptions<StorageOptions>()
                .Configure<IConfiguration>((options, config) => config.GetSection("StorageOptions").Bind(options));

            //var callingAssembly = Assembly.GetExecutingAssembly();
            ////services.AddAutoMapper(callingAssembly);
            ////services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            //services.AddMediatR(Assembly.GetExecutingAssembly());

            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPerformanceBehavior<,>));
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehavior<,>));
            ////services.AddTransient<IRequestHandler<UpdateContentCommand, ContentItemInfo>, UpdateContentHandler>();
            services.AddTransient<IDateTime, UtcDateTimeService>();

            services.AddTransient<IUnitOfWorkFactory, UnitOfWorkFactory>();


            return services;
        }
    }
}
