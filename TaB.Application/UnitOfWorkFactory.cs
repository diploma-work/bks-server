﻿using TaB.Persistence;
using Microsoft.EntityFrameworkCore;

namespace TaB.Application
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory 
    {
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;

        public UnitOfWorkFactory(IDbContextFactory<ApplicationDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public IUnitOfWork GetUnitOfWork()
        {
            return new UnitOfWork<ApplicationDbContext>(_dbContextFactory.CreateDbContext());
        }
    }
}
