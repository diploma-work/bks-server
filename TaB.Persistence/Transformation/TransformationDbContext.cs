﻿using TaB.SharedKernel.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace TaB.Persistence.Transformation
{
    public class TransformationDbContext : ApplicationDbContext
    {
        public TransformationDbContext(
            DbContextOptions<ApplicationDbContext> options
            , ICurrentUserService currentUser
            ) : base(options, currentUser)
        {
        }

        // TODO: override OnModelCreating and load configurations for only used entities
    }
}
