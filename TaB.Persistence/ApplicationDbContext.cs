﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using TaB.SharedKernel.Interfaces;
using System.Reflection;
using TaB.Domain.Entities;
using TaB.Domain.Entities.Content;
using TaB.Domain.Entities.Testing;

namespace TaB.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        private readonly ICurrentUserService _currentUser;

        private IDbContextTransaction? _currentTransaction;

        public ApplicationDbContext(
             DbContextOptions<ApplicationDbContext> options,
             ICurrentUserService currentUser) : base(options)
        {
            _currentUser = currentUser;
        }

        public DbSet<UserRole> UserRoles { get; set; } = default!;
        public DbSet<UserLogin> UserLogins { get; set; } = default!;
        public DbSet<Content> Contents { get; set; } = default!;
        public DbSet<ContentMetadata> ContentMetadatas { get; set; } = default!;
        public DbSet<Test> Tests { get; set; } = default!;
        public DbSet<TestResult> TestResults { get; set; } = default!;


        protected override void OnModelCreating(ModelBuilder builder)
        {
            //_logger.LogInformation("Start model creating");
            base.OnModelCreating(builder);
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            //_logger.LogInformation("Finish model creating");
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
        {
            var result = await base.SaveChangesAsync(cancellationToken);

            return result;
        }

        public override int SaveChanges()
        {
            var result = base.SaveChanges();

            return result;
        }

        public async Task BeginTransactionAsync()
        {
            if (_currentTransaction != null)
            {
                return;
            }

            _currentTransaction = await Database.BeginTransactionAsync();
        }

        public async Task CommitTransactionAsync()
        {
            try
            {
                await SaveChangesAsync().ConfigureAwait(false);

                await _currentTransaction?.CommitAsync()!;

            }
            catch
            {
                RollbackTransaction();
                throw;
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                _currentTransaction?.Rollback();
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        public override void Dispose()
        {
            if (_currentTransaction != null)
            {
                RollbackTransaction();
            }
            base.Dispose();
        }

        public override ValueTask DisposeAsync()
        {
            if (_currentTransaction != null)
            {
                RollbackTransaction();
            }
            return base.DisposeAsync();
        }
    }
}