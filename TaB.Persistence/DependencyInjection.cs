﻿using TaB.Persistence.Transformation;
using TaB.Persistence.Migrations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace TaB.Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
                services.AddDbContextFactory<ApplicationDbContext>(
                    options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
                            o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery))
                        .EnableSensitiveDataLogging(true)
                    /*, ServiceLifetime.Transient*/);

                services.AddDbContextFactory<TransformationDbContext>(
                    options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
                                          o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery))
                                      .EnableSensitiveDataLogging(true));

                services.AddScoped<ApplicationDbContext>(p =>
                    p.GetRequiredService<IDbContextFactory<ApplicationDbContext>>()
                        .CreateDbContext());

            //services.AddTransient<INotificationPublisher, NotificationPublisher>();

            services.AddMigrations(configuration);

            return services;
        }
    }
}
