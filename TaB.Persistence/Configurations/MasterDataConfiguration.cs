﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaB.Domain.Entities;

namespace TaB.Persistence.Configurations
{
    public class MasterDataConfiguration : IEntityTypeConfiguration<MasterData>
    {
        public void Configure(EntityTypeBuilder<MasterData> builder)
        {
            builder.ToTable("MasterData");
            builder.HasKey(x => x.Id);


            builder.Property(x => x.Id)
                .HasColumnName(@"Id")
                .HasColumnType("int")
                .IsRequired()
                .ValueGeneratedOnAdd();

            builder.Property(x => x.Code)
                .HasColumnName(@"Code")
                .HasColumnType(@"nvarchar")
                .HasMaxLength(128);

            builder.Property(x => x.Value)
               .HasColumnName(@"Value")
               .HasColumnType(@"nvarchar")
               .HasMaxLength(128);
        }
    }
}
