﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TaB.Persistence.Configurations
{
    public class FileConfiguration : IEntityTypeConfiguration<Domain.Entities.File>
    {
        public void Configure(EntityTypeBuilder<Domain.Entities.File> builder)
        {
            builder.ToTable("Files");
            builder.HasKey(x => new { x.Id });

            builder.Property(x => x.Id)
                .HasColumnName(@"Id")
                .HasColumnType("int")
                .IsRequired()
                .ValueGeneratedOnAdd();

            builder.Property(x => x.FileType)
                .HasColumnName(@"FileType")
                .HasColumnType("int")
                .IsRequired();

            builder.Property(x => x.Size)
                .HasColumnName(@"Size")
                .HasColumnType("long")
                .IsRequired();

            builder.Property(x => x.Name)
                .HasColumnName(@"Name")
                .HasColumnType("nvarchar")
                .IsRequired(false)
                .HasMaxLength(256);

            builder.Property(x => x.Link)
                .HasColumnName(@"Link")
                .HasColumnType("nvarchar")
                .IsRequired()
                .HasMaxLength(4096);
        }
    }
}
