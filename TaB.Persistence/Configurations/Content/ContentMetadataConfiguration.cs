﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaB.Domain.Entities.Content;
using TaB.Domain.Entities.Content.Material;

namespace TaB.Persistence.Configurations.Content
{
    public class ContentMetadataConfiguration : IEntityTypeConfiguration<ContentMetadata>
    {
        public void Configure(EntityTypeBuilder<ContentMetadata> builder)
        {
            builder.ToTable("ContentMetadata");
            builder.HasKey(p => p.ContentId);

            builder.Ignore(p => p.Content);

            builder.Property(x => x.Title)
                .HasColumnName(@"Title")
                .HasColumnType("nvarchar(128)")
                .IsRequired();

            builder.Property(x => x.ContentString)
                .HasColumnName(@"ContentString")
                .HasColumnType("nvarchar(max)")
                .IsRequired();

            builder.HasDiscriminator<int>("ContentTypeId")
                .HasValue<Domain.Entities.Content.News.NewsMetadata>((int)Domain.Enums.ContentType.News)
                .HasValue<MaterialMetadata>((int)Domain.Enums.ContentType.Material)
                .IsComplete(false);
        }
    }
}
