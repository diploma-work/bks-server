﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaB.Domain.Entities.Content.News;

namespace TaB.Persistence.Configurations.Content.News
{
    public class NewsMetadataConfiguration : IEntityTypeConfiguration<NewsMetadata>
    {
        public void Configure(EntityTypeBuilder<NewsMetadata> builder)
        {
            builder.HasOne(p => p.News)
               .WithOne(p => p.Metadata)
               .HasForeignKey<NewsMetadata>(k => k.ContentId)
               .IsRequired();
        }
    }
}
