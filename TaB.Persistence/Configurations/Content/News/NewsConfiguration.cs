﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaB.Domain.Entities.Content.News;

namespace TaB.Persistence.Configurations.Content.News
{
    public class NewsConfiguration : IEntityTypeConfiguration<Domain.Entities.Content.News.News>
    {
        public void Configure(EntityTypeBuilder<Domain.Entities.Content.News.News> builder)
        {
            builder.HasOne(p => p.Metadata)
                .WithOne(p => p.News)
                .HasForeignKey<NewsMetadata>(k => k.ContentId)
                .IsRequired();
        }
    }
}
