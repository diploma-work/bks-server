﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaB.Domain.Entities;
using TaB.Domain.Entities.Content.Material;
using TaB.Domain.Entities.Content.News;

namespace TaB.Persistence.Configurations.Content
{
    public class ContentConfiguration : IEntityTypeConfiguration<Domain.Entities.Content.Content>
    {
        public void Configure(EntityTypeBuilder<Domain.Entities.Content.Content> builder)
        {
            builder.ToTable("Content");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName(@"Id")
                .HasColumnType("int")
                .IsRequired()
                .ValueGeneratedOnAdd();

            builder.Property(x => x.Status)
                .HasColumnName(@"Status")
                .HasColumnType("int")
                .IsRequired();

            builder.HasDiscriminator<int>("ContentTypeId")
                .HasValue<Domain.Entities.Content.News.News>((int)Domain.Enums.ContentType.News)
                .HasValue<Domain.Entities.Content.Material.Material>((int)Domain.Enums.ContentType.Material)
                .IsComplete(false);

            // Foreign keys

            builder.HasMany(p => p.Files)
                .WithMany(m => m.ContentList)
                .UsingEntity<FilesInContent>(j =>
                        j.HasOne(p => p.File)
                            .WithMany(m => m.ContentFiles)
                            .HasForeignKey(k => k.FilesId),
                    j =>
                        j.HasOne(p => p.Content)
                            .WithMany(m => m.ContentFiles)
                            .HasForeignKey(k => new { k.ContentId }));        


        }
    }
}
