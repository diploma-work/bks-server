﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaB.Domain.Entities.Content.Material;

namespace TaB.Persistence.Configurations.Content.Material
{
    public class MaterialMetadataConfiguration : IEntityTypeConfiguration<MaterialMetadata>
    {
        public void Configure(EntityTypeBuilder<MaterialMetadata> builder)
        {
            builder.HasOne(p => p.Material)
               .WithOne(p => p.Metadata)
               .HasForeignKey<MaterialMetadata>(k => k.ContentId)
               .IsRequired();
        }
    }
}
