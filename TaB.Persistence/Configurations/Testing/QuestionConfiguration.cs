﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaB.Domain.Entities.Testing;

namespace TaB.Persistence.Configurations.Testing
{
    public class QuestionConfiguration : IEntityTypeConfiguration<Question>
    {
        public void Configure(EntityTypeBuilder<Question> builder)
        {
            builder.ToTable("Questions");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Text)
                .HasColumnName(@"Text")
                .HasColumnType(@"nvarchar")
                .HasMaxLength(256);

            //Foreing key 

            builder.HasOne(x => x.Test)
                .WithMany(x => x.Questions)
                .HasForeignKey(x => x.TestId);

            builder.HasMany(x => x.Answers)
                .WithOne(x=>x.Question)
                .HasForeignKey(x => x.QuestionId); 
        }
    }
}
