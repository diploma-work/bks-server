﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaB.Domain.Entities.Testing;

namespace TaB.Persistence.Configurations.Testing
{
    public class AnswerConfiguration : IEntityTypeConfiguration<Answer>
    {
        public void Configure(EntityTypeBuilder<Answer> builder)
        {
            builder.ToTable("Answers");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Text)
                .HasColumnName(@"Text")
                .HasColumnType(@"nvarchar")
                .HasMaxLength(256);

            builder.Property(x => x.IsCorrect)
                .HasColumnName(@"IsCorrect")
                .HasColumnType(@"boolean");

            //foreign key

            builder.HasOne(x => x.Question)
                .WithMany(x => x.Answers)
                .HasForeignKey(x => x.QuestionId);

        }
    }
}
