﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaB.Domain.Entities.Testing;

namespace TaB.Persistence.Configurations.Testing
{
    public class TestConfiguration : IEntityTypeConfiguration<Test>
    {
        public void Configure(EntityTypeBuilder<Test> builder)
        {
            builder.ToTable("Tests");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Topic)
                .HasColumnName(@"Topic")
                .HasColumnType(@"nvarchar")
                .HasMaxLength(256);

            builder.Property(x => x.QuestionsCount)
                .HasColumnName(@"QuestionsCount")
                .HasColumnType(@"int")
                .IsRequired();

            //Foreign key

            builder.HasMany(p => p.Questions)
                .WithOne(p => p.Test)
                .HasForeignKey(p => p.TestId);
        }
    }
}

