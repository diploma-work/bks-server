﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaB.Domain.Entities.Testing;

namespace TaB.Persistence.Configurations.Testing
{
    public class TestResultConfiguration : IEntityTypeConfiguration<TestResult>
    {
        public void Configure(EntityTypeBuilder<TestResult> builder)
        {
            builder.ToTable("TestsResults");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.CorrectQuestionsCount)
                .HasColumnName(@"CorrectQuestionsCount")
                .HasColumnType(@"int");

            builder.Property(x => x.AnswersText)
                .HasColumnName(@"AnswersText")
                .HasColumnType(@"nvarchar")
                .HasMaxLength(4096);

            //foreign key

            builder.HasOne(x=>x.User)
                .WithMany(x=>x.TestResults)
                .HasForeignKey(x=>x.UserLoginId);

            //builder.HasOne(x => x.Test)
            //    .WithMany(x => x.TestResults)
            //    .HasForeignKey(x => x.Test);
        }
    }
}
