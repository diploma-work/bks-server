﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaB.Domain.Entities;

namespace TaB.Persistence.Configurations
{
    public class CommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.ToTable("Comments");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName(@"Id")
                .HasColumnType("int")
                .IsRequired()
                .ValueGeneratedOnAdd();

            builder.Property(x => x.Id)
                .HasColumnName(@"Id")
                .HasColumnType("int")
                .IsRequired();

            builder.Property(x => x.ContentId)
                .HasColumnName(@"ContentId")
                .HasColumnType("int")
                .IsRequired();

            builder.Property(x => x.ParentCommentId)
                .HasColumnName(@"ParentCommentId")
                .HasColumnType("int");

            builder.Property(x => x.UserLoginId)
               .HasColumnName(@"UserLoginId")
               .HasColumnType("int")
               .IsRequired();

            builder.Property(x => x.Text)
                .HasColumnName(@"Text")
                .HasColumnType("nvarchar")
                .IsRequired()
                .HasMaxLength(1024);

            //Foreign keys

            builder.HasOne(x => x.ParrentComment)
                .WithMany(x => x.ChildrenComments)
                .HasForeignKey(x=>x.ParentCommentId);

            builder.HasOne(x => x.News)
                .WithMany(x => x.Comments)
                .HasForeignKey(x=>x.ContentId);

            builder.HasOne(x => x.User)
                .WithMany(x => x.Comments)
                .HasForeignKey(x => x.UserLoginId);


        }
    }
}
