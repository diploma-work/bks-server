﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaB.Domain.Entities;

namespace TaB.Persistence.Configurations
{
    internal class LikeConfiguration : IEntityTypeConfiguration<Like>
    {
        public void Configure(EntityTypeBuilder<Like> builder)
        {
            builder.ToTable("Likes");
            builder.HasKey(p => new {
                p.ContentId,
                p.UserLoginId
            });

            builder.Property(x => x.ContentId)
                .HasColumnName("ContentId")
                .HasColumnType("int")
                .IsRequired();

            builder.Property(x => x.UserLoginId)
                .HasColumnName("UserLoginId")
                .HasColumnType("int")
                .IsRequired();

            // Foreign keys

            builder.HasOne(x => x.User)
                .WithMany(x => x.Likes)
                .IsRequired()
                .HasForeignKey(x => x.UserLoginId);

            builder.HasOne(x => x.News)
                .WithMany(x => x.Likes)
                .IsRequired()
                .HasForeignKey(x => x.ContentId);
        }
    }
}
