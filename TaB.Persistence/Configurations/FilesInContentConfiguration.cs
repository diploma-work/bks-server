﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaB.Domain.Entities;

namespace TaB.Persistence.Configurations
{
    public class FilesInContentConfiguration : IEntityTypeConfiguration<FilesInContent>
    {
        public void Configure(EntityTypeBuilder<FilesInContent> builder)
        {
            builder.ToTable("FilesInContent");
            builder.HasKey(p => new {
                p.ContentId,
                p.FilesId
            });

            builder.Property(x => x.FilesId)
                .HasColumnName("FilesId")
                .HasColumnType("int")
                .IsRequired();

            builder.Property(x => x.ContentId)
                .HasColumnName("ContentId")
                .HasColumnType("int")
                .IsRequired();
        }
    }
}
