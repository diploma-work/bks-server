﻿using TaB.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TaB.Persistence.Configurations
{
    public class UserRoleConfiguration : IEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder.ToTable("UserRole");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName(@"Id")
                .HasColumnType("int")
                .IsRequired()
                .ValueGeneratedOnAdd();

            builder.Property(x => x.Code)
                .HasColumnName(@"Code")
                .HasColumnType("nvarchar")
                .IsRequired()
                .HasMaxLength(128);

            builder.Property(x => x.Name)
                .HasColumnName(@"Name")
                .HasColumnType("nvarchar")
                .IsRequired()
                .HasMaxLength(256);

            builder.Property(x => x.Comment)
                .HasColumnName(@"Comment")
                .HasColumnType("nvarchar")
                .IsRequired(false)
                .HasMaxLength(int.MaxValue);

            builder.Ignore(x => x.ConcurrencyStamp);
            builder.Ignore(x => x.NormalizedName);
        }
    }
}