﻿using System.Security.Claims;
using System.Security.Principal;
using TaB.SharedKernel.Installers;
using TaB.SharedKernel.Interfaces;
using TaB.UserCommon.Services;
using Microsoft.AspNetCore.Http;

namespace TaB.ContentManagment.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private int _id;
        private int _languageId;
        private string _roleCode = string.Empty;

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            UserId = httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier)!;
            User = httpContextAccessor.HttpContext?.User!;
        }

        public IPrincipal User { get; }
        public string UserId { get; }

        public int Id
        {
            get
            {
                if (string.IsNullOrEmpty(UserId)) return 0;
                if (_id != 0) return _id;

                FillValues();
                return _id;
            }
        }

        public int LanguageId
        {
            get
            {
                if (string.IsNullOrEmpty(UserId)) return 0;
                if (_languageId != 0) return _languageId;

                FillValues();
                return _languageId;
            }
        }

        public string RoleCode
        {
            get
            {
                if (string.IsNullOrEmpty(UserId)) return _roleCode;
                if (!string.IsNullOrWhiteSpace(_roleCode)) return _roleCode;

                FillValues();
                return _roleCode;
            }
        }

        private void FillValues()
        {
            var umr = ServiceLocator.Resolve<IUserService>();
            var user = umr.GetBy(x => x.UserId == UserId);
            if (user == null) return;
            _id = user.Id;
            _roleCode = user.Role;
        }
    }
}
