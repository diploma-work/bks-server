﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Testing;
using TaB.Application.Features.Testing.Models;
using TaB.SharedKernel.Endpoints.Admin;

namespace TaB.ContentManagment.Endpoints.Testing
{
    public class UpdateEndpoint : BaseAdminEndpoint<UpdateRequest, TestInfo, UpdateEndpoint>
    {
        [HttpPost("Testing/Test/")]
        [OpenApiOperation(
            operationId: "Testing.Test",
            summary: "Update Test",
            description: "Update Test"
        )]
        [OpenApiTags("Test")]
        public override async Task<ActionResult<TestInfo>> HandleAsync([FromBody] UpdateRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
