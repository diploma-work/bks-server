﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Testing.TestResults;
using TaB.Application.Features.Testing.TestResults.Models;
using TaB.SharedKernel.Endpoints;

namespace TaB.ContentManagment.Endpoints.Testing.TestResult
{
    public class CreateEndpoint : BaseEndpoint<CreateRequest, TestResultsInfo, CreateEndpoint>
    {
        [HttpPut("Testing/TestResults/")]
        [OpenApiOperation(
            operationId: "TestResults.Create",
            summary: "Create new TestResults",
            description: "Create new TestResults"
        )]
        [OpenApiTags("TestResults")]
        public override async Task<ActionResult<TestResultsInfo>> HandleAsync([FromBody] CreateRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
