﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Testing.TestResults;
using TaB.Application.Features.Testing.TestResults.Models;
using TaB.SharedKernel.Endpoints;
using TaB.SharedKernel.Pagination;

namespace TaB.ContentManagment.Endpoints.Testing.TestResult
{
    public class GetAllEndpoint : BaseEndpoint<GetAllRequest, IPagedList<TestResultsShortInfo>, GetAllEndpoint>
    {
        [HttpPost("Testing/TestResults/GetAll")]
        [OpenApiOperation(
            operationId: "TestResults.GetAll",
            summary: "Get all TestResults",
            description: "Get all TestResults"
            )]
        [OpenApiTags("TestResults")]
        public override async Task<ActionResult<IPagedList<TestResultsShortInfo>>> HandleAsync([FromBody] GetAllRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
