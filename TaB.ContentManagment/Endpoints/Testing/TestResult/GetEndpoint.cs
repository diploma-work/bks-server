﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Testing.TestResults;
using TaB.Application.Features.Testing.TestResults.Models;
using TaB.SharedKernel.Endpoints;

namespace TaB.ContentManagment.Endpoints.Testing.TestResult
{
    public class GetEndpoint : BaseEndpoint<GetRequest, TestResultsInfo, GetEndpoint>
    {
        [HttpGet("Testing/TestResults/{Id:int}")]
        [OpenApiOperation(
            operationId: "TestResults.Get",
            summary: "Get TestResults",
            description: "Get TestResults"
        )]
        [OpenApiTags("TestResults")]
        public override async Task<ActionResult<TestResultsInfo>> HandleAsync([FromRoute] GetRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
