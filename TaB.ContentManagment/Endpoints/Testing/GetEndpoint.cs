﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Testing;
using TaB.Application.Features.Testing.Models;
using TaB.SharedKernel.Endpoints;

namespace TaB.ContentManagment.Endpoints.Testing
{
    public class GetEndpoint : BaseEndpoint<GetRequest, TestInfo, GetEndpoint>
    {
        [HttpGet("Testing/Test/{Id:int}")]
        [OpenApiOperation(
            operationId: "Test.Get",
            summary: "Get Test",
            description: "Get Test"
        )]
        [OpenApiTags("Test")]
        public override async Task<ActionResult<TestInfo>> HandleAsync([FromRoute] GetRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
