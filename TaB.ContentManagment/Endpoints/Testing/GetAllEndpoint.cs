﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Testing;
using TaB.Application.Features.Testing.Models;
using TaB.SharedKernel.Endpoints;
using TaB.SharedKernel.Pagination;

namespace TaB.ContentManagment.Endpoints.Testing
{
    public class GetAllEndpoint : BaseEndpoint<GetAllRequest, IPagedList<TestShortInfo>, GetAllEndpoint>
    {
        [HttpPost("Testing/Test/GetAll")]
        [OpenApiOperation(
            operationId: "Test.GetAll",
            summary: "Get all Test",
            description: "Get all Test"
            )]
        [OpenApiTags("Test")]
        public override async Task<ActionResult<IPagedList<TestShortInfo>>> HandleAsync([FromBody] GetAllRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
