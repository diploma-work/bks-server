﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Testing;
using TaB.Application.Features.Testing.Models;
using TaB.SharedKernel.Endpoints.Admin;

namespace TaB.ContentManagment.Endpoints.Testing
{
    public class CreateEndpoint : BaseAdminEndpoint<CreateRequest, TestInfo, CreateEndpoint>
    {
        [HttpPut("Testing/Test/")]
        [OpenApiOperation(
            operationId: "Test.Create",
            summary: "Create new Test",
            description: "Create new Test"
        )]
        [OpenApiTags("Test")]
        public override async Task<ActionResult<TestInfo>> HandleAsync([FromBody] CreateRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
