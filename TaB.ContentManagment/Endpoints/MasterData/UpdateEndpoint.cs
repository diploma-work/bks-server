﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.MasterData;
using TaB.Application.Features.MasterData.Models;
using TaB.SharedKernel.Endpoints.Admin;

namespace TaB.ContentManagment.Endpoints.MasterData
{
    public class UpdateEndpoint : BaseAdminEndpoint<UpdateRequest, MasterDataInfo, UpdateEndpoint>
    {
        [HttpPost("MasterData/")]
        [OpenApiOperation(
            operationId: "MasterData.Update",
            summary: "Update MasterData",
            description: "Update MasterData"
        )]
        [OpenApiTags("MasterData")]
        public override async Task<ActionResult<MasterDataInfo>> HandleAsync([FromBody] UpdateRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
