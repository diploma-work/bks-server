﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.MasterData;
using TaB.Application.Features.MasterData.Models;
using TaB.SharedKernel.Endpoints;

namespace TaB.ContentManagment.Endpoints.MasterData
{
    public class GetAllEndpoint : BaseEndpoint<GetAllRequest, List<MasterDataInfo>, GetAllEndpoint>
    {
        [HttpPost("MasterData/GetAll")]
        [OpenApiOperation(
            operationId: "MasterData.GetAll",
            summary: "Get all MasterData",
            description: "Get all MasterData"
            )]
        [OpenApiTags("MasterData")]
        public override async Task<ActionResult<List<MasterDataInfo>>> HandleAsync([FromBody] GetAllRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
