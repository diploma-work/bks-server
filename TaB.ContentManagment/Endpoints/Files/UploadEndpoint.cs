﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Files;
using TaB.SharedKernel.Endpoints.Admin;
using TaB.SharedKernel.Models;

namespace TaB.ContentManagment.Endpoints.Files
{
    public class UploadEndpoint : BaseAdminEndpoint<UploadEndpoint>
    {
        [HttpPost("Files/Upload/")]
        [OpenApiOperation(
            operationId: "Files.Upload",
            summary: "Upload file",
            description: "Upload file"
        )]
        [OpenApiTags("Files")]
        public async Task<ActionResult<FilesInfo>> HandleAsync(IFormFile request, CancellationToken cancellationToken)
        {
            Logger.LogInformation($"Asset upload {request.ContentType} {request.FileName} {request.Length}");
            return await Mediator.Send(new UploadRequest { File = request }, cancellationToken);
        }
    }
}
