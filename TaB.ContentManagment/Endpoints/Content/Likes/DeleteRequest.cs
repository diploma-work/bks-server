﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Likes;
using TaB.SharedKernel.Endpoints;

namespace TaB.ContentManagment.Endpoints.Content.Likes
{
    public class DeleteEndpoint : BaseEndpoint<DeleteRequest, Unit, DeleteEndpoint>
    {
        [HttpPost("Content/Likes/Delete")]
        [OpenApiOperation(
            operationId: "Like.Delete",
            summary: "Delete Like",
            description: "Delete Like"
        )]
        [OpenApiTags("Likes")]
        public override async Task<ActionResult<Unit>> HandleAsync([FromBody] DeleteRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
