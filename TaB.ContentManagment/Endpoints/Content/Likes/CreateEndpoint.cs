﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Likes;
using TaB.Application.Features.Content.Likes.Models;
using TaB.SharedKernel.Endpoints;

namespace TaB.ContentManagment.Endpoints.Content.Likes
{
    public class CreateEndpoint : BaseEndpoint<CreateRequest, LikesInNewsInfo, CreateEndpoint>
    {
        [HttpPut("Content/Likes/")]
        [OpenApiOperation(
            operationId: "Like.Create",
            summary: "Like new Content",
            description: "Like new Content"
        )]
        [OpenApiTags("Likes")]
        public override async Task<ActionResult<LikesInNewsInfo>> HandleAsync([FromBody] CreateRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
