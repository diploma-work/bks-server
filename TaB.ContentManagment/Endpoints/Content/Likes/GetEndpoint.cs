﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Likes;
using TaB.Application.Features.Content.Likes.Models;
using TaB.SharedKernel.Endpoints;

namespace TaB.ContentManagment.Endpoints.Content.Likes
{
    public class GetEndpoint : BaseEndpoint<GetRequest, LikesInNewsInfo, GetEndpoint>
    {
        [HttpGet("Content/Likes/{ContentId:int}")]
        [OpenApiOperation(
            operationId: "Likes.Get",
            summary: "Get Likes",
            description: "Get Likes"
        )]
        [OpenApiTags("Likes")]
        public override async Task<ActionResult<LikesInNewsInfo>> HandleAsync([FromRoute] GetRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
