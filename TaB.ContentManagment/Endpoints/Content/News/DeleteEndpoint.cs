﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.News;
using TaB.SharedKernel.Endpoints.Admin;

namespace TaB.ContentManagment.Endpoints.Content.News
{
    public class DeleteEndpoint : BaseAdminEndpoint<DeleteRequest, Unit, DeleteEndpoint>
    {
        [HttpPost("Content/News/Delete")]
        [OpenApiOperation(
            operationId: "News.Delete",
            summary: "Delete news",
            description: "Delete news"
        )]
        [OpenApiTags("News")]
        public override async Task<ActionResult<Unit>> HandleAsync([FromBody] DeleteRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
