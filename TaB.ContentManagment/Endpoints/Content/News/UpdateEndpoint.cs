﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.News;
using TaB.Application.Features.Content.News.Models;
using TaB.SharedKernel.Endpoints.Admin;

namespace TaB.ContentManagment.Endpoints.Content.News
{
    public class UpdateEndpoint : BaseAdminEndpoint<UpdateRequest, NewsInfo, UpdateEndpoint>
    {
        [HttpPost("Content/News/")]
        [OpenApiOperation(
            operationId: "News.Update",
            summary: "Update news",
            description: "Update news"
        )]
        [OpenApiTags("News")]
        public override async Task<ActionResult<NewsInfo>> HandleAsync([FromBody] UpdateRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
