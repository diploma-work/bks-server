﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.News;
using TaB.Application.Features.Content.News.Models;
using TaB.SharedKernel.Endpoints.Unautorized;

namespace TaB.ContentManagment.Endpoints.Content.News
{
    public class GetEndpoint : BaseUnauthorizedEndpoint<GetRequest, NewsInfo, GetEndpoint>
    {
        [HttpGet("Content/News/{Id:int}")]
        [OpenApiOperation(
            operationId: "News.Get",
            summary: "Get news",
            description: "Get news"
        )]
        [OpenApiTags("News")]
        public override async Task<ActionResult<NewsInfo>> HandleAsync([FromRoute] GetRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
