﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.News;
using TaB.Application.Features.Content.News.Models;
using TaB.SharedKernel.Endpoints.Admin;

namespace TaB.ContentManagment.Endpoints.Content.News
{
    public class CreateEndpoint : BaseAdminEndpoint<CreateRequest, NewsInfo, CreateEndpoint>
    {
        [HttpPut("Content/News/")]
        [OpenApiOperation(
            operationId: "News.Create",
            summary: "Create new News",
            description: "Create new News"
        )]
        [OpenApiTags("News")]
        public override async Task<ActionResult<NewsInfo>> HandleAsync([FromBody] CreateRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
