﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.News;
using TaB.Application.Features.Content.News.Models;
using TaB.SharedKernel.Endpoints.Unautorized;
using TaB.SharedKernel.Pagination;

namespace TaB.ContentManagment.Endpoints.Content.News
{
    public class GetAllEndpoint : BaseUnauthorizedEndpoint<GetAllRequest, IPagedList<NewsInfo>, GetAllEndpoint>
    {
        [HttpPost("Content/News/GetAll")]
        [OpenApiOperation(
            operationId: "News.GetAll",
            summary: "Get all news",
            description: "Get all news"
            )]
        [OpenApiTags("News")]
        public override async Task<ActionResult<IPagedList<NewsInfo>>> HandleAsync([FromBody] GetAllRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
