﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Comments;
using TaB.Application.Features.Content.Comments.Models;
using TaB.SharedKernel.Endpoints;

namespace TaB.ContentManagment.Endpoints.Content.Comments
{
    public class CreateEndpoint : BaseEndpoint<CreateRequest, CommentInfo, CreateEndpoint>
    {
        [HttpPut("Content/Comments/")]
        [OpenApiOperation(
            operationId: "Comment.Create",
            summary: "Create new Comment",
            description: "Create new Comment"
        )]
        [OpenApiTags("Comments")]
        public override async Task<ActionResult<CommentInfo>> HandleAsync([FromBody] CreateRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
