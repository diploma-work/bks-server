﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Comments;
using TaB.Application.Features.Content.Comments.Models;
using TaB.SharedKernel.Endpoints.Unautorized;
using TaB.SharedKernel.Pagination;

namespace TaB.ContentManagment.Endpoints.Content.Comments
{
    public class GetAllEndpoint : BaseUnauthorizedEndpoint<GetAllRequest, IPagedList<CommentInfo>, GetAllEndpoint>
    {
        [HttpPost("Content/Comments/GetAll")]
        [OpenApiOperation(
            operationId: "Comments.GetAll",
            summary: "Get all Comments",
            description: "Get all Comments"
            )]
        [OpenApiTags("Comments")]
        public override async Task<ActionResult<IPagedList<CommentInfo>>> HandleAsync([FromBody] GetAllRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
