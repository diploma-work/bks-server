﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Comments;
using TaB.Application.Features.Content.Comments.Models;
using TaB.SharedKernel.Endpoints.Unautorized;

namespace TaB.ContentManagment.Endpoints.Content.Comments
{
    public class GetEndpoint : BaseUnauthorizedEndpoint<GetRequest, CommentInfo, GetEndpoint>
    {
        [HttpGet("Content/Comments/{Id:int}")]
        [OpenApiOperation(
            operationId: "Comments.Get",
            summary: "Get Comment",
            description: "Get Comment"
        )]
        [OpenApiTags("Comments")]
        public override async Task<ActionResult<CommentInfo>> HandleAsync([FromRoute] GetRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
