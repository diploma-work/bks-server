﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Comments;
using TaB.Application.Features.Content.Comments.Models;
using TaB.SharedKernel.Endpoints;

namespace TaB.ContentManagment.Endpoints.Content.Comments
{
    public class UpdateEndpoint : BaseEndpoint<UpdateRequest, CommentInfo, UpdateEndpoint>
    {
        [HttpPost("Content/Comments/")]
        [OpenApiOperation(
            operationId: "Comment.Update",
            summary: "Update Comment",
            description: "Update Comment"
        )]
        [OpenApiTags("Comments")]
        public override async Task<ActionResult<CommentInfo>> HandleAsync([FromBody] UpdateRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
