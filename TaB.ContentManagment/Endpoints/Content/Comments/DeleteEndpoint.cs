﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Comments;
using TaB.SharedKernel.Endpoints.Admin;

namespace TaB.ContentManagment.Endpoints.Content.Comments
{
    public class DeleteEndpoint : BaseAdminEndpoint<DeleteRequest, Unit, DeleteEndpoint>
    {
        [HttpPost("Content/Comments/Delete")]
        [OpenApiOperation(
            operationId: "Comment.Delete",
            summary: "Delete Comment",
            description: "Delete Comment"
        )]
        [OpenApiTags("Comments")]
        public override async Task<ActionResult<Unit>> HandleAsync([FromBody] DeleteRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
