﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Material;
using TaB.Application.Features.Content.Material.Models;
using TaB.SharedKernel.Endpoints.Unautorized;

namespace TaB.ContentManagment.Endpoints.Content.Material
{
    public class GetEndpoint : BaseUnauthorizedEndpoint<GetRequest, MaterialInfo, GetEndpoint>
    {
        [HttpGet("Content/Material/{Id:int}")]
        [OpenApiOperation(
            operationId: "Material.Get",
            summary: "Get Material",
            description: "Get Material"
        )]
        [OpenApiTags("Material")]
        public override async Task<ActionResult<MaterialInfo>> HandleAsync([FromRoute] GetRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
