﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Material;
using TaB.Application.Features.Content.Material.Models;
using TaB.SharedKernel.Endpoints.Unautorized;
using TaB.SharedKernel.Pagination;

namespace TaB.ContentManagment.Endpoints.Content.Material
{
    public class GetAllEndpoint : BaseUnauthorizedEndpoint<GetAllRequest, IPagedList<MaterialInfo>, GetAllEndpoint>
    {
        [HttpPost("Content/Material/GetAll")]
        [OpenApiOperation(
            operationId: "Material.GetAll",
            summary: "Get all Material",
            description: "Get all Material"
            )]
        [OpenApiTags("Material")]
        public override async Task<ActionResult<IPagedList<MaterialInfo>>> HandleAsync([FromBody] GetAllRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
