﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Material;
using TaB.Application.Features.Content.Material.Models;
using TaB.SharedKernel.Endpoints.Admin;

namespace TaB.ContentManagment.Endpoints.Content.Material
{
    public class UpdateEndpoint : BaseAdminEndpoint<UpdateRequest, MaterialInfo, UpdateEndpoint>
    {
        [HttpPost("Content/Material/")]
        [OpenApiOperation(
            operationId: "Material.Update",
            summary: "Update Material",
            description: "Update Material"
        )]
        [OpenApiTags("Material")]
        public override async Task<ActionResult<MaterialInfo>> HandleAsync([FromBody] UpdateRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
