﻿using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Material;
using TaB.Application.Features.Content.Material.Models;
using TaB.SharedKernel.Endpoints.Admin;

namespace TaB.ContentManagment.Endpoints.Content.Material
{
    public class CreateEndpoint : BaseAdminEndpoint<CreateRequest, MaterialInfo, CreateEndpoint>
    {
        [HttpPut("Content/Material/")]
        [OpenApiOperation(
            operationId: "Material.Create",
            summary: "Create new Material",
            description: "Create new Material"
        )]
        [OpenApiTags("Material")]
        public override async Task<ActionResult<MaterialInfo>> HandleAsync([FromBody] CreateRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
