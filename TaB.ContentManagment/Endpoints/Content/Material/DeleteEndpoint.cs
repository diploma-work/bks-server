﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using TaB.Application.Features.Content.Material;
using TaB.SharedKernel.Endpoints.Unautorized;

namespace TaB.ContentManagment.Endpoints.Content.Material
{
    public class DeleteEndpoint : BaseUnauthorizedEndpoint<DeleteRequest, Unit, DeleteEndpoint>
    {
        [HttpPost("Content/Material/Delete")]
        [OpenApiOperation(
            operationId: "Material.Delete",
            summary: "Delete Material",
            description: "Delete Material"
        )]
        [OpenApiTags("Material")]
        public override async Task<ActionResult<Unit>> HandleAsync([FromBody] DeleteRequest request, CancellationToken cancellationToken) =>
            await base.HandleAsync(request, cancellationToken);
    }
}
