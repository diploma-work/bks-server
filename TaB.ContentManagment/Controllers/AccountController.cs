﻿using TaB.Application.Controllers;
using TaB.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace TaB.UserManager.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly SignInManager<UserLogin> _signInManager;
        private UserManager<UserLogin> AppUserManager { get; }

        public AccountController(ILogger<AccountController> logger, UserManager<UserLogin> userManager, SignInManager<UserLogin> signInManager) : base(logger)
        {
            _signInManager = signInManager;
            AppUserManager = userManager;
        }

        [HttpGet("LogOut")]
        [Authorize]
        [OpenApiOperation(
            summary: "Logout current user",
            description: "Logout current user",
            operationId: "Account.LogOut"
            )]
        [OpenApiTags("Account")]
        public async Task<ActionResult> LogOut()
        {
            try
            {
                var user = await AppUserManager.FindByIdAsync(User.Identity.Name);
                var role = user.UserRole.Name;

                await _signInManager.SignOutAsync();

                Logger.LogInformation($"User logged out [User Id: {user.UserId}, User Role: {role}]");

                return Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e, "Unhandled error: {0}", e.Message);
                return BadRequest("error", e.Message);
            }
        }
    }
}
