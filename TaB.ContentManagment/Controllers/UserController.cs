﻿using System.Net;
using TaB.Application.Controllers;
using TaB.SharedKernel.Exceptions;
using TaB.SharedKernel.Models;
using TaB.SharedKernel.Pagination;
using TaB.UserCommon;
using TaB.UserCommon.Filters;
using TaB.UserCommon.Models;
using TaB.UserCommon.Services;
using TaB.UserCommon.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSwag.Annotations;
using AutoMapper;
using TaB.SharedKernel;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using TaB.Persistence;
using TaB.Domain.Entities;
using TaB.UserCommon.Identity;
using Microsoft.AspNetCore.Identity;
using System.Configuration;

namespace TaB.UserManager.Controllers
{
    public class UserController : BaseApiController
    {
        private const int TempPasswordDefaultLength = 12;

        private readonly IUserService _service;
        public IConfiguration Configuration { get; set; }
        private readonly UserPostValidator _postValidator;
        private readonly UserPutValidator _putValidator;
        private readonly IMapper _mapper;
        private StorageOptions _storageOptions;
        private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
        private readonly UserManager<UserLogin> _userManager;

        public UserController(
            ILogger<UserController> logger,
            IUserService service,
            UserPostValidator postValidator,
            IOptions<StorageOptions> storageOptions,
            UserPutValidator putValidator,
            IDbContextFactory<ApplicationDbContext> dbContextFactory,
            UserManager<UserLogin> userManager,
            IConfiguration Configuration,
        IMapper mapper) : base(logger)
        {
            _service = service;
            _postValidator = postValidator;
            _putValidator = putValidator;
            _dbContextFactory = dbContextFactory;
            _userManager = userManager;
            _mapper = mapper;
            _storageOptions = storageOptions.Value;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("GetAll")]
        [OpenApiOperation(
            summary: "Get all users",
            description: "Get all users",
            operationId: "User.PagedList"
            )]
        [OpenApiTags("User")]
        [SwaggerResponse(200, typeof(IPagedList<UserInfo>))]
        public ActionResult GetAll([FromBody] UserFilter filter)
        {
            Logger.LogInformation($"GetAll, filter = {JsonConvert.SerializeObject(filter, Formatting.Indented)}");
            filter ??= new UserFilter();

            var validator = new UserFilterValidator();
            var results = validator.Validate(filter);

            if (results.IsValid) return Ok(_service.GetAll(filter));

            Logger.LogWarning("BadRequest: {0}", ModelState);
            return BadRequest("error", GetFirstModelError(results));
        }

        [HttpPut("")]
        [OpenApiOperation(
            summary: "Add new user",
            description: "Add new user",
            operationId: "User.Create"
        )]
        [OpenApiTags("User")]
        [SwaggerResponse(200, typeof(UserInfo))]
        [SwaggerResponse(400, typeof(Error))]
        public ActionResult Put([FromBody] PutUserInfo user)
        {
            if(user.SecretPhrase != _storageOptions.SecretPhrase)
                return BadRequest("Incorrect secret phrase");

            Logger.LogInformation($"PUT {user}");

            var results = _putValidator.Validate(_mapper.Map<PostUserInfo>(user));
            if (!results.IsValid)
            {
                Logger.LogWarning("BadRequest: {0}", ModelState);
                return BadRequest("error", GetFirstModelError(results));
            }

            try
            {
                user = _mapper.Map<PutUserInfo>(_service.Add(user));
            }
            catch (ExceptionBase ex)
            {
                return BadRequest("error", ex.Message);
            }

            return Ok(user);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("")]
        [OpenApiOperation(
            summary: "Update user",
            description: "Update user",
            operationId: "User.Update")]
        [OpenApiTags("User")]
        [SwaggerResponse(200, typeof(UserInfo))]
        [SwaggerResponse(400, typeof(Error))]
        public ActionResult Post(PostUserInfo user)
        {
            Logger.LogInformation("POST api/user [{0}]", user);

            var results = _postValidator.Validate(user);
            if (!results.IsValid)
            {
                Logger.LogWarning("BadRequest: {0}", ModelState);
                return BadRequest("error", GetFirstModelError(results));
            }

            try
            {
                return Ok(_service.Update(user));
            }
            catch (ExceptionBase ex)
            {
                Logger.LogError(ex, ex.Message);
                return BadRequest("error", ex.Message);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{userId}")]
        [OpenApiOperation(
            summary: "Delete user",
            description: "Delete user",
            operationId: "User.Delete"
        )]
        [OpenApiTags("User")]
        [SwaggerResponse(400, typeof(Error))]
        public ActionResult Delete(string userId)
        {
            Logger.LogInformation(userId);

            try
            {
                if (User.Identity.Name == userId)
                {
                    Logger.LogError("User could not delete himself");
                    return BadRequest("error", "User could not delete himself");
                }
                _service.Delete(x => x.UserId == userId);
                return Ok();
            }
            catch (ExceptionBase ex)
            {
                Logger.LogError(ex, ex.Message);
                return BadRequest("error", ex.Message);
            }
        }

        [Authorize]
        [HttpPut("ChangePassword")]
        [OpenApiOperation(
            summary: "Change password for  user",
            description: "Change password for  user",
            operationId: "User.ResetPassword"
        )]
        [OpenApiTags("User")]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel model)
        {
            await using var db = _dbContextFactory.CreateDbContext();

            var user = db.Set<UserLogin>().FirstOrDefault(x=>x.UserId == model.UserId);
            if (user == null) return BadRequest($"User with UserId {model.UserId} do not exist");

            if (await _userManager.CheckPasswordAsync(user, model.OldPassword))
            {
                user.PasswordHash =  new CustomPasswordHasher().HashPassword(null, model.NewPassword);
                db.Set<UserLogin>().Update(user);
            }
            else
                return BadRequest($"OldPassword isnt correct");

            return Ok();
        }

        [Authorize]
        [HttpPost("ResetPassword")]
        [OpenApiOperation(
            summary: "Reset password for  user",
            description: "Reset password for  user",
            operationId: "User.ResetPassword"
        )]
        [OpenApiTags("User")]
        [SwaggerResponse(200, typeof(ResetPasswordModel))]
        public ActionResult ResetPassword(ResetPasswordModel data)
        {
            Logger.LogInformation($"POST api/ResetPassword for user {data.UserId}");

            try
            {
                _service.ResetPassword(data);
                return Ok();

            }
            catch (ExceptionBase ex)
            {
                Logger.LogWarning("User error {0}", ex.Message);
                return ex.StatusCode == HttpStatusCode.BadRequest
                    ? BadRequest("error", ex.Message)
                    : NotFound("error", ex.Message);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("roles")]
        [OpenApiOperation(
            summary: "Get all roles",
            description: "Get all roles",
            operationId: "User.Roles"
        )]
        [OpenApiTags("User")]
        [SwaggerResponse(200, typeof(List<RoleInfo>))]
        public ActionResult Roles()
        {
            Logger.LogInformation("GET Roles");
            return Ok(_service.GetRoles().Where(x => x.Code != "User").ToList());
        }

        //[Authorize(Roles = "Admin")]
        [HttpGet("UpdateSecretPhase")]
        [OpenApiOperation(
            summary: "Update Secret Phase",
            description: "Update Secret Phase",
            operationId: "Update.SecretPhase"
        )]
        [OpenApiTags("SecretPhase")]
        [SwaggerResponse(200, typeof(List<RoleInfo>))]
        public ActionResult UpdatePhase()
        {
            AddOrUpdateAppSetting("StorageOptions:SecretPhrase", "1");
            var filePath = Path.Combine(AppContext.BaseDirectory, "appsettings.json");
            string json = System.IO.File.ReadAllText(filePath);
            return Ok();
        }

        public void AddOrUpdateAppSetting<T>(string key, T value)
        {
            try
            {
                var filePath = Path.Combine(AppContext.BaseDirectory, "appSettings.json");
                string json = System.IO.File.ReadAllText(filePath);
                dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);

                var sectionPath = key.Split(":")[0];

                if (!string.IsNullOrEmpty(sectionPath))
                {
                    var keyPath = key.Split(":")[1];
                    jsonObj[sectionPath][keyPath] = value;
                }
                else
                {
                    jsonObj[sectionPath] = value; // if no sectionpath just set the value
                }

                string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj, Newtonsoft.Json.Formatting.Indented);
                System.IO.File.WriteAllText(filePath, output);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error writing app settings");
            }
        }
    }
}
