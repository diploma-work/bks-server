using AutoMapper;
using DryIoc;
using FluentMigrator.Runner;
using TaB.Application;
using TaB.Persistence;
using TaB.SharedKernel.Exceptions.Filter;
using TaB.SharedKernel.Installers;
using TaB.SharedKernel.Interfaces;
using TaB.UserCommon;
using TaB.ContentManagment.Services;
using MediatR;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using NSwag;
using NSwag.Generation.Processors.Security;
using Microsoft.Extensions.FileProviders;
using TaB.SharedKernel;

namespace TaB.ContentManagment
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions<StorageOptions>()
               .Configure<IConfiguration>((options, config) => config.GetSection("StorageOptions").Bind(options));

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    });
            });

            services.AddAutoMapper(GetAutoMapperProfilesFromAllAssemblies()
                .ToArray());

            services.AddPersistence(Configuration);
            services.AddCore(Configuration);
            services.AddIdentity(Configuration);

            services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies().Where(x => x.FullName != null && x.FullName.Contains("TaB")).ToArray());

            services.AddTransient<ICurrentUserService, CurrentUserService>();

            services.AddHttpContextAccessor();

            services.AddControllers(options =>
                    options.Filters.Add(new ApiExceptionFilter()))
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver { NamingStrategy = new CamelCaseNamingStrategy { ProcessDictionaryKeys = false } };
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
#if DEBUG
                    options.SerializerSettings.Formatting = Formatting.Indented;
#endif
                })
                .AddControllersAsServices();

            services.AddRazorPages();

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "wwwroot";
            });

            services.AddOpenApiDocument(configure =>
            {
                configure.Title = "User Manager API";
                configure.AddSecurity("JWT", Enumerable.Empty<string>(), new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    In = OpenApiSecurityApiKeyLocation.Header,
                    Description = "Type into the textbox: Bearer {your JWT token}."
                });
                
                
                configure.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("JWT"));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IMigrationRunner migrationRunner)
        {

            var storageOptions = Configuration.GetSection("StorageOptions").Get<StorageOptions>();
            //TODO: remove after client will be fixed
            app.Use(async (context, next) =>
            {
                if (context.Request.Path == "/token")
                {
                    context.Request.ContentType = "application/x-www-form-urlencoded";
                }
                await next.Invoke();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(storageOptions.DocumentsFolder),
                RequestPath = new PathString("/docs")
            });
            app.UseSpaStaticFiles();

            //app.UseHttpsRedirection();

            app.UseCors("AllowAll");

            app.UseRouting();

            app.UseIdentity();

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });

            app.UseOpenApi();
            app.UseSwaggerUi3();

            migrationRunner.MigrateUp();
        }

        public static IEnumerable<Type> GetAutoMapperProfilesFromAllAssemblies()
        {
            return from assembly in AppDomain.CurrentDomain.GetAssemblies()
                   from aType in assembly.GetTypes()
                   where aType.IsClass && !aType.IsAbstract && aType.IsSubclassOf(typeof(Profile))
                   select aType;
        }

        public void ConfigureContainer(IContainer container)
        {
            container = container.With(rules =>
                Rules.Default
                    .With(propertiesAndFields: request =>
                        request.ServiceType.Name.EndsWith("Controller") ? PropertiesAndFields.Properties()(request) : null)
                    .WithAutoConcreteTypeResolution()
                    .With(FactoryMethod.ConstructorWithResolvableArguments)
                    .WithoutThrowOnRegisteringDisposableTransient()
            );

            // You may place your registrations here or split them in different classes, or organize them in some kind of modules, e.g:
            ServiceLocator.Configure(container);
            // NOTE:
            // Don't configure the container rules here because DryIoc uses the immutable container/configuration
            // and you customized container will be lost.
            // Instead you may use something like `CreateMyPreConfiguredContainer` above.
        }

        public static IContainer CreateMyPreConfiguredContainer() =>
            // This is an example configuration,
            // for possible options check the https://github.com/dadhi/DryIoc/blob/master/docs/DryIoc.Docs/RulesAndDefaultConventions.md
            new Container(rules =>

                // Configures property injection for Controllers, ensure that you've added `AddControllersAsServices` in `ConfigureServices`
                rules.With(propertiesAndFields: request =>
                        request.ServiceType.Name.EndsWith("Controller") ? PropertiesAndFields.Properties()(request) : null)
                    .WithAutoConcreteTypeResolution()
            );

    }
}
